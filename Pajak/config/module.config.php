<?php

// This app creted by : Miftahul Huda (miftahul06@gmail.com)

return array(
    'controllers' => array(
        'invokables' => array(
            //Login dan Main
            'LoginAccess' => 'Pajak\Controller\LoginAccess',
            'MainController' => 'Pajak\Controller\MainController',
            //Modul Wajib Pajak
            'Pendataan' => 'Pajak\Controller\Pendataan\Pendataan',
            'Laporan' => 'Pajak\Controller\Laporan\Laporan',
            //Modul Setting
            'SettingUser' => 'Pajak\Controller\Setting\SettingUser',
            'LayoutBackground' => 'Pajak\Controller\Setting\LayoutBackground',
            'SlideBackground' => 'Pajak\Controller\Setting\SlideBackground',
            'AclManager' => 'Pajak\Controller\Setting\AclManager',
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'Tools' => 'Pajak\Controller\Plugin\Tools',
        )
    ),
    'router' => array(
        'routes' => array(
            'sign_in' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/sign_in[/:action][/:par1]',
                    'defaults' => array(
                        'controller' => 'LoginAccess',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'captcha_form' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/[:controller[/[:action[/]]]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(),
                        ),
                    ),
                    'captcha_form_generate' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/[:controller[/captcha/[:id]]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'LoginAccess',
                                'action' => 'generate',
                            ),
                        ),
                    ),
                ),
            ),
            'main' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/main[/:action][/:textcari]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'MainController',
                        'action' => 'index',
                    ),
                ),
            ),
            'pendataan' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pendataan[/:action][/:s_idjenis][/:page][/:rows][/:direction][/:combocari][/:kolomcari][/:combosorting][/:sortasc][/:sortdesc][/:combooperator]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Pendataan',
                        'action' => 'index',
                    ),
                ),
            ),
            'laporan' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/laporan[/:action][/:s_idjenis][/:t_idobjek][/:modaltransaksi][/:page][/:rows][/:direction][/:combocari][/:kolomcari][/:combosorting][/:sortasc][/:sortdesc][/:combooperator]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Laporan',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_user' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_user[/:action][/:page][/:rows][/:direction][/:combocari][/:kolomcari][/:combosorting][/:sortasc][/:sortdesc][/:combooperator]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingUser',
                        'action' => 'index',
                    ),
                ),
            ),
            // 'background' => array(
            //     'type' => 'segment',
            //     'options' => array(
            //         'route' => '/background[/:action][/:page][/:rows][/:direction]',
            //         'constraints' => array(
            //             'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
            //             'id' => '[0-9]+',
            //         ),
            //         'defaults' => array(
            //             'controller' => 'LayoutBackground',
            //             'action' => 'index',
            //         ),
            //     ),
            // ),
            // 'slidebackground' => array(
            //     'type' => 'segment',
            //     'options' => array(
            //         'route' => '/slidebackground[/:action][/:page][/:rows][/:direction]',
            //         'constraints' => array(
            //             'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
            //             'id' => '[0-9]+',
            //         ),
            //         'defaults' => array(
            //             'controller' => 'SlideBackground',
            //             'action' => 'index',
            //         ),
            //     ),
            // ),
            'acl_manager' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/acl_manager[/:action][/:page][/:rows][/:direction]',
                    'defaults' => array(
                        'controller' => 'AclManager',
                        'action' => 'index'
                    )
                )
            ),
            'realisasi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/realisasi[/:action][/:textcari]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'LoginAccess',
                        'action' => 'realisasi',
                    ),
                ),
            ),
            'resetpassword' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/resetpassword[/:action][/:par1]',
                    'defaults' => array(
                        'controller' => 'LoginAccess',
                        'action' => 'resetpassword',
                    ),
                ),
            ),
            'confirmresetpassword' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/confirmresetpassword[/:action][/:par1]',
                    'defaults' => array(
                        'controller' => 'LoginAccess',
                        'action' => 'confirmresetpassword',
                    ),
                ),
            ),
            'npwpdrd' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/npwpdrd[/:action][/:par1]',
                    'defaults' => array(
                        'controller' => 'LoginAccess',
                        'action' => 'npwpdrd',
                    ),
                ),
            ),
            'npwpdrdobjek' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/npwpdrdobjek[/:action][/:par1]',
                    'defaults' => array(
                        'controller' => 'LoginAccess',
                        'action' => 'npwpdrdobjek',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'pajak/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            'pajak' => __DIR__ . '/../view',
        ),
    ),
);
