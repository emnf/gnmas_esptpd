<?php

namespace Pajak\Helper;

use Zend\Debug\Debug;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class DendaHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{

    protected $tbl;

    public function __invoke()
    {
        return $this;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function hitungDenda($ts1, $ts2, $pajak, $persenpengurang = null)
    {
        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $day1 = date('d', $ts1);
        $day2 = date('d', $ts2);
        if ($day1 < $day2) {
            $tambahanbulan = 1;
        } else {
            $tambahanbulan = 0;
        }

        $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
        $persendenda = $year1 < 2024 ? 2 : 1;
        if ($jmlbulan > 24) {
            $jmlbulan = 24;
            $jmldenda = $jmlbulan * $persendenda / 100 * $pajak;
        } elseif ($jmlbulan <= 0) {
            $jmlbulan = 0;
            $jmldenda = $jmlbulan * $persendenda / 100 * $pajak;
        } else {
            $jmldenda = $jmlbulan * $persendenda / 100 * $pajak;
        }

        $jmldenda = ($persenpengurang != null) ? ($jmldenda - ($jmldenda * $persenpengurang / 100)) : $jmldenda;

        $response['jmldenda'] = $jmldenda;
        $response['jmlbulan'] = $jmlbulan;
        $response['persendenda'] = $persendenda;

        return $response;
    }
}
