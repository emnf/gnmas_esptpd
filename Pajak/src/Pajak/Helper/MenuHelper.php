<?php

namespace Pajak\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use DOMPDFModule\View\Model\PdfModel;

class MenuHelper extends AbstractHelper implements ServiceLocatorAwareInterface {

    protected $tbl;

    public function __invoke() {
        return $this;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function GetDataPemda() {

        $ar_pemda = $this->gettbl("PemdaTable")->getdata();

        return $ar_pemda;
    }

    public function GetDataBackground() {

        $bg = $this->gettbl("CustomLayoutTable")->get_bg();

        return $bg;
    }

    public function gettbl($tbl_service) {
        $this->tbl = $this->getServiceLocator()->getServiceLocator()->get($tbl_service);
        return $this->tbl;
    }

    //=========== warna header
    public function warnadefault($warna) {
        if (!empty($warna)) {
            $warnajudulheader = $warna;
        } else {
            $warna = '#0073b7';
        }

        return $warna;
    }

    public function GetDataLayout() {

        $warna = $this->gettbl("CustomLayoutTable")->get_layout();

        return $warna;
    }
    
    public function GetDataReklameSudutPandang($id) {

        $data = $this->gettbl("PendataanTable")->cek_sudutpandang_reklame($id);

        return $data;
    }
    
    public function getRekeningSelfesptpd() {

        $data = $this->gettbl("PendataanTable")->getRekeningSelfesptpd();

        return $data;
    }
    
    public function getnamapejabat($id){
        $ar_ttd = $this->gettbl('PejabatTable')->ceknamapejabat($id);
        return $ar_ttd;
    }
    
    public function jumlahtotalwp() {

        $data = $this->gettbl("LaporanTable")->jumlahtotalwp();

        return $data;
    }
    
    public function jumlahtotalobjekwp($t_jenisobjek = null) {

        $data = $this->gettbl("LaporanTable")->jumlahtotalobjekwp($t_jenisobjek);

        return $data;
    }
    
    public function datachartobjekwp() {

        $data = $this->gettbl("LaporanTable")->datachartobjekwp();

        return $data;
    }
    
    public function datachartobjekwpperkecamatan() {

        $data = $this->gettbl("LaporanTable")->datachartobjekwpperkecamatan();

        return $data;
    }
    
    public function datachartwpperkecamatan() {

        $data = $this->gettbl("LaporanTable")->datachartwpperkecamatan();

        return $data;
    }
    
    public function datachard_pelaporanobjekpajak($tahun) {

        $data = $this->gettbl("LaporanTable")->datachard_pelaporanobjekpajak($tahun);

        return $data;
    }
    
    
    public function datachard_realisasitarget($tahun) {

        $data = $this->gettbl("LaporanTable")->datachard_realisasitarget($tahun);

        return $data;
    }
    
    
    public function get_menu_user_esptpd_admin() {

        $dataobjekwp = $this->gettbl("LaporanTable")->menu_user_esptpd_admin();

        return $dataobjekwp;
    }
    
    public function get_menu_user_esptpd($idwp) {

        $dataobjekwp = $this->gettbl("LaporanTable")->menu_user_esptpd($idwp);

        return $dataobjekwp;
    }
    
    public function get_carijumlahpelaporan_admin($idobjek) {

        $dataobjekwp = $this->gettbl("LaporanTable")->carijumlahpelaporan_admin($idobjek);

        return $dataobjekwp;
    }
    
    public function get_carijumlahpelaporan($idwp,$idobjek) {

        $dataobjekwp = $this->gettbl("LaporanTable")->carijumlahpelaporan($idwp,$idobjek);

        return $dataobjekwp;
    }
    
    public function menu_user_esptpd_cetakkartudata($idwp) {

        $dataobjekwp = $this->gettbl("LaporanTable")->menu_user_esptpd_cetakkartudata($idwp);

        return $dataobjekwp;
    }
    
    public function get_menu_user_esptpd_subrek($idop, $idwp) {

        $dataobjekwp = $this->gettbl("LaporanTable")->menu_user_esptpd_subrek($idop, $idwp);

        return $dataobjekwp;
    }
    
    public function getmenu_opwp($s_idjenis, $idwp) {

        $dataobjekwp = $this->gettbl("LaporanTable")->getmenu_opwp($s_idjenis, $idwp);

        return $dataobjekwp;
    }
    
    public function detailwp($T_IDWP, $S_IDJENIS, $S_IDKOREK) {

        $datawp = $this->gettbl("LaporanTable")->detailwplogin($T_IDWP, $S_IDJENIS, $S_IDKOREK);

        return $datawp;
    }
    
    public function GetDataKecamatan($id) {

        $data = $this->gettbl("KecamatanTable")->getDataId($id);

        return $data;
    }
    
    public function GetDataKelurahan($id) {

        $data = $this->gettbl("KelurahanTable")->getDataId($id);

        return $data;
    }
    
    public function GetDataPeruntukanAirtanah($id) {

        $data = $this->gettbl("PembayaranTable")->getDataperuntukan($id);

        return $data;
    }
    
    public function getDatajenisketetapan() {

        $data = $this->gettbl("PembayaranTable")->getDatajenisketetapan();

        return $data;
    }
    
    public function getDatajenispajak() {

        $data = $this->gettbl("PembayaranTable")->getDatajenispajak();

        return $data;
    }
    
    /*public function getOneKecamatan($id) {

        $data = $this->gettbl("KecamatanTable")->getOneKecamatan($id);

        return $data;
    }
    
    public function getOneKelurahan($id) {

        $data = $this->gettbl("KelurahanTable")->getOneKelurahan($id);

        return $data;
    }*/

    public function headeratas($judul, $link) {
        $warna = $this->GetDataLayout();

        $headeratas = '<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary" style="border: 1px solid ' . $this->warnadefault($warna['warna_judul_box']) . ';">
                    <div class="box-header with-border" style="
    background: #0073b7 !important;
    background: -webkit-gradient(linear, left bottom, left top, color-stop(0, #0073b7), color-stop(1, #0089db)) !important;
    background: -ms-linear-gradient(bottom, #0073b7, #0089db) !important;
    background: -moz-linear-gradient(center bottom, ' . $this->warnadefault($warna['warna_judul_gradient_bawah']) . ' 0, ' . $this->warnadefault($warna['warna_judul_gradient_atas']) . ' 100%) !important;
    background: -o-linear-gradient(#0089db, #0073b7) !important;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#0089db\', endColorstr=\'#0073b7\', GradientType=0) !important;
    color: #fff;
">
                        <center><h3 class="box-title">' . $judul . '</h3> </center>
                        <div class="pull-right">         
                            ' . $link . '
                        </div>
                    </div>
                    <div class="box-body">    
    ';
        return $headeratas;
    }

    public function headeratas_end() {
        $headeratas_end = '</div>

                </div>
            </div>
        </div>
    </section>
</div>           ';

        return $headeratas_end;
    }

    
    public function fr_datatables($home, $idtabel, $url, $jmlkolom, $pencarian_bawah, $seleksi_kolom = null, $typeinput = null) {

        if (!empty($seleksi_kolom)) {
            $sortnyadiseleksi = $seleksi_kolom;
        } else {
            $sortnyadiseleksi = "0";
        }
        
        // "'8','9'"; //["8","9"];
        return '<script type="text/javascript">
                var seleksi_kolom = [' . $sortnyadiseleksi . '];
                 
                 $(document).ready(function () {
                        var dTable;
                        fr_datatables("' . $home . '","' . $idtabel . '", "' . $url . '", "' . $jmlkolom . '", "' . $pencarian_bawah . '",seleksi_kolom,"' . $typeinput . '");
                        
                });

                $(\'.sidebar-toggle\').click(function() {
                        $(\'.dataTables_scrollHeadInner\').attr(\'style\', \'width:100%;\');
                        $(\'.dataTable\').attr(\'style\', \'width:100%;\');
                        $(\'.dataTables_scrollFootInner\').attr(\'style\', \'width:100%;\');
                        //dTable.columns.adjust();

                });
               </script>';
    }
    
    

    public function kekata($x) {
        $x = abs($x);
        $angka = array("", "Satu", "Dua", "Tiga", "Empat", "Lima",
            "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = $this->kekata($x - 10) . " Belas";
        } else if ($x < 100) {
            $temp = $this->kekata($x / 10) . " Puluh" . $this->kekata($x % 10);
        } else if ($x < 200) {
            $temp = " Seratus" . $this->kekata($x - 100);
        } else if ($x < 1000) {
            $temp = $this->kekata($x / 100) . " Ratus" . $this->kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " Seribu" . $this->kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->kekata($x / 1000) . " Ribu" . $this->kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->kekata($x / 1000000) . " Juta" . $this->kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->kekata($x / 1000000000) . " Milyar" . $this->kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->kekata($x / 1000000000000) . " Trilyun" . $this->kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }

    function terbilang($x, $style = 4) {
        if ($x < 0) {
            $hasil = "MINUS " . trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }

}
