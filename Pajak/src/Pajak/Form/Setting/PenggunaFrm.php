<?php

namespace Pajak\Form\Setting;

use Zend\Form\Form;

class PenggunaFrm extends Form {

    public function __construct($ar_role = array(), $ar_wp = null
    , $s_main = null, $s_user = null, $s_laporan = null, $s_laporanbendahara = null, $ar_skpd = null) {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 's_iduser',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 's_username',
            'type' => 'text',
            'attributes' => array(
                'id' => 's_username',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 's_password',
            'type' => 'password',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 's_password',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_pass2',
            'type' => 'password',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 't_pass2',
                'required' => true
            )
        ));

		$this->add(array(
            'name' => 't_password_old',
            'type' => 'password',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 't_password_old',
                'required' => true
            )
        ));
		
        $this->add(array(
            'name' => 's_nama',
            'type' => 'text',
            'attributes' => array(
                'id' => 's_nama',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 's_email',
            'type' => 'email',
            'attributes' => array(
                'id' => 's_email',
                'class' => 'form-control',
//                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_jabatan',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 's_jabatan'
            )
        ));

        $this->add(array(
            'name' => 's_akses',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 's_akses',
                'class' => 'form-control',
                'required' => true,
                'onchange' => 'splitakses();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan pilih',
                'value_options' => $ar_role,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 's_wp',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 's_wp',
                'class' => 'form-control select2',
                'style' => 'width: 100%;',
                'onChange' => "cariobjekwp(this.value)"
            ),
            'options' => array(
                'empty_option' => 'Silahkan pilih',
                'value_options' => $ar_wp,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 's_skpd',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 's_skpd',
                'class' => 'form-control select2',
                'style' => 'width: 100%;',
            ),
            'options' => array(
                'empty_option' => 'Silahkan pilih',
                'value_options' => $ar_skpd,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 's_laporan',
            'options' => array(
                'value_options' => $s_laporan,
                'label'=>'Laporan WP'
            ),
            'attributes' => array(
                'id'=>'s_laporan',
                'class' => 'multi-select',
                'multiple' => 'multiple'
            ),            
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 's_main',
            'options' => array(
                'value_options' => $s_main,
                'label'=>'Halaman Utama'
            ),
            'attributes' => array(
                'id'=>'s_main',
                'class' => 'multi-select',
                'multiple' => 'multiple'
            ),            
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 's_user',
            'options' => array(
                'value_options' => $s_user,
                'label'=>'Menu Setting User'
            ),
            'attributes' => array(
                'id'=>'s_user',
                'class' => 'multi-select',
                'multiple' => 'multiple'
            ),            
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 's_laporanbendahara',
            'options' => array(
                'value_options' => $s_laporanbendahara,
                'label'=>'Menu Bendahara Dinas'
            ),
            'attributes' => array(
                'id'=>'s_laporanbendahara',
                'class' => 'multi-select',
                'multiple' => 'multiple'
            ),            
        ));

        $this->add(array(
            'type' => 'submit',
            'name' => 'simpan',
            'attributes' => array(
                'value' => 'Simpan',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}
