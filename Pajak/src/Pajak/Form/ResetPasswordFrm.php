<?php

namespace Pajak\Form;

use Zend\Form\Form;
use Zend\InputFilter;

class ResetPasswordFrm extends Form {

    public function __construct() {
        parent::__construct();

        $this->setAttribute('method', 'post');

        $this->add(array(
           'name'=>'s_email',
            'type'=>'email',
            'options'=>array(
//                'label'=>'Password'
            ),
            'attributes'=>array(
                'id' => 's_email',
                'class'=>'form-control',
                'placeholder'=>'Email',
                'required' => true
            )
        ));
        
        $this->add(array(
           'name'=>'Resetsubmit',
            'type'=>'Submit',
            'attributes'=>array(
                'value'=>'Kirim',
                'id'=>'Resetsubmit',
                'class'=>"btn btn-md btn-warning btn-block",
//                'onclick' => 'searchep()'
            )
        ));
    }

    
}