<?php

namespace Pajak\Form\Laporan;

use Zend\Form\Form;

class LaporanFrm extends Form {

    public function __construct($databulanbelum = null) {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 't_idtransaksi',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idtransaksi',
            )
        ));
        
        $this->add(array(
            'name' => 't_idskpd',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idskpd',
            )
        ));

        $this->add(array(
            'name' => 't_idobjek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idobjek',
            )
        ));

        $this->add(array(
            'name' => 't_idkorek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idkorek',
            )
        ));

        $this->add(array(
            'name' => 't_jenispajak',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_jenispajak',
            )
        ));
        
        $this->add(array(
            'name' => 't_operatorpendataan',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_operatorpendataan',
            )
        ));

        $this->add(array(
            'name' => 't_nourut',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nourut',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));
        
        $this->add(array(
            'name' => 't_namakegiatan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namakegiatan',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_masapajak',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_masapajak',
                'class' => 'form-control',
                // 'onchange' => 'CariLaporanTransaksi();',
                'required' => true
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $databulanbelum,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_periodepajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_periodepajak',
                'class' => 'form-control',
                'value' => date('Y'),
                'onchange' => 'CariLaporanByObjek();',
                'onblur' => 'CariLaporanByObjek();'
            )
        ));

        $this->add(array(
            'name' => 't_dasarpengenaan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_dasarpengenaan',
                'class' => 'form-control',
                'required' => true,
//                'readonly' => true,
                'style' => 'text-align:right',
                'onchange' => 'hitungpajak();this.value = formatCurrency(this.value);',
                'onblur' => 'hitungpajak();this.value = formatCurrency(this.value);',
                'onkeyup' => 'hitungpajak();this.value = formatCurrency(this.value);',
                'onKeyPress' => "return numbersonly(this, event);",
            )
        ));
		
		$this->add(array(
            'name' => 't_nilaiperolehan1',
            'type' => 'number',

            'attributes' => array(
                'id' => 't_nilaiperolehan1',
                'class' => 'form-control',
                //'required'=>false,
                'inputmode'=>'decimal',
                'step'=>'0.01',
                'min'=>'0',
                'max'=>'10',
                'value'=>0,
                'style' => 'text-align:right',
                'onchange' => 'ambilhargadasarwalet();hitungpajakwalet();',
                'onblur' => 'ambilhargadasarwalet();hitungpajakwalet();',
                'onkeyup' => 'ambilhargadasarwalet();hitungpajakwalet();',
               
            )
        ));
		
		$this->add(array(
            'name' => 't_hargadasar',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_hargadasar',
                'class' => 'form-control',
//                'required' => true,
//                'readonly' => true,
                'style' => 'text-align:right',
                'onchange' => 'hitungpajakwalet();this.value = formatCurrency(this.value);',
                'onblur' => 'hitungpajakwalet();this.value = formatCurrency(this.value);',
                'onkeyup' => 'hitungpajakwalet();this.value = formatCurrency(this.value);',
                'onKeyPress' => "return numbersonly(this, event);",
            )
        ));
		
		$this->add(array(
            'name' => 't_jenissarang',
            //'type' => Select::class,
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jenissarang',
                'class' => 'form-control',
                //'onchange' => 'gethargadasarwalet1();hitungpajakwalet();',
            //),
            //'options' => array(
            //    'empty_option' => 'Silahkan Pilih',
            //    'value_options' => $s_walet,
            //    'disable_inarray_validator' => true, // <-- disable
            )
        ));
		
		$this->add(array(
            'name' => 't_umurbangunan',
            //'type' => Select::class,
            'type' => 'text',
            'attributes' => array(
                'id' => 't_umurbangunan',
                'class' => 'form-control',
                //'onchange' => 'gethargadasarwalet1();hitungpajakwalet();',
            //),
            //'options' => array(
            //    'empty_option' => 'Silahkan Pilih',
            //    'value_options' => $s_walet,
            //    'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_tarifpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarifpajak',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));

//        $this->add(array(
//            'name' => 't_rincianomzet',
//            'type' => 'file',
//            'attributes' => array(
//                'id' => 't_rincianomzet',
//                'class' => 'form-control',
//                'required' => true,
//                'readonly' => false,
               // 'onChange' => 'readURL(this)',
//                'style' => 'text-align:right; border:none;  text-decoration: none;  font-size: 12px; '
//            )
//        ));
        
        
        
        $this->add(array(
            'name' => 't_jmlhpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlhpajak',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right; background:black; color: #24f711; padding: 5px 10px; height:40px; font-size: 16pt; font-weight:bolder'
            )
        ));
        
        $this->add(array(
            'name' => 'Laporansubmit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Simpan',
                'id' => 'Laporansubmit',
                'class' => "btn btn-warning btn-block",
//                'onclick' => 'this.disabled;simpan();'
            )
        ));
        
         $this->add(array(
            'name' => 't_namaskpd',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namaskpd',
                'class' => 'form-control',
            )
        ));
         
          $this->add(array(
            'name' => 't_namaobjek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namaobjek',
                'class' => 'form-control',
            )
        ));

           $this->add(array(
            'name' => 't_keterangankatering',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_keterangankatering',
                'class' => 'form-control',
            )
        ));
             $this->add(array(
            'name' => 't_asallistrik',
            'type' => 'Zend\Form\Element\Select',
            
            'attributes' => array(
                'id' => 't_asallistrik',
                'class' => 'form-control',
                // 'required' => true,
                'onChange' => 'opentabel();'
            ),
            'options' => array(
                'value_options' => [
                    1 => '01 || Pajak Penerangan Jalan PLN',
                    2 => '02 || Pajak Penerangan Jalan Non-PLN'
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

            $this->add(array(
            'name' => 't_opdkatering',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_opdkatering',
                'class' => 'form-control',
            )
        ));


          

    }
}
