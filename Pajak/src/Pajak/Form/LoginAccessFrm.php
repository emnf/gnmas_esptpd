<?php

namespace Pajak\Form;

use Zend\Form\Form,
    Zend\Form\Element\Captcha,
    Zend\Captcha\Image as CaptchaImage;
use Zend\InputFilter;

class LoginAccessFrm extends Form {

    public function __construct() {
        parent::__construct();

        $this->setAttribute('method', 'post');

        // $dirdata = './data';
        // //pass captcha image options
        // $captchaImage = new CaptchaImage(  array(
        //         'font' => $dirdata . '/fonts/arial.ttf',
        //         'width' => 200,
        //         'height' => 60,
        //         'timeout' => 300,
        //         'text-align' => 'center',
        //         'dotNoiseLevel' => 40,
        //         'lineNoiseLevel' => 5,
        //         'wordLen' => 5)
        // );
        // $captchaImage->setImgDir($dirdata.'/captcha');
        // $captchaImage->setImgUrl($dirdata.'/captcha');
        // //add captcha element...
        // $this->add(array(
        //     'type' => 'Zend\Form\Element\Captcha',
        //     'name' => 'captcha',
        //     'options' => array(
        //         // 'label' => 'Please verify you are human',
        //         'captcha' => $captchaImage,
        //     ),
        //     'attributes' => array(
        //         'style' => 'color:#000;margin-top:10px;font-size:10pt;',
        //         'class'=>'form-control',
        //         'placeholder'=>'Masukkan Kode Keamanan',
        //         // 'required' => true,
        //         'autocomplete' => 'off'
        //     )
        // ));

        $this->add(array(
            'name' => 's_username',
            'type' => 'text',
            'options'=>array(
//                'label'=>'Username'
            ),
            'attributes'=>array(
//                'id' => 's_username',
                'style' => 'font-size:10pt;',
                'class'=>'form-control',
                'placeholder'=>'Username',
                'required' => true
            )
        ));
        
        $this->add(array(
           'name'=>'s_password',
            'type'=>'password',
            'options'=>array(
//                'label'=>'Password'
            ),
            'attributes'=>array(
                'style' => 'font-size:10pt;',
                'class'=>'form-control',
                'placeholder'=>'Password',
                'required' => true
            )
        ));
        
        $this->add(array(
           'name'=>'Loginsubmit',
            'type'=>'Submit',
            'attributes'=>array(
                'value'=>'Login',
                'id'=>'Loginsubmit',
                'class'=>"btn btn-success btn-block",
//                'onclick' => 'searchep()'
            )
        ));
    }

    public function createInputFilter(){
        $inputFilter = new InputFilter\InputFilter();
        
        $username = new InputFilter\InputFilter('s_username');
        $username->setRequired(true);
        $inputFilter->add($username);
        
        $password = new InputFilter\InputFilter('s_password');
        $password->setRequired(true);
        $inputFilter->add($password);
        
        return $inputFilter;
    }
}