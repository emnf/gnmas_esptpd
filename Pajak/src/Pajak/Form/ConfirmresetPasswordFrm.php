<?php

namespace Pajak\Form;

use Zend\Form\Form;
use Zend\InputFilter;

class ConfirmresetPasswordFrm extends Form {

    public function __construct() {
        parent::__construct();

        $this->setAttribute('method', 'post');

        $this->add(array(
           'name'=>'s_passwordbaru',
            'type'=>'password',
            'options'=>array(
//                'label'=>'Password'
            ),
            'attributes'=>array(
                'id' => 's_passwordbaru',
                'class'=>'form-control',
                'placeholder'=>'Masukkan Password Baru',
                'required' => true
            )
        ));
        
        $this->add(array(
           'name'=>'s_passwordbarulagi',
            'type'=>'password',
            'options'=>array(
//                'label'=>'Password'
            ),
            'attributes'=>array(
                'id' => 's_passwordbarulagi',
                'class'=>'form-control',
                'placeholder'=>'Masukkan Password Baru Lagi',
                'required' => true
            )
        ));
        
        $this->add(array(
           'name'=>'Submit',
            'type'=>'submit',
            'attributes'=>array(
                'value'=>'Ubah Password',
                'id'=>'Submit',
                'class'=>"btn btn-md btn-warning btn-block",
//                'onclick' => 'searchep()'
            )
        ));
    }

    
}