<?php

namespace Pajak\Model\Pendaftaran;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class ObjekTable extends AbstractTableGateway {

    protected $table = 't_wpobjek';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new ObjekBase());
        $this->initialize();
    }

    // Kanggo
    public function getDataObjekById($t_idobjek) {
        $rowset = $this->select(array('t_idobjek' => $t_idobjek));
        $row = $rowset->current();
        return $row;
    }

    // Kanggo
    public function getDataObjekLeftMenu($t_idwp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wpobjek'
        ));
        $select->columns(array(
            "t_jenisobjek", "t_namaobjek", "t_idobjek"
        ));
        $where = new Where();
        $where->equalTo('t_idwp', $t_idwp);
        $where->literal('s_idjenis not in (4,8,6,9)');
        // $where->literal('a.t_objektutup is null');
        $select->where($where);
        $select->order('t_jenisobjek asc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    // Kanggo
    public function getObjekPajakbyIdObjek($idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "t_jenisobjek", "t_alamatlengkapobjek", "s_namajenis"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('b.t_idobjek', $idobjek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    //Kanggo
    public function getPendataanAwalbyIdObjek($idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_jenisobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_masaawal", "t_idkorek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            'd' => 'view_rekening'
        ), 'd.s_idkorek = c.t_idkorek', array(
            's_rinciankorek', 's_sub1korek'
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idwpobjek', $idobjek);
        $where->literal('c.is_deluserpendataan=0');
        $select->where($where);
        $select->order("c.t_masaawal asc");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    // Kanggo
    public function getPendataanbyMasa($month, $periodepajak, $idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "t_jenisobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_tglpenetapan", "t_tglpembayaran", "t_jmlhpembayaran"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "d.s_idkorek = c.t_idkorek", array(
            "korek", "s_namakorek", "s_rinciankorek", "s_sub1korek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idwpobjek', $idobjek);
        $where->literal("date_part('month', c.t_masaawal) ='" . str_pad($month, 2, '0', STR_PAD_LEFT) . "' and date_part('year',c.t_masaawal) ='" . $periodepajak . "'");
//        $where->literal('c.is_deluserpendataan is null');
        $where->equalTo('c.is_deluserpendataan', 0);
        $select->where($where);
        $select->order("c.t_masaawal asc");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    // Kanggo
    public function datanominalpelaporan($id) {
        $sql = "SELECT a.t_jenisobjek, a.t_namaobjek, a.t_idobjek, a.s_namajenissingkat, sum(b.t_jmlhpajak) as jumlahpajak,
                        (SELECT sum(d.t_jmlhpajak) as jml
                        from t_transaksi d 
                        WHERE a.t_idwp=" . $id . " and d.t_periodepajak='" . date('Y') . "' and d.t_idwpobjek=a.t_idobjek
						AND d.is_deluserpendataan=0) 
                        as jmlhpajakperiodeterakhir
                from view_wpobjek a
                left join t_transaksi b on b.t_idwpobjek=a.t_idobjek
                where a.t_idwp=" . $id . "
                and a.t_jenisobjek not in (4,8,6,9) 
                GROUP BY a.t_jenisobjek, a.t_namaobjek, a.t_idobjek, a.s_namajenissingkat 
                order by a.t_jenisobjek asc";
                //and a.t_objektutup is null
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }
    
    // Kanggo
    public function datanominalpelaporanadmin() {
        $sql = "SELECT a.s_idjenis as t_jenisobjek, NULL as t_namaobjek, a.s_namajenissingkat, sum(b.t_jmlhpajak) as jumlahpajak, 
                        (SELECT sum(c.t_jmlhpajak) as jmlhpajakperiodeterakhir 
                        from t_transaksi c 
                        WHERE c.t_jenispajak=a.s_idjenis and c.t_periodepajak='" . date('Y') . "' AND c.is_deluserpendataan=0) 
                        as jmlhpajakperiodeterakhir
                FROM s_jenisobjek a
                left JOIN t_transaksi b on b.t_jenispajak=a.s_idjenis
                where a.s_jenispungutan='Pajak' AND a.s_idjenis NOT in (4,8)
                
                GROUP BY a.s_idjenis ORDER BY a.s_idjenis";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    // Kanggo
    public function SaveDataKorek($post) {
        for ($i = 0; $i < count($post->t_idobjek); $i++) {
            $data['t_korekobjek'] = $post->t_idkorek[$i];
            $this->update($data, array('t_idobjek' => $post->t_idobjek[$i]));
        }
    }
    
    // Kanggo
    public function getDataObjekKorek($t_idwp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            'a' => 'view_wpobjek'
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "b.s_idkorek = a.t_korekobjek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idwp', $t_idwp);
//        $where->literal('a.t_idwp = '.$t_idwp.'');
//        $where->literal('a.t_objektutup is null');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        // var_dump($state);exit();
        return $res;
    }

    // Kanggo tambah
    public function getAllWp() {
        $sudah = "SELECT s_wp FROM esptpd_batang.s_users";
        $result = $this->adapter->query($sudah)->execute();
        $in = array();
        foreach ($result as $d)
        {

           $in[] = $d['s_wp'];
        }
//                    var_dump($in); exit();
        $in_implode = implode(',', $in);
        $sql = "SELECT * FROM view_wp where t_idwp not in ($in_implode)";
        $st = $this->adapter->query($sql);
        $rs = $st->execute();
		$ar_role = array();
        foreach ($rs as $key => $value) {
            $ar_role[$value['t_idwp']] = $value['t_npwpd'] . " || " . $value['t_nama'];
        }
        return $ar_role;
    }
    
    public function getAllWpSudah($sudah) {
        $sql = "SELECT * FROM view_wp where t_idwp not in ($sudah) and t_jenispendaftaran='P'";
        $st = $this->adapter->query($sql);
        $rs = $st->execute();
		$ar_role = array();
        foreach ($rs as $key => $value) {
            $ar_role[$value['t_idwp']] = $value['t_npwpd'] . " || " . $value['t_nama'];
        }
        return $ar_role;
    }
    
    public function getAllSkpdSudah($sudah) {
        $sql = "SELECT * FROM t_kantorskpd where t_idskpd not in ($sudah) and t_tutupskpd=0";
        $st = $this->adapter->query($sql);
        $rs = $st->execute();
		$ar_role = array();
        $nomor = 1;
        foreach ($rs as $key => $value) {
            $ar_role[$value['t_idskpd']] = $nomor++ . " || " . $value['t_namaskpd'];
        }
        return $ar_role;
    }
    
    //kanggo edit
    public function getAllWpEdit() {
        $sql = "SELECT * FROM view_wp where t_jenispendaftaran='P'";
        $st = $this->adapter->query($sql);
        $rs = $st->execute();
        foreach ($rs as $key => $value) {
            $ar_role[$value['t_idwp']] = $value['t_npwpd'] . " || " . $value['t_nama'];
        }
        return $ar_role;
    }
    
    public function getAllSkpdEdit() {
        $sql = "SELECT * FROM t_kantorskpd ";
        $st = $this->adapter->query($sql);
        $rs = $st->execute();
        $nomor = 1;
        foreach ($rs as $key => $value) {
            $ar_role[$value['t_idskpd']] = $nomor++ . " || " . $value['t_namaskpd'];
        }
        return $ar_role;
    }
    
    public function getRekeningById($id)
    {
        $id_korek = 0;
        if($id != null) $id_korek = $id;
        $sql = "SELECT * FROM s_rekening WHERE s_idkorek=$id_korek";
        $res = $this->adapter->query($sql)->execute()->current();
        return $res;
    }
    
    public function getPendataanKatering($periodepajak, $idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_tglpenetapan", "t_tglpembayaran", "t_jmlhpembayaran"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "d.s_idkorek = c.t_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idwpobjek', $idobjek);
        $where->literal("date_part('year',c.t_masaawal) ='" . $periodepajak . "'");
        $where->literal('c.is_deluserpendataan=0');
        $select->where($where);
        $select->order("c.t_tglpendataan asc");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function arskpd(){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_kantorskpd");
        $where = new Where();
        $where->literal("is_tutupskpd is null");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $data = array();
        foreach ($res as $d){
            $data[] = $d;
        }
        return $data;
    }

    public function getDataWP($t_idwp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->columns(array(
            "t_npwpd", "t_nama", "t_alamat_lengkap"
        ));
        $where = new Where();
        $where->equalTo('t_idwp', $t_idwp);
        // $where->literal('a.t_objektutup is null');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    
}
