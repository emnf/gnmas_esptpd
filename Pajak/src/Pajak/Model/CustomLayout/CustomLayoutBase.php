<?php

namespace Pajak\Model\CustomLayout;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CustomLayoutBase implements InputFilterAwareInterface {    
    
    public $id_bg_esptpd, $file_bg_esptpd, $s_iduser, $status_bg_esptpd;
     
    protected $inputFilter;    
    public function exchangeArray($data){        
        $this->id_bg_esptpd = (isset($data['id_bg_esptpd'])) ? $data['id_bg_esptpd'] : null;
        $this->file_bg_esptpd = (isset($data["file_bg_esptpd"])) ? $data["file_bg_esptpd"] : null;
        $this->s_iduser = (isset($data['s_iduser'])) ? $data['s_iduser'] : null;
        $this->status_bg_esptpd = (isset($data['status_bg_esptpd'])) ? $data['status_bg_esptpd'] : null;
    }
    
    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}