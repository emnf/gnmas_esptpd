<?php

namespace Pajak\Model\Bphtb;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class BphtbTable extends AbstractTableGateway {

    protected $table = '';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->initialize();
    }
    
    public function caritargetbphtb(){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_pemda");
        $where = new Where();
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function getrealisasibphtb($dataget){
       $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("view_cetaklaporan");
        $select->columns(array(
            "realisasi" => new \Zend\Db\Sql\Expression("SUM(total_bayar)"),
        ));
        $where = new Where();
        $where->literal("t_tanggalpembayaran is not null");
        $where->literal("EXTRACT('YEAR' from t_tanggalpembayaran) = ".$dataget['tahun']." ");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res; 
    }

}
