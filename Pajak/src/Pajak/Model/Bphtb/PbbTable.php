<?php

namespace Pajak\Model\Bphtb;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class PbbTable extends AbstractTableGateway {

    protected $table = '';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->initialize();
    }
    
    public function targetpbb(){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("SPPT");
        $select->columns(array(
            'target' => new \Zend\Db\Sql\Expression("SUM(PBB_YG_HARUS_DIBAYAR_SPPT)"),
        ));
        $where = new Where();
        $where->literal('THN_PAJAK_SPPT = '.  date('Y'.''));
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function realisasipbb_dengansppt(){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            'A' => 'PEMBAYARAN_SPPT'
        ));
        $select->columns(array(
                'realisasi' => new \Zend\Db\Sql\Expression('SUM( A.JML_SPPT_YG_DIBAYAR)')));
        $select->join(array(
            'B' => 'SPPT'
        ), 'A.KD_PROPINSI = B.KD_PROPINSI '
                . 'AND A.KD_DATI2 = B.KD_DATI2 '
                . 'AND A.KD_KECAMATAN = B.KD_KECAMATAN '
                . 'AND A.KD_KELURAHAN = B.KD_KELURAHAN '
                . 'AND A.KD_BLOK = B.KD_BLOK '
                . 'AND A.NO_URUT = B.NO_URUT '
                . 'AND A.THN_PAJAK_SPPT = B.THN_PAJAK_SPPT', array(), 'left');
        $where = new Where();
        $where->literal('A.THN_PAJAK_SPPT = '.  date('Y').' AND B.STATUS_PEMBAYARAN_SPPT = 1');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
//        echo $state->getSql();exit();
        $res = $state->execute()->current();
        return $res;
    }
   
    public function realisasipbb(){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            'A' => 'PEMBAYARAN_SPPT'
        ));
        $select->columns(array(
                'realisasi' => new \Zend\Db\Sql\Expression('SUM( A.JML_SPPT_YG_DIBAYAR)')));
        $where = new Where();
        $where->literal('A.THN_PAJAK_SPPT = '.  date('Y').'');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function cekgridpbb(){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("SPPT");
        $where = new Where();
        $where->literal('THN_PAJAK_SPPT = '.  date('Y'.''));
        $select->where($where);
        $select->limit((int) 10);
        $select->offset((int) 10);
        $state = $sql->prepareStatementForSqlObject($select);
        $Result = $state->execute();
        $array = array();
        foreach ($Result as $r){
            $array[] = $r;
        }
        var_dump($array);exit();
    }

}
