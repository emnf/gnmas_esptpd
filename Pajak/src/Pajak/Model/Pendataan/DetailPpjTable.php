<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class DetailPpjTable extends AbstractTableGateway {

    protected $table = 't_detailppj';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new DetailPpjBase());
        $this->initialize();
    }

   public function simpanpendataanppj($dataparent,$datapost) {
        // var_dump(count($datapost['t_kategori']));die();
        $this->delete(array('t_idtransaksi' => $dataparent['t_idtransaksi']));
        for ($i = 0; $i < (count($datapost['t_kategori'])+1); $i++) {
            if (!empty($datapost['t_kategori'][$i])) {
                $data = array(
                    "t_idtransaksi"=>(int)$dataparent['t_idtransaksi'],
                    "t_kapasitas"=>(int)$datapost['t_kapasitas'][$i],
                    "t_jam"=>(int)$datapost['t_jam'][$i],
                    "t_hari"=>(int)$datapost['t_hari'][$i],
                    "t_umur"=>(int)$datapost['t_umur'][$i],
                    "t_hargasatuan"=>(float)(str_ireplace(".", "",$datapost['t_hargasatuan'][$i])),
                    "t_jumlah"=>(int)$datapost['t_jumlah'][$i],
                    "t_detailhitung"=>$datapost['t_detailhitung'][$i],
                    "t_pajak"=>(float)(str_ireplace(".", "",$datapost['t_pajak'][$i])),
                    "t_kategori"=>(int)$datapost['t_kategori'][$i],
                    "t_kwh"=>(float)$datapost['t_kwh'][$i],
                    "t_biayabeban"=>(float)$datapost['t_biayabeban'][$i],
                    't_kategori'=>$datapost['t_kategori'][$i],
                    't_njtl'=>$datapost['t_njtl'][$i],
                );
            // var_dump($data);
                $this->insert($data);
            }
        }
       // die();
    }


    public function getDetailByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

      public function getPendataanByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

}
