<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class PendataanTable extends AbstractTableGateway {

    protected $table = 't_transaksi';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new PendataanBase());
        $this->initialize();
    }

    // Kanggo
    public function getPendataanId($t_idtransaksi) {
        $rowset = $this->select(array('t_idtransaksi' => $t_idtransaksi));
        $row = $rowset->current();
        return $row;
    }

    public function getRekeningSelfesptpd() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_jenisobjek");
        
        $where = new Where();
        $where->literal("s_idjenis IN (1,2,3,5,6,7,9) ");
        $select->where($where);
        $select->order('s_idjenis ASC');
//        echo $select->getSqlString(); exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    // Kanggo
    public function getDataPendataanID($t_idtransaksi) {

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan"
            , "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak"
            , "t_dasarpengenaan", "t_tarifpajak", "t_kodebayar","t_asallistrik",
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_wp"
                ), "a.t_idwp = c.t_idwp", array(
            "t_nama", "t_alamat", "t_alamat_lengkap", "t_npwpd", "t_kabupaten", "t_rt", "t_rw", "t_alamat"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "b.t_idkorek = d.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek", "s_rinciankorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('b.t_idtransaksi', $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function getDataNPWPDRD($npwpd_rd) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("view_wp");
        $where = new Where();
        $where->equalTo('t_npwpd', $npwpd_rd);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function getDataWPId($t_idwp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("view_wpobjek");
        $where = new Where();
        $where->equalTo('t_idwp', $t_idwp);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function getDataTransaksi($t_idjenispajak,$bulan_pajak,$periode_pajak,$pelaporan_dari,$status_bayar) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan"
            , "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak", "is_esptpd"
            , "t_dasarpengenaan", "t_tarifpajak", "t_kodebayar",'t_tglpembayaran',"t_jmlhpembayaran"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_wp"
                ), "a.t_idwp = c.t_idwp", array(
            "t_nama", "t_alamat", "t_alamat_lengkap", "t_npwpd", "t_kabupaten", "t_rt", "t_rw", "t_alamat"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "b.t_idkorek = d.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek", "s_rinciankorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('t_jenispajak', $t_idjenispajak);
        if(!empty($status_bayar)){
            if($status_bayar == 1){
                $where->isNotNull("t_tglpembayaran");
            }elseif($status_bayar == 2){
                $where->isNull("t_tglpembayaran");
            }
        }
        
        if(!empty($pelaporan_dari)){
            if($pelaporan_dari == 1){
                $where->equalTo("is_esptpd",1);
            }elseif($pelaporan_dari == 2){
                $where->equalTo("is_esptpd",0);
            }
        }
        
        if(!empty($bulan_pajak)){
            $where->literal("extract(month from t_masaawal) = '" . $bulan_pajak . "'");
        }
        $where->literal("extract(year from t_masaawal) = '" . $periode_pajak . "' and t_jmlhpajak <> 0");
        $select->where($where);
        $select->order('t_nourut DESC');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function getDataTransaksiWP($t_idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan"
            , "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak"
            , "t_dasarpengenaan", "t_tarifpajak", "t_kodebayar",'t_tglpembayaran',"t_jmlhpembayaran"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_wp"
                ), "a.t_idwp = c.t_idwp", array(
            "t_nama", "t_alamat", "t_alamat_lengkap", "t_npwpd", "t_kabupaten", "t_rt", "t_rw", "t_alamat"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "b.t_idkorek = d.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek", "s_rinciankorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('t_idwpobjek', $t_idobjek);
        $where->literal("extract(year from t_masaawal) = '" . date('Y') . "'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    //Pendataan by Admin
    public function getGridCount(PendataanBase $base, $s_idjenis) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('b.t_jenisobjek = ' . $s_idjenis . ' and c.t_tglpendataan is not null');
        $where->notEqualTo("t_nourut", 0);
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("$base->combocari LIKE '%$base->kolomcari%'");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $where->equalTo("c.is_deluserpendataan", 0);
//        $where->literal('c.is_deluserpendataan is null');
        $select->where($where);
        $select->order('t_nourut desc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(PendataanBase $base, $offset, $s_idjenis) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_jenispungutan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
                    "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak", "t_tglpembayaran", "t_jenispajak", "t_jmlhpembayaran", "t_tglpembayaran", 
                    "t_masaawal", "t_masaakhir", "is_esptpd"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "s_users"
                ), "c.t_operatorpendataan = d.s_iduser", array(
            "s_nama"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('b.t_jenisobjek = ' . $s_idjenis . ' and c.t_tglpendataan is not null');
        $where->notEqualTo("t_nourut", 0);
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("$base->combocari LIKE '%$base->kolomcari%'");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $where->equalTo("c.is_deluserpendataan", 0);
//        $where->literal('c.is_deluserpendataan is null');
        $select->where($where);
        if ($base->sortasc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortasc");
            } else {
                $select->order("t_nourut desc");
            }
        } elseif ($base->sortdesc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortdesc");
            } else {
                $select->order("t_nourut desc");
            }
        }
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
	
    public function getDataDetailPPJ($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_detailppj"
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_idkorek = b.s_idkorek", array(
            "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
	
    public function getDataDetailParkir($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_detailparkir"
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_idkorek = b.s_idkorek", array(
            "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
	
	public function getDataParkir($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi"
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_idkorek = b.s_idkorek", array(
            "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
		//var_dump($res);die;
        return $res;
    }

    public function getDataDetailMinerba($t_idtransaksi) {
		//var_dump($t_idtransaksi);die;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_detailminerba"
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_idkorek = b.s_idkorek", array(
            "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
		//var_dump($res);die;
        return $res;
    }
	
	public function getDataDetailWalet($id){
		$sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_detailwalet");
        $where = new Where();
        $where->equalTo("t_idtransaksi", $id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
	}

    public function gethargadasarppj($kva){
    $sql= "SELECT * FROM s_hargasatuanppj WHERE s_kva0 <='$kva' and s_kva1 >='$kva'";
    // var_dump($sql);die();
      $statement = $this->adapter->query($sql);
      $res = $statement->execute()->current();
      return $res;
    }
    public function getfaktordaya($idfaktor){
    $sql= "SELECT * FROM s_faktordayappj WHERE s_idfaktor='$idfaktor'";
    // var_dump($sql);die();
      $statement = $this->adapter->query($sql);
      $res = $statement->execute()->current();
      return $res;
    }
	
	
}
