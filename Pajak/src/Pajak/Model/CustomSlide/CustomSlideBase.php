<?php

namespace Pajak\Model\CustomSlide;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CustomSlideBase implements InputFilterAwareInterface {    
    
    public $id_bg_slide, $file_bg_slide, $s_iduser, $status_bg_slide;
     
    protected $inputFilter;    
    public function exchangeArray($data){        
        $this->id_bg_slide = (isset($data['id_bg_slide'])) ? $data['id_bg_slide'] : null;
        $this->file_bg_slide = (isset($data["file_bg_slide"])) ? $data["file_bg_slide"] : null;
        $this->s_iduser = (isset($data['s_iduser'])) ? $data['s_iduser'] : null;
        $this->status_bg_slide = (isset($data['status_bg_slide'])) ? $data['status_bg_slide'] : null;
    }
    
    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}