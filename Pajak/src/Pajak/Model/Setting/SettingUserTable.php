<?php

namespace Pajak\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class SettingUserTable extends AbstractTableGateway {

    protected $table = 's_users';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new SettingUserBase());
        $this->initialize();
    }

    //========================== data user pengguna
    public function getjumlahdata($select) {
        
        $sql = new Sql($this->adapter);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }
    
    
    public function semuadatapengguna($input, $aColumns, $session, $cekurl, $allParams) {
        
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_users");
        $where = new \Zend\Db\Sql\Where();
        $where->literal("s_akses in (1,2,3) ");
        //var_dump($input); exit();
        
        if(($input->getPost('sSearch_1')) || ($input->getPost('sSearch_1') == '0')){
              $where->literal("s_username ILIKE '%".$input->getPost('sSearch_1')."%'");
        }
        if(($input->getPost('sSearch_2')) || ($input->getPost('sSearch_2') == '0')){
              $where->literal("s_nama ILIKE '%".$input->getPost('sSearch_2')."%'");
        }
        if(($input->getPost('sSearch_3')) || ($input->getPost('sSearch_3') == '0')){
              $where->literal("s_noidentitas = '".$input->getPost('sSearch_3')."'");
        }
        if(($input->getPost('sSearch_4')) || ($input->getPost('sSearch_4') == '0')){
              $where->literal("s_akses = '".$input->getPost('sSearch_4')."'");
        }
        
        $select->where($where);
        
        //echo $select->getSqlString(); exit();
        
        //================ menghitung jumlah datane coy
        $totaldata = $this->getjumlahdata($select);
        $iTotal = $totaldata; 
        //================ end menghitung jumlah datane coy
        
        //================ ordernya coy
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                        $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))]." ".($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                    
                }
            }
        }
        
        
        if (!empty($aOrderingRules)) {
            $select->order(implode(", ", $aOrderingRules));
        } else {
            $select->order("s_iduser ASC");
        }
        //================ end ordernya coy
        
        //================ pagination e coy
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $select->limit(intval($input->getPost('iDisplayLength'))); $select->offset(intval($input->getPost('iDisplayStart')));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        }else{
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $select->limit(intval($input->getPost('iDisplayLength'))); $select->offset(intval($input->getPost('iDisplayStart'))); 
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $select->limit(10); $select->offset(0); 
                $no = 1;
            }
        }
        //================ end pagination e coy
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute();
        
        
        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal, 
            "aaData" => array(),
        );
        
        //var_dump($rResult); exit();

        foreach ($rResult as $aRow) {
            $row = array();
            
            $btn = '<a class="btn btn-success btn-xs" href="setting_user/edit?s_iduser='.$aRow['s_iduser'].'" title="Edit"><i class="fa fa-edit"></i> Edit</a> 
                    <a class="btn btn-danger btn-xs" href="#" onClick="hapus(' . $aRow['s_iduser'] .')" title="Hapus"><i class="fa fa-bitbucket"></i> Hapus</a>';
            
            if($aRow['s_akses'] == 1){
                $s_akses = 'Wajib Pajak';
            }elseif($aRow['s_akses'] == 2){
                $s_akses = 'Administrator';
            }elseif($aRow['s_akses'] == 3){
                $s_akses = 'OPD / SKPD';
            }
            
            $row = array($no, $aRow['s_username'], '<center>'.$aRow['s_nama'].'</center>', $aRow['s_noidentitas'], $s_akses, '<center>'.$btn.'</center>');
            $output['aaData'][] = $row;
            $no++;
        }
        
        return $output;
    }    
    
    public function semuadataSessionhislog($input, $aColumns, $session, $cekurl, $allParams) {
        
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from("member_session");
        $where = new \Zend\Db\Sql\Where();
        //var_dump($input); exit();
        if(($input->getPost('sSearch_1')) || ($input->getPost('sSearch_1') == '0')){
              $where->literal("s_username ILIKE '%".$input->getPost('sSearch_1')."%'");
        }
        if(($input->getPost('sSearch_2')) || ($input->getPost('sSearch_2') == '0')){
              $where->literal("s_nama ILIKE '%".$input->getPost('sSearch_2')."%'");
        }
        if(($input->getPost('sSearch_3')) || ($input->getPost('sSearch_3') == '0')){
              $where->literal("ip ILIKE '%".$input->getPost('sSearch_3')."%'");
        }
        
        $select->where($where);
        
        //echo $select->getSqlString(); exit();
        
        //================ menghitung jumlah datane coy
        $totaldata = $this->getjumlahdata($select);
        $iTotal = $totaldata; 
        //================ end menghitung jumlah datane coy
        
        //================ ordernya coy
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                        $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))]." ".($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                    
                }
            }
        }
        
        
        if (!empty($aOrderingRules)) {
            $select->order(implode(", ", $aOrderingRules));
        } else {
            $select->order("id_session DESC");
        }
        //================ end ordernya coy
        
        //================ pagination e coy
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $select->limit(intval($input->getPost('iDisplayLength'))); $select->offset(intval($input->getPost('iDisplayStart')));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        }else{
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $select->limit(intval($input->getPost('iDisplayLength'))); $select->offset(intval($input->getPost('iDisplayStart'))); 
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $select->limit(10); $select->offset(0); 
                $no = 1;
            }
        }
        //================ end pagination e coy
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute();
        
        
        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal, 
            "aaData" => array(),
        );
        
        //var_dump($rResult); exit();

        foreach ($rResult as $aRow) {
            $row = array();
            $row = array($no, $aRow['s_username'], $aRow['s_nama'], '<center>'.$aRow['ip'].'</center>', date('d-m-Y H:i:s', strtotime($aRow['login_time'])), date('d-m-Y H:i:s', strtotime($aRow['last_active'])));
            $output['aaData'][] = $row;
            $no++;
        }
        
        return $output;
    }    
    
    public function pilihwajibpajak($id){
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_wp");
        $select->join(["np1" => "s_kecamatan"], "np1.s_idkec = t_wp.t_kecamatan_npwpd", ["s_kodekec","s_namakec"], "left");
        $select->join(["np2" => "s_kelurahan"], "np2.s_idkel = t_wp.t_kelurahan_npwpd", ["s_kodekel","s_namakel",
                            "t_npwpd" => new \Zend\Db\Sql\Expression("(t_wp.t_jenispendaftaran::text || '.'::text || t_wp.t_bidangusaha || '.'::text || lpad(t_wp.t_nopendaftaran::text, 7, '0'::text) || '.'::text || lpad(np1.s_kodekec::text, 2, '0'::text) || '.'::text || lpad(np2.s_kodekel::text, 2, '0'::text))")
                ], "left");$where = new \Zend\Db\Sql\Where();
        $where->literal("t_idwp = ".$id." ");
        $select->where($where);
        //echo $select->getSqlString(); exit();
        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute()->current();
        return $rResult;
    }
    
    
    
     public function semuadatawpbelumdaftar($input, $aColumns, $session, $cekurl, $allParams) {
        
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_wp");
        //$select->join("t_wpobjek", "t_wpobjek.t_idwp = t_wp.t_idwp", ["*"], "left");
        /*$select->join("s_rekening", "s_rekening.s_idkorek = t_wpobjek.t_korekobjek", 
                ["s_tipekorek",
                  "s_kelompokkorek",
                  "s_jeniskorek",
                  "s_objekkorek",
                  "s_rinciankorek",
                  "s_sub1korek",  
                  "s_sub2korek",
                  "s_sub3korek",  
                  "s_namakorek",
                  "koderek" => new \Zend\Db\Sql\Expression("(s_rekening.s_tipekorek||'.'||s_rekening.s_kelompokkorek||'.'||s_rekening.s_jeniskorek||'.'||s_rekening.s_objekkorek||'.'||s_rekening.s_rinciankorek||'.'||s_rekening.s_sub1korek||'.'||s_rekening.s_sub2korek||'.'||s_rekening.s_sub3korek)")  
                ], 
                "left");*/
        //$select->join("s_kecamatan", "s_kecamatan.s_idkec = t_wpobjek.t_kecamatanobjek", ["s_kodekec","s_namakecobjek" => new \Zend\Db\Sql\Expression("s_kecamatan.s_namakec")], "left");
        //$select->join("s_kelurahan", "s_kelurahan.s_idkel = t_wpobjek.t_kelurahanobjek", ["s_kodekel","s_namakelobjek" => new \Zend\Db\Sql\Expression("s_kelurahan.s_namakel")], "left");
        $select->join(["np1" => "s_kecamatan"], "np1.s_idkec = t_wp.t_kecamatan_npwpd", ["s_kodekec","s_namakec"], "left");
        $select->join(["np2" => "s_kelurahan"], "np2.s_idkel = t_wp.t_kelurahan_npwpd", ["s_kodekel","s_namakel",
                            "t_npwpd" => new \Zend\Db\Sql\Expression("(t_wp.t_jenispendaftaran::text || '.'::text || t_wp.t_bidangusaha || '.'::text || lpad(t_wp.t_nopendaftaran::text, 7, '0'::text) || '.'::text || lpad(np1.s_kodekec::text, 2, '0'::text) || '.'::text || lpad(np2.s_kodekel::text, 2, '0'::text))")
                ], "left");
        
        $where = new \Zend\Db\Sql\Where();
        $where->literal("not exists (select * from s_users where s_wp = t_wp.t_idwp )");
        //var_dump($input); exit();
        
        if(($input->getPost('sSearch_1')) || ($input->getPost('sSearch_1') == '0')){
              $where->literal("(t_wp.t_jenispendaftaran::text || '.'::text || t_wp.t_bidangusaha || '.'::text || lpad(t_wp.t_nopendaftaran::text, 7, '0'::text) || '.'::text || lpad(np1.s_kodekec::text, 2, '0'::text) || '.'::text || lpad(np2.s_kodekel::text, 2, '0'::text)) ILIKE '%".$input->getPost('sSearch_1')."%'");
        }
        if(($input->getPost('sSearch_2')) || ($input->getPost('sSearch_2') == '0')){
              $where->literal("t_wp.t_nama_badan ILIKE '%".$input->getPost('sSearch_2')."%'");
        }
        if(($input->getPost('sSearch_3')) || ($input->getPost('sSearch_3') == '0')){
              $where->literal("t_wp.t_nama ILIKE '%".$input->getPost('sSearch_3')."%'");
        }
        if(($input->getPost('sSearch_4')) || ($input->getPost('sSearch_4') == '0')){
              $where->literal("t_wp.t_alamat ILIKE '%".$input->getPost('sSearch_4')."%'");
        }
        if(($input->getPost('sSearch_5')) || ($input->getPost('sSearch_5') == '0')){
              $tanggal = explode('-', $input->getPost('sSearch_5'));
            if (count($tanggal) > 1) {    
                if (count($tanggal) > 2) {
                    $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                    $where->literal("t_wp.t_tgldaftar::text ILIKE '%".$tanggalcari."%'");
                }else{
                    $tanggalcari = "" .$tanggal[1] . "-" . $tanggal[0] . "";
                    $where->literal("t_wp.t_tgldaftar::text ILIKE '%".$tanggalcari."%'");
                }
            }else{
                $where->literal("t_wp.t_tgldaftar::text ILIKE '%".$input->getPost('sSearch_5')."%'");
            }    
        }
        if(($input->getPost('sSearch_6')) || ($input->getPost('sSearch_6') == '0')){
              $where->literal("np1.s_namakec ILIKE '%".$input->getPost('sSearch_6')."%'");
        }
        if(($input->getPost('sSearch_7')) || ($input->getPost('sSearch_7') == '0')){
              $where->literal("np2.s_namakel ILIKE '%".$input->getPost('sSearch_7')."%'");
        }
        
        $select->where($where);
        
        //echo $select->getSqlString(); exit();
        
        //================ menghitung jumlah datane coy
        $totaldata = $this->getjumlahdata($select);
        $iTotal = $totaldata; 
        //================ end menghitung jumlah datane coy
        
        //================ ordernya coy
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                        $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))]." ".($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                    
                }
            }
        }
        
        
        if (!empty($aOrderingRules)) {
            $select->order(implode(", ", $aOrderingRules));
        } else {
            $select->order("t_wp.t_idwp ASC");
        }
        //================ end ordernya coy
        
        //================ pagination e coy
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $select->limit(intval($input->getPost('iDisplayLength'))); $select->offset(intval($input->getPost('iDisplayStart')));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        }else{
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $select->limit(intval($input->getPost('iDisplayLength'))); $select->offset(intval($input->getPost('iDisplayStart'))); 
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $select->limit(10); $select->offset(0); 
                $no = 1;
            }
        }
        //================ end pagination e coy
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute();
        
        
        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal, 
            "aaData" => array(),
        );
        
        //var_dump($rResult); exit();

        foreach ($rResult as $aRow) {
            $row = array();
            
            $btn = '<a class="btn btn-danger btn-xs" href="#" onClick="pilihWajibPajak(' . $aRow['t_idwp'] .')" title="PILIH WP"><i class="fa fa-hand-o-up"></i> PILIH</a>';
                    
            $row = array($no, $aRow['t_npwpd'], $aRow['t_nama_badan'], $aRow['t_nama'], $aRow['t_alamat'], date('d-m-Y', strtotime($aRow['t_tgldaftar'])), $aRow['s_namakec'], $aRow['s_namakel'], '<center>'.$btn.'</center>');
            $output['aaData'][] = $row;
            $no++;
        }
        
        return $output;
    } 
        
    //========================== end data user pengguna
    
    

    public function fetchAll() {
        $resultSet = $this->select();
        return $resultSet;
    }

    public function getUser(SettingUserBase $sb) {
        $rowset = $this->select(array('s_iduser' => $sb->s_iduser));
        $row = $rowset->current();
        return $row;
    }

    public function getUserId($id) {
        $rowset = $this->select(array('s_iduser' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function cekEmail($email) {
        $rowset = $this->select(array('s_email' => $email));
        $row = $rowset->current();
        return $row;
    }
    
	public function cekPassOld($pass, $session) {
        $rowset = $this->select(array('s_password' => md5($pass), 's_iduser' => $session['s_iduser']));
        $row = $rowset->current();
        return $row;
    }
	
    public function getUserEdit(SettingUserBase $sb) {
        $rowset = $this->select(array('s_username' => $sb->s_username, 's_tipe_pejabat' => $sb->s_tipe_pejabat, 's_idpejabat_idnotaris' => $sb->s_idpejabat_idnotaris));
        $row = $rowset->current();
        return $row;
    }

    public function saveData(SettingUserBase $sb) {
        if ($sb->s_akses == 1) {
            $arr_data = '(2),(30)'; // 2 = main , 30 = laporan
            $s_wp = $sb->s_wp;
            $s_skpd = 0;
        } elseif ($sb->s_akses == 2) {
            $arr_data = '(2),(21)'; // 2 = main , 21 = setting user
            $s_wp = 0;
            $s_skpd = 0;
        } elseif ($sb->s_akses == 3) {
            $arr_data = '(2),(31)'; // 2 = main , 30 = laporan
            $s_wp = 0;
            $s_skpd = $sb->s_skpd;
        }

        $data = array(
            's_username' => $sb->s_username,
            's_password' => md5($sb->s_password),
            's_akses' => (int) $sb->s_akses,
            's_nama' => $sb->s_nama,
            's_email' => $sb->s_email,
            's_menu' => $arr_data,
            's_wp' => $s_wp,
            's_skpd' => $s_skpd,
        );

        $id = $sb->s_iduser;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('s_iduser' => $sb->s_iduser));
        }
        $rowset = $this->select(array('s_username' => $sb->s_username,
            's_password' => md5($sb->s_password)));
        $row = $rowset->current();
        return $row;
    }

    public function saveresourcepermission($s_iduser, $s_akses) {
        $sql = "INSERT INTO permission_resource (s_iduser, s_idpermission) VALUES (" . $s_iduser . "," . $s_akses . ")";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function saveSessionLogin($s_iduser,$s_username,$s_nama) {
        $data = array(
            's_iduser' => $s_iduser,
            's_username' => $s_username,
            's_nama' => $s_nama,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'login_time' => date("Y-m-d H:i:s"),
            'last_active' => date("Y-m-d H:i:s")
        );
        $tabel_histlog = new \Zend\Db\TableGateway\TableGateway('member_session', $this->adapter);
        $tabel_histlog->insert($data);
    }
    
    public function saveHislogActivity($session, $hislog_action) {
        $data = array(
            's_iduser' => $session['s_iduser'],
            's_username' => $session['s_username'],
            's_nama' => $session['s_nama'],
            'ip' => $_SERVER['REMOTE_ADDR'],
            'hislog_time' => date("Y-m-d H:i:s"),
            'hislog_action' => $hislog_action
        );
        $tabel_histlog = new \Zend\Db\TableGateway\TableGateway('hislog_activity', $this->adapter);
        $tabel_histlog->insert($data);
    }
    
    public function getSessionlastLoginId($s_userid) {
        $sql = "select last_active FROM member_session WHERE s_iduser=" . $s_userid. " order by last_active desc limit 1";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }
    
    
    
//    public function savesession($session) {
//        $datahistlog = array(
//            'hislog_opr_id' => $session['s_iduser'],
//            'hislog_opr_user' => $session['s_username'] . "-" . $session['s_jabatan'],
//            'hislog_opr_nama' => $session['s_username'],
//            'hislog_time' => date("Y-m-d h:i:s")
//        );
//        $datahistlog['hislog_idspt'] = 0;
//        $datahistlog['hislog_action'] = "SIMPAN / EDIT DATA USER - " . $s_iduser;
//        $tabel_histlog = new \Zend\Db\TableGateway\TableGateway('history_log', $this->adapter);
//        $tabel_histlog->insert($datahistlog);
//    }

    public function deleteResourcePermision($s_iduser) {
        $sql = "DELETE FROM permission_resource WHERE s_iduser = " . $s_iduser;
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function CariAkses($role_akses) {
        $sql = "select id FROM permission WHERE permission_role like '%(" . $role_akses . ")%'";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

//    public function saveUserRole(SettingUserBase $userrole) {
//        $sql = "INSERT INTO user_role(user_id,role_id) VALUES (" . $userrole->s_iduser . "," . $userrole->s_akses . ")";
//        $st = $this->adapter->query($sql);
//        $rs = $st->execute();
//    }
//    public function saveRolePermission($role_id, $permission_id) {
//        $sql = "INSERT INTO role_permission(role_id, permission_id) VALUES ($role_id,$permission_id)";
//        $st = $this->adapter->query($sql);
//        $rs = $st->execute();
//    }

    public function getRole() {
        $sql = "SELECT * FROM role";
        $st = $this->adapter->query($sql);
        $rs = $st->execute();
        foreach ($rs as $key => $value) {
            $ar_role[$value['rid']] = $value['rid'].' || '.strtoupper($value['role_name']);
        }
        return $ar_role;
    }

    public function getPejabat() {
        $sql = "SELECT * FROM s_pejabat";
        $st = $this->adapter->query($sql);
        return $st->execute();
    }

    public function getuserdata($user) {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $select->join(array('role' => 'role'), 'role.rid = s_users.s_akses', array('rid', 'role_name'), 'LEFT');
//        $select->join(array('userRole' => 'user_role'), "userRole.user_id = role.rid", array('role_id'), 'LEFT');
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("s_username", $user);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }

    public function hapusData($id) {
        $this->delete(array('s_iduser' => $id));
        $sql = "DELETE FROM permission_resource WHERE s_iduser=$id";
        $st = $this->adapter->query($sql);
        $st->execute();
    }

    public function getPermission($resource_id) {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from('permission');
        $select->order('permission_name', 'asc');
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("resource_id", $resource_id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getPermissionAcc() {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from('permission');
        $select->order('permission_name', 'asc');
        $where = new \Zend\Db\Sql\Where();
//        $where->notIn("id", array(
//            1, 2, 3, 4, 5, 7, 8, 9, 10,
//            11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
//            21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
//            31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
//            41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
//            51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
//            61, 62, 63, 64, 65, 66, 67, 68
//        ));
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();

        $i = 0;
        $data_array = array();
        foreach ($res as $row) {
            $data_array[$i] = $row['id'];
            $i++;
        }
        return $data_array;
    }

    public function getResourcePermision($id) {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from('permission_resource');
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("s_iduser", $id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $returnArray = array();
        foreach ($res as $row) {
            $returnArray[] = $row['s_idpermission'];
        }
        return $returnArray;
    }

    public function savepassword(SettingUserBase $sb, $session) {
        $data = array(
            's_password' => md5($sb->s_password)
        );
        $this->update($data, array('s_iduser' => $session['s_iduser']));
    }

    public function getjmlpengguna() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->count();
        return $res;
    }

    public function getjmlpenggunatahun() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        $where->literal("year(s_tgldaftar) = '" . date('Y') . "'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->count();
        return $res;
    }

    public function getDataPermission(SettingUserBase $sb) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("permission");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function validasiToken($token){
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new \Zend\Db\Sql\Where ();
        $where->literal("s_passwordreset='$token'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function resetpassword($id) {
        $newpassword = \Zend\Math\Rand::getString(64, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', true);
        $this->update(array(
            "s_passwordreset" => $newpassword, 
            "s_passwordresetvalid" => date('Y-m-d H:i:s', strtotime("+15 minutes"))
            ), 
        array("s_iduser" => $id));
        return $newpassword;
    }

    public function getBy($key = array()) {
        return $this->select($key);
    }
    
    public function getUserByEmail($email) {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new \Zend\Db\Sql\Where ();
        $where->literal("s_email='$email'");
//        $where->equalTo("s_email", $email);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function validation($key) {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new \Zend\Db\Sql\Where ();
        $where->equalTo("s_passwordreset", $key);
        $where->greaterThanOrEqualTo("s_passwordresetvalid", date('Y-m-d H:i:s'));
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function changepassword($pass, $id) {
        $this->update(array("s_password" => md5($pass)), array("s_iduser" => $id));
    }
    
    public function getWpSudah()
    {
        $sudah = "SELECT s_wp FROM s_users";
        $result = $this->adapter->query($sudah)->execute();
        $in = array();
        foreach ($result as $d)
        {

           $in[] = $d['s_wp'];
        }
        
        $in_implode = implode(',', $in);
        return $in_implode;
    }
    
    public function getSkpdSudah()
    {
        $sudah = "SELECT s_skpd FROM s_users";
        $result = $this->adapter->query($sudah)->execute();
        $in = array();
        foreach ($result as $d)
        {

           $in[] = $d['s_skpd'];
        }
        
        $in_implode = implode(',', $in);
        return $in_implode;
    }
    
    //History Activity User
    public function semuadataHisaktifitas($input, $aColumns, $session, $cekurl, $allParams) {
        
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from("hislog_activity");
        $where = new \Zend\Db\Sql\Where();
        //var_dump($input); exit();
        if(($input->getPost('sSearch_1')) || ($input->getPost('sSearch_1') == '0')){
              $where->literal("s_username ILIKE '%".$input->getPost('sSearch_1')."%'");
        }
        if(($input->getPost('sSearch_2')) || ($input->getPost('sSearch_2') == '0')){
              $where->literal("s_nama ILIKE '%".$input->getPost('sSearch_2')."%'");
        }
        if(($input->getPost('sSearch_3')) || ($input->getPost('sSearch_3') == '0')){
              $where->literal("ip ILIKE '%".$input->getPost('sSearch_3')."%'");
        }
        
        $select->where($where);
        
        //echo $select->getSqlString(); exit();
        
        //================ menghitung jumlah datane coy
        $totaldata = $this->getjumlahdata($select);
        $iTotal = $totaldata; 
        //================ end menghitung jumlah datane coy
        
        //================ ordernya coy
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                        $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))]." ".($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                    
                }
            }
        }
        
        
        if (!empty($aOrderingRules)) {
            $select->order(implode(", ", $aOrderingRules));
        } else {
            $select->order("id_hislog DESC");
        }
        //================ end ordernya coy
        
        //================ pagination e coy
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $select->limit(intval($input->getPost('iDisplayLength'))); $select->offset(intval($input->getPost('iDisplayStart')));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        }else{
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $select->limit(intval($input->getPost('iDisplayLength'))); $select->offset(intval($input->getPost('iDisplayStart'))); 
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $select->limit(10); $select->offset(0); 
                $no = 1;
            }
        }
        //================ end pagination e coy
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute();
        
        
        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal, 
            "aaData" => array(),
        );
        
        //var_dump($rResult); exit();

        foreach ($rResult as $aRow) {
            $row = array();
            $row = array($no, $aRow['s_username'], $aRow['s_nama'], '<center>'.$aRow['ip'].'</center>', date('d-m-Y H:i:s', strtotime($aRow['hislog_time'])), $aRow['hislog_action']);
            $output['aaData'][] = $row;
            $no++;
        }
        
        return $output;
    }    
    
}
