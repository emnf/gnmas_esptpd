<?php

namespace Pajak\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class PemdaTable extends AbstractTableGateway {

    protected $table = "s_pemda";

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
//        $this->resultSetPrototype->setArrayObjectPrototype(new \Pajak\Model\Setting\PemdaBase());
        $this->initialize();
    }

    public function getdata() {
        $rowset = $this->select();
        $row = $rowset->current();
        return $row;
    }

}
