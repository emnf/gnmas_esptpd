<?php

namespace Pajak\Model\Laporan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class LaporanTable extends AbstractTableGateway
{

    protected $table = 't_transaksi';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new LaporanBase());
        $this->initialize();
    }

    // Laporan SPTPD Self Assesment
    public function simpanpendataanself(LaporanBase $base, $session, $post)
    {
        //var_dump($post);exit();
        $delay = \Zend\Math\Rand::getString(1, '12345', true);
        sleep($delay);

        $masaawal = $base->t_periodepajak . "-" . $base->t_masapajak . "-01";
        $masaakhir = date('Y-m-t', strtotime($masaawal));
        // Kode Provinsi dan Kab/Kota
        $kdProvkabkota = '6207';

        // if ($post['t_jenisobjekpajak'] == 5) {
        //     $base->t_idkorek = 59;
        // }else
        if ($post['t_jenisobjekpajak'] == 6) {
            $base->t_idkorek = 85;
        }

        // Jatuh Tempo Self Assesment
        $t_tgljatuhtempo = $this->geTglJatuhTempo($post['t_jenisobjekpajak']);
        $t_tgljatuhtempofix = date('Y-m-' . $t_tgljatuhtempo['t_akhirbayar'], strtotime("+1 month" . $masaawal));

        //        var_dump($post);exit();
        $data = array(
            't_idwpobjek' => $base->t_idobjek,
            't_idkorek' => $base->t_idkorek,
            't_jenispajak' => $base->t_jenispajak,
            't_periodepajak' => $base->t_periodepajak,
            't_tglpendataan' => date('Y-m-d'),
            't_masaawal' => $masaawal,
            't_masaakhir' => $masaakhir,
            't_dasarpengenaan' => (float) str_ireplace(".", "", $base->t_dasarpengenaan),
            't_tarifpajak' => (float) $base->t_tarifpajak,
            't_jmlhpajak' => (float) str_ireplace(".", "", $base->t_jmlhpajak),
            't_operatorpendataan' => $session['s_iduser'],
            //            't_operatorpendataan' => 2,
            't_tgljatuhtempo' => $t_tgljatuhtempofix,
            't_namakegiatan' => $base->t_namakegiatan,
            'is_esptpd' => 1,


        );
        if ($post['t_jenisobjekpajak'] == 2) {
            if ((int)$base->t_idkorek == 33 || (int)$base->t_idkorek = 34) {
                $data['t_opdkatering'] = $post['t_opdkatering'];
                $data['t_keterangankatering'] = $post['t_keterangankatering'];
            }
        } else if ($post['t_jenisobjekpajak'] == 9) {
            $data['t_nilaiperolehan'] = $post['t_nilaiperolehan1'];
            $data['t_jenissarang'] = $post['t_jenissarang'];
            $data['t_umurbangunan'] = $post['t_umurbangunan'];
        }

        if ($base->t_jenispajak == '5') { //ppj
            $data['t_asallistrik'] = $post['t_asallistrik'];
        }
        //var_dump($data);exit();
        if (!empty($post['t_idskpd'])) {
            $data['t_skpd'] = 1;
            $data['t_idskpd'] = $post['t_idskpd'];
            //            $data['t_namaskpd'] = $post['t_namaskpd'];
        }

        if (empty($base->t_idtransaksi)) {
            // Kode Bayar Self => 1-SPTPD
            $jenissurat = $this->getjenissurat(1);
            // No. Pendataan SPT
            $no = $this->getLaporanMaxByPeriode($base->t_periodepajak);
            $t_nourut = (int) $no['t_nourut'] + 1;
            $data['t_nourut'] = $t_nourut;
            $data['t_kodebayar'] = $kdProvkabkota . "" . str_pad($base->t_jenispajak, 2, "0", STR_PAD_LEFT) . ""
                . "" . str_pad($jenissurat['s_idsurat'], 2, "0", STR_PAD_LEFT) . ""
                . "" . substr($base->t_periodepajak, 2) . ""
                . "" . str_pad($t_nourut, 5, "0", STR_PAD_LEFT);
            $t_transaksi = new \Zend\Db\TableGateway\TableGateway('t_transaksi', $this->adapter);
            $t_transaksi->insert($data);

            //            $last = $t_transaksi->lastInsertValue;
        } else {
            $t_transaksi = new \Zend\Db\TableGateway\TableGateway('t_transaksi', $this->adapter);
            $t_transaksi->update($data, array('t_idtransaksi' => $base->t_idtransaksi));
            //            $last = $base->t_idtransaksi;
        }
        //        return $last;
        // if ($post['t_jenisobjekpajak'] == 6 || $post['t_jenisobjekpajak'] == 5) { //|| $post['t_jenisobjekpajak'] == 7 
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_transaksi");
        $select->columns(array(
            "t_idtransaksi"
        ));
        $where = new Where();
        if (empty($base->t_idtransaksi)) {
            $where->equalTo('t_nourut', $t_nourut);
        } else {
            $where->equalTo('t_idtransaksi', $base->t_idtransaksi);
        }
        $where->equalTo('t_tglpendataan', date('Y-m-d'));
        $where->equalTo('t_masaawal', $masaawal);
        $where->equalTo('t_masaakhir', $masaakhir);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
        // }
    }

    public function simpandetail($post, $dataparent, $i)
    {
        $data = array(
            't_tglomzet' => date('Y-m-d', strtotime($post['t_tglomzet'][$i])),
            't_nobill' => $post['t_nobill'][$i],
            't_omzet' => str_ireplace(".", "", $post['t_omzet'][$i]),
            't_idtransaksi' => $dataparent
        );
        $id = (int) $post['t_iddetailomzet'][$i];
        if ($id == 0) {
            $t_detailomzet = new \Zend\Db\TableGateway\TableGateway('t_detailomzet', $this->adapter);
            $t_detailomzet->insert($data);
        } else {
            $t_detailomzet = new \Zend\Db\TableGateway\TableGateway('t_detailomzet', $this->adapter);
            $t_detailomzet->delete(array('t_iddetailomzet' => $dataparent));
            $t_detailomzet->insert($data);
        }
    }

    public function simpandatawalet(LaporanBase $base, $session, $post)
    {
        //var_dump($base);die;

        $masaawal = $base->t_periodepajak . "-" . $base->t_masapajak . "-01";
        $masaakhir = date('Y-m-t', strtotime($masaawal));

        $kdProvkabkota = '6207';
        // Jika Inputan Berupa SKPD Jabatan
        if (!empty($post['t_tarifkenaikan'])) {
            $t_tarifkenaikan = $post['t_tarifkenaikan'];
            $t_jmlhkenaikan = str_ireplace(".", "", $post['t_jmlhkenaikan']);
        } else {
            $t_tarifkenaikan = 0;
            $t_jmlhkenaikan = 0;
        }

        $delay = \Zend\Math\Rand::getString(1, '12345', true);
        sleep($delay);

        // Jatuh Tempo Self Assesment
        $t_tgljatuhtempo = $this->geTglJatuhTempo($post['t_jenisobjekpajak']);
        $t_tgljatuhtempofix = date('Y-m-' . $t_tgljatuhtempo['t_akhirbayar'], strtotime("+1 month" . $masaawal));


        $data = array(
            't_idwpobjek' => $base->t_idobjek,
            't_idkorek' => $base->t_idkorek,
            't_jenispajak' => (float) $base->t_jenispajak,
            't_periodepajak' => (string) $base->t_periodepajak,
            't_tglpendataan' => date('Y-m-d'),
            't_masaawal' => $masaawal,
            't_masaakhir' => $masaakhir,
            //'t_tarifdasarkorek' => (float)str_ireplace(".", "", $base->t_tarifdasarkorek),
            //'t_nilaiperolehan' => (float) $base->t_nilaiperolehan,
            't_dasarpengenaan' => (float) str_ireplace(".", "", $base->t_dasarpengenaan),
            't_tarifpajak' => (float) $base->t_tarifpajak,
            't_jmlhpajak' => (float) str_ireplace(".", "", $base->t_jmlhpajak),
            't_operatorpendataan' => $session['s_iduser'],
            't_tgljatuhtempo' => $t_tgljatuhtempofix,
            't_tarifkenaikan' => (float) $t_tarifkenaikan,
            't_jmlhkenaikan' => (float) $t_jmlhkenaikan,
            't_jenissarang' => (int)$post['t_jenissarang'],
            't_umurbangunan' => (int)$post['t_umurbangunan'],
            'is_esptpd' => 1,
        );
        //		var_dump($data);die;
        $t_idtransaksi = $base->t_idtransaksi;
        if (empty($base->t_idtransaksi)) {
            // Kode Bayar Self => 1-SPTPD
            $jenissurat = $this->getjenissurat(1);
            // No. Pendataan SPT
            $no = $this->getLaporanMaxByPeriode($base->t_periodepajak);
            $t_nourut = (int) $no['t_nourut'] + 1;
            $data['t_nourut'] = $t_nourut;
            $data['t_kodebayar'] = $kdProvkabkota . "" . str_pad($base->t_jenispajak, 2, "0", STR_PAD_LEFT) . ""
                . "" . str_pad($jenissurat['s_idsurat'], 2, "0", STR_PAD_LEFT) . ""
                . "" . substr($base->t_periodepajak, 2) . ""
                . "" . str_pad($t_nourut, 5, "0", STR_PAD_LEFT);
            $t_transaksi = new \Zend\Db\TableGateway\TableGateway('t_transaksi', $this->adapter);
            $t_transaksi->insert($data);

            //            $last = $t_transaksi->lastInsertValue;
        } else {
            $t_transaksi = new \Zend\Db\TableGateway\TableGateway('t_transaksi', $this->adapter);
            $t_transaksi->update($data, array('t_idtransaksi' => $base->t_idtransaksi));
            //            $last = $base->t_idtransaksi;
        }

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_transaksi");
        $select->columns(array(
            "t_idtransaksi"
        ));
        $where = new Where();
        //$where->equalTo('t_kodebayar', $data['t_kodebayar']);
        //$where->equalTo('t_nourut', $t_nourut);
        $where->equalTo('t_tglpendataan', date('Y-m-d'));
        $where->equalTo('t_masaawal', $masaawal);
        $where->equalTo('t_masaakhir', $masaakhir);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function simpandetailwalet($dataparent, $post, $harga_dasar)
    {
        $t_idtransaksi = $dataparent['t_idtransaksi'];
        //var_dump($t_idtransaksi);die;
        $data = array(
            't_idtransaksi' => $dataparent['t_idtransaksi'],
            't_jenissarang1' => (int)$post['t_jenissarang'],
            't_nilaiperolehan1' => (float)$post['t_nilaiperolehan1'],
            't_jenissarang2' => 0,
            't_nilaiperolehan2' => 0,
            't_jenissarang3' => 0,
            't_nilaiperolehan3' => 0,
            't_umurbangunan' => (int)$post['t_umurbangunan'],
            't_tarifpajak' => (float)$post['t_tarifpajak'],
            't_totalpajak' => (float) str_ireplace(".", "", $post['t_jmlhpajak']),
            't_hargadasar1' => (int) str_ireplace(".", "", $post['t_hargadasar']),
            't_hargadasar2' => 0,
            't_hargadasar3' => 0,

        );
        //$id = (int) $dataparent;
        $cekdetail = $this->getDetailWalet($t_idtransaksi);
        //var_dump($cekdetail); die;
        if (empty($cekdetail)) {
            $t_detailwalet = new \Zend\Db\TableGateway\TableGateway('t_detailwalet', $this->adapter);
            $t_detailwalet->insert($data);
        } else {
            $t_detailwalet = new \Zend\Db\TableGateway\TableGateway('t_detailwalet', $this->adapter);
            $t_detailwalet->delete(array('t_idtransaksi' => $t_idtransaksi));
            //var_dump($data);die;
            $t_detailwalet->insert($data);
        }
    }

    public function getGridCount(LaporanBase $base, $s_idjenis, $t_idobjek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
        ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
        ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('b.t_jenisobjek = ' . $s_idjenis . ' and c.t_tglpendataan is not null');
        $where->equalTo("c.t_idwpobjek", $t_idobjek);
        $where->notEqualTo("t_nourut", 0);
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("$base->combocari LIKE '%$base->kolomcari%'");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $where->equalTo("c.is_deluserpendataan", 0);
        //        $where->literal('c.is_deluserpendataan is null');
        $select->where($where);
        $select->order('t_nourut desc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(LaporanBase $base, $offset, $s_idjenis, $t_idobjek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
        ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_jenispungutan"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
        ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak", "t_tglpembayaran", "t_jenispajak", "t_jmlhpembayaran", "t_tglpembayaran",
            "t_masaawal", "t_masaakhir", "is_esptpd", "t_kodebayar"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "s_users"
        ), "c.t_operatorpendataan = d.s_iduser", array(
            "s_nama"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('b.t_jenisobjek = ' . $s_idjenis . ' and c.t_tglpendataan is not null');
        $where->equalTo("c.t_idwpobjek", $t_idobjek);
        $where->notEqualTo("t_nourut", 0);
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("$base->combocari LIKE '%$base->kolomcari%'");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $where->equalTo("c.is_deluserpendataan", 0);
        //        $where->literal('c.is_deluserpendataan is null');
        $select->order("t_masaawal desc");
        $select->where($where);
        if ($base->sortasc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortasc");
            } else {
                $select->order("t_tglpendataan desc");
            }
        } elseif ($base->sortdesc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortdesc");
            } else {
                $select->order("t_tglpendataan desc");
            }
        }
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getLaporanSeMasaWp(LaporanBase $base, $t_idtransaksi)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wpobjek'
        ));
        $select->join(array(
            "b" => "t_transaksi"
        ), "b.t_idwpobjek = a.t_idobjek", array(
            "t_idwpobjek" => "t_idwpobjek"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal("extract(month from b.t_masaawal) = '" . $base->t_masapajak . "' and extract(year from b.t_masaawal) = '" . $base->t_periodepajak . "' and b.t_idwpobjek= " . $base->t_idobjek);
        //        $where->literal("date_part('month',b.t_masaawal) = '" . $base->t_masapajak . "' and date_part('year',b.t_masaawal) = '" . $base->t_periodepajak . "' and b.t_idwpobjek= " . $base->t_idobjek);
        $where->notEqualTo("b.t_idtransaksi", (int) $t_idtransaksi);
        //        $where->literal('b.is_deluserpendataan is null');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataLaporanID($t_idtransaksi)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
        ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idtransaksi", "t_idkorek", "t_idwpobjek", "t_nourut", "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak", "t_dasarpengenaan", "t_tarifpajak", "t_kodebayar", "t_tgljatuhtempo", "t_tglpembayaran"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_wp"
        ), "a.t_idwp = c.t_idwp", array(
            "t_nama", "t_alamat", "t_alamat_lengkap", "t_npwpd", "t_kabupaten", "t_nama"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
        ), "b.t_idkorek = d.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek", "s_rinciankorek"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('b.t_idtransaksi', $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataLaporanSebelumnya($t_jenisobjek, $t_idwpobjek, $t_masaawal)
    {
        $date = explode("-", date("Y-m-d", strtotime($t_masaawal . "-1 month")));
        $tgl = $date[2];
        $bln = $date[1];
        $thn = $date[0];
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
        ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak", "t_dasarpengenaan", "t_tarifpajak"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_jenisobjek', $t_jenisobjek);
        $where->equalTo('b.t_idwpobjek', $t_idwpobjek);
        $where->literal("date_part('month',b.t_masaawal) = '" . $bln . "' and date_part('year',b.t_masaawal) = '" . $thn . "'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getLaporanMax()
    {
        $sql = "select max(t_nourut) as t_nourut from t_transaksi";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getLaporanMaxByPeriode($tahun)
    {
        $sql = "select max(t_nourut) as t_nourut from t_transaksi where t_periodepajak ='$tahun' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }


    public function geTglJatuhTempo($t_jenisobjekpajak)
    {
        $sql = "select t_akhirbayar from s_jenisobjek where s_idjenis='" . $t_jenisobjekpajak . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getjenissurat($s_idsurat)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_jenissurat");
        $where = new Where();
        $where->equalTo("s_idsurat", $s_idsurat);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getKorekWp($t_idwpobjek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "view_rekening"
        ), "a.t_korekobjek = b.s_idkorek", array(
            "s_idkorek", "s_persentarifkorek", "s_jenisobjek"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idobjek', $t_idwpobjek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getKorekWpObjek($t_idwpobjek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi"
        ));
        $select->join(array(
            "b" => "view_rekening"
        ), "a.t_idkorek = b.s_idkorek", array(
            "s_idkorek", "s_persentarifkorek", "s_jenisobjek"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idwpobjek', $t_idwpobjek);
        $select->where($where);
        $select->limit(1);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPendaftaranbyIdObjek($t_idobjek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
        ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_jenisobjek", "t_nop", "t_namaobjek", "t_rtobjek", "t_rwobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_namajenis", "t_latitudeobjek",
            "t_longitudeobjek", "t_alamatlengkapobjek", "t_korekobjek", "t_tgldaftarobjek"
        ), $select::JOIN_LEFT);
        $select->join(array(
            'c' => 's_rekening'
        ), 'c.s_idkorek = b.t_korekobjek', array(
            's_rinciankorek', 's_sub1korek', 's_objekkorek'
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('b.t_idobjek', $t_idobjek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPendaftaranbyIdTransaksi($t_idtransaksi)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
        ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_jenisobjek", "t_nop", "t_namaobjek", "t_rtobjek", "t_rwobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_namajenis", "t_latitudeobjek",
            "t_longitudeobjek", "t_alamatlengkapobjek", "t_korekobjek", "t_tgldaftarobjek"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
        ), "b.t_idobjek = c.t_idwpobjek", array(
            "*",
        ), $select::JOIN_LEFT);
        $select->join(array(
            'd' => 's_rekening'
        ), 'd.s_idkorek = c.t_idkorek', array(
            's_objekkorek', 's_rinciankorek'
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idtransaksi', $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataPembayaranID($t_idtransaksi)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
        ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek", "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_namajenispajak" => "s_namajenis", "s_jenispungutan", "t_jenisobjek"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
        ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_tglpembayaran", "t_jmlhpembayaran", "t_nourut", "t_nopembayaran",
            "t_masaawal", "t_masaakhir", "t_periodepajak", "t_jmlhpajak", "t_kodebayar", "t_masaawal",
            "t_masaakhir", "t_jenispajak", "t_namakegiatan", "t_tgljatuhtempo", "t_jmlhdendapembayaran", "t_sanksiadm"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
        ), "d.s_idkorek = c.t_idkorek", array(
            "korek", "s_namakorek", "s_persentarifkorek", "s_namajenis"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idtransaksi', $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getBulanBelumLapor($t_idobjek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_transaksi'
        ));
        $select->columns(array(
            't_masaawal'
        ));
        $where = new Where();
        $where->equalTo('t_idwpobjek', $t_idobjek);
        $where->equalTo('t_periodepajak', date('Y'));
        $where->literal('a.is_deluserpendataan is null');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $selectData = array();
        $abulan = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
        foreach ($res as $row) {
            $selectData[date('m', strtotime($row['t_masaawal']))] = $abulan[date('m', strtotime($row['t_masaawal']))];
        }
        $abulanpakai = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
        $pakai = array_diff_key($abulanpakai, $selectData);
        return $pakai;
    }

    public function getBulanBelumLaporEdit($t_idobjek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_transaksi'
        ));
        $select->columns(array(
            't_masaawal'
        ));
        $where = new Where();
        $where->equalTo('t_idwpobjek', $t_idobjek);
        $where->equalTo('t_periodepajak', date('Y'));
        $where->literal('a.is_deluserpendataan=0');
        $where->literal('a.t_tglpembayaran IS NOT NULL ');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $selectData = array();
        $abulan = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
        foreach ($res as $row) {
            $selectData[date('m', strtotime($row['t_masaawal']))] = $abulan[date('m', strtotime($row['t_masaawal']))];
        }
        $abulanpakai = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
        $pakai = array_diff_key($abulanpakai, $selectData);
        return $pakai;
    }

    public function cekBelumLapor($t_idobjek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_transaksi'
        ));
        $select->columns(array(
            't_masaawal'
        ));
        $where = new Where();
        $where->equalTo('a.t_idwpobjek', $t_idobjek);
        $where->equalTo('a.is_deluserpendataan', 0);
        //        $where->literal('a.is_deluserpendataan is null');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function cekPelaporanSebelumnya($t_jenisobjek, $t_idwpobjek, $t_masapendataan)
    {
        $date = date("Y-m-d", strtotime($t_masapendataan . "-1 month"));
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
        ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak", "t_dasarpengenaan", "t_tarifpajak"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_jenisobjek', $t_jenisobjek);
        $where->equalTo('b.t_idwpobjek', $t_idwpobjek);
        $where->literal("b.t_masaawal = '" . $date . "'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function griddatalaporanskpd($input, $aColumns, $session, $cekurl, $allParams)
    {
        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = intval($input->getPost('iDisplayLength'));
                $sOffset = intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = 10;
                $sOffset = 0;
                $no = 1;
            }
        }

        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = implode(", ", $aOrderingRules);
        } else {
            $sOrder = "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "  LIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "  LIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "  LIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "  LIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "  LIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    $aFilteringRules[] = " " . $aColumns[$i] . "  LIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }



        if (!empty($aFilteringRules)) {
            $sWhere = implode(" AND ", $aFilteringRules);
        } else {
            $sWhere = "";
        }
        //        var_dump($sWhere);exit();

        //====== select data grid
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_transaksi");
        $select->join("view_wpobjek", "t_transaksi.t_idwpobjek = view_wpobjek.t_idobjek", array(
            "t_namaobjek",
        ), "left");
        $select->join("view_wp", "view_wp.t_idwp = view_wpobjek.t_idwp", array(
            "t_nama", "t_npwpd",
        ), "left");
        $where = new Where();
        $where->equalTo('t_transaksi.is_deluserpendataan', 0);
        $where->equalTo('t_transaksi.t_idskpd', $session['s_skpd']);
        if ($sWhere != '') {
            $where->literal(" " . $sWhere . " ");
        }
        $select->where($where);
        $select->order('t_transaksi.t_nourut DESC');
        $select->order('t_transaksi.t_tglpendataan DESC');
        $select->limit((int) $sLimit);
        $select->offset((int) $sOffset);
        $state = $sql->prepareStatementForSqlObject($select);
        $rResult = $state->execute();
        //===== select data grid end

        //===== select count data
        $count = $sql->select();
        $count->from("t_transaksi");
        $count->join("view_wpobjek", "t_transaksi.t_idwpobjek = view_wpobjek.t_idobjek", array(
            "t_namaobjek",
        ), "left");
        $count->join("view_wp", "view_wp.t_idwp = view_wpobjek.t_idwp", array(
            "t_nama",
        ), "left");
        $wherecount = new Where();
        $wherecount->equalTo('t_transaksi.is_deluserpendataan', 0);
        $wherecount->equalTo('t_transaksi.t_idskpd', $session['s_skpd']);
        if ($sWhere != '') {
            $wherecount->literal(" " . $sWhere . " ");
        }
        $count->where($wherecount);
        $countsql = $sql->prepareStatementForSqlObject($count);
        $totaldata = $countsql->execute()->count();

        //===== select count data end

        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];

        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );
        //        var_dump($allParams);exit();
        foreach ($rResult as $aRow) {
            $row = array();

            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }
            $tglpembayaran = '<span style="color:green;font-weight:bold">Lunas /(' . date('d-m-Y', strtotime($aRow['t_tglpembayaran'])) . ')<br>' .  number_format($aRow['t_jmlhpembayaran'] + $aRow['t_jmlhbayardenda'], 0, ',', '.') . '</span>';
            if (empty($aRow['t_tglpembayaran'])) {
                $tglpembayaran = '<span style="color:red;font-weight:bold">Belum Bayar</span>';
            }

            $is_esptpd = ($aRow['is_esptpd'] != 0 ? '<b style="color:#db403c">e-SPTPD</b>' : '<b style="color:#3850b8">SIMPATDA</b>');

            if (empty($aRow['t_tglpembayaran'])) {
                if ($aRow['t_jenispajak'] == 1) {
                    $sptpd = "<a href='cetaksptpdhotel?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($aRow['t_jenispajak'] == 2) {
                    $sptpd = "<a href='cetaksptpdrestoran?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($aRow['t_jenispajak'] == 3) {
                    $sptpd = "<a href='cetaksptpdhiburan?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($aRow['t_jenispajak'] == 6) {
                    $sptpd = "<a href='cetaksptpdminerba?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($aRow['t_jenispajak'] == 5) {
                    $sptpd = "<a href='cetaksptpdppj?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($aRow['t_jenispajak'] == 7) {
                    $sptpd = "<a href='cetaksptpdparkir?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                }
                $kodebayar = "<a href='cetakkodebayar?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak Kode Bayar'><i class='glyph-icon icon-print'></i> KODE BAYAR</a>";
                $sspd = "";
                $edit = "<a href='formpageskpdedit?&t_idtransaksi=$aRow[t_idtransaksi]' class='btn btn-primary btn-xs' ><i class='glyph-icon icon-pencil'></i> EDIT</a><br>";
            } else {
                if ($aRow['t_jenispajak'] == 1) {
                    $sptpd = "<a href='cetaksptpdhotel?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($aRow['t_jenispajak'] == 2) {
                    $sptpd = "<a href='cetaksptpdrestoran?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($aRow['t_jenispajak'] == 3) {
                    $sptpd = "<a href='cetaksptpdhiburan?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($aRow['t_jenispajak'] == 6) {
                    $sptpd = "<a href='cetaksptpdminerba?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($aRow['t_jenispajak'] == 5) {
                    $sptpd = "<a href='cetaksptpdppj?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($aRow['t_jenispajak'] == 7) {
                    $sptpd = "<a href='cetaksptpdparkir?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } else {
                    $sptpd = "";
                }
                $kodebayar = "<a href='cetakkodebayar?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-warning btn-xs' title='Cetak Kode Bayar'><i class='glyph-icon icon-print'></i> KODE BAYAR</a>";
                $sspd = "<a href='cetaksspd?&t_idtransaksi=$aRow[t_idtransaksi]' target='_blank' class='btn btn-success btn-xs' title='Cetak SSPD'><i class='glyph-icon icon-print'></i> SSPD</a>";
                $edit = "";
            }


            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $aRow['t_nourut'] . "<br>" . $is_esptpd . "</center>",
                "<center>" . $aRow['t_npwpd'] . "</center>",
                "<center>" . $aRow['t_nama'] . "</center>",
                "<center>" . $aRow['t_namaobjek'] . "</center>",
                "<center>" . date('d-m-Y', strtotime($aRow['t_masaawal'])) . " s.d " . date('d-m-Y', strtotime($aRow['t_masaakhir'])) . "</center>",
                "<center>" . date('d-m-Y', strtotime($aRow['t_tglpendataan'])) . "</center>",
                "<center>" . number_format($aRow['t_jmlhpajak'], 0, ',', '.') . "</center>",
                "<center>" . $tglpembayaran . "</center>",
                "<center>" . $edit . ' ' . $sptpd . ' ' . $kodebayar . ' ' . $sspd . "</center>"
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    public function getdataskpd($session)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_kantorskpd");
        $where = new Where();
        $where->equalTo('t_idskpd', $session['s_skpd']);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function datagridobjekpajak($input, $aColumns, $session, $cekurl, $allParams)
    {
        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            // $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $sLimit = intval($input->getPost('iDisplayLength'));
            $sOffset = intval($input->getPost('iDisplayStart'));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = intval($input->getPost('iDisplayLength'));
                $sOffset = intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = 10;
                $sOffset = 0;
                $no = 1;
            }
        }

        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = implode(", ", $aOrderingRules);
        } else {
            $sOrder = "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "  LIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "  LIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "  LIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "  LIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "  LIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    $aFilteringRules[] = " " . $aColumns[$i] . "  LIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }



        if (!empty($aFilteringRules)) {
            $sWhere = implode(" AND ", $aFilteringRules);
        } else {
            $sWhere = "";
        }
        //        var_dump($sWhere);exit();

        //====== select data grid
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("view_wpobjek");
        $select->join("view_wp", "view_wp.t_idwp = view_wpobjek.t_idwp", array(
            "t_nama",
        ), "left");
        $where = new Where();
        $where->equalTo('view_wpobjek.t_jenisobjek', 2);
        //        $where->isNull('view_wpobjek.t_objektutup');
        if ($sWhere != '') {
            $where->literal(" " . $sWhere . " ");
        }
        $select->where($where);
        $select->order('view_wpobjek.t_idobjek');
        $select->limit((int) $sLimit);
        $select->offset((int) $sOffset);
        $state = $sql->prepareStatementForSqlObject($select);
        $rResult = $state->execute();
        //===== select data grid end

        //===== select count data
        $count = $sql->select();
        $count->from("view_wpobjek");
        $count->join("view_wp", "view_wp.t_idwp = view_wpobjek.t_idwp", array(
            "t_nama",
        ), "left");
        $wherecount = new Where();
        $wherecount->equalTo('view_wpobjek.t_jenisobjek', 2);
        //        $where->isNull('view_wpobjek.t_objektutup');
        if ($sWhere != '') {
            $wherecount->literal(" " . $sWhere . " ");
        }
        $count->where($wherecount);
        $countsql = $sql->prepareStatementForSqlObject($count);
        $totaldata = $countsql->execute()->count();

        //===== select count data end

        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];

        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );
        //        var_dump($allParams);exit();
        foreach ($rResult as $aRow) {
            $row = array();

            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }

            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $aRow['t_nama'] . "</center>",
                "<center>" . $aRow['t_namaobjek'] . "</center>",
                "<center>" . $aRow['s_namajenis'] . "</center>",
                "<button type='button' class='btn btn-xs btn-primary' onclick='pilihop(" . $aRow['t_idobjek'] . ")'><span class='glyphicon glyphicon-check'></span> Pilih</button>",
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    public function getdatarealisasi($dataget)
    {
        $tahun = $dataget['tahun'];
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "x" => 's_jenisobjek'
        ));
        $select->columns(array(
            "*",
            "target" => new \Zend\Db\Sql\Expression("(SELECT s_targetjumlah FROM s_target f
            LEFT JOIN s_targetdetail g on g.s_idtargetheader = f.s_idtarget
            LEFT JOIN s_rekening h on h.s_idkorek = g.s_targetrekening
            WHERE 
            -- f.s_statustarget = 1 AND 
            h.s_rinciankorek = '00' AND f.s_tahuntarget = '$tahun' AND h.s_jenisobjek = x.s_idjenis)"),

            "transaksi" => new \Zend\Db\Sql\Expression("(SELECT COALESCE(SUM(b.t_jmlhpembayaran),0) FROM t_transaksi b
            WHERE b.t_jenispajak = x.s_idjenis AND YEAR(b.t_tglpembayaran) = '$tahun' AND b.is_deluserpendataan is NULL)"),

            "transaksidenda" => new \Zend\Db\Sql\Expression("(SELECT COALESCE(SUM(b.t_jmlhbayardenda),0) FROM t_transaksi b
            WHERE b.t_jenispajak = x.s_idjenis AND YEAR(b.t_tglbayardenda) = '$tahun' AND b.is_deluserpendataan is NULL)"),

            "skpdkb" => new \Zend\Db\Sql\Expression("(SELECT COALESCE(SUM(a.t_jmlhbayarskpdkb),0) FROM t_skpdkb a
            LEFT JOIN t_transaksi b on b.t_idtransaksi = a.t_idtransaksi
            WHERE b.t_jenispajak = x.s_idjenis AND b.is_deluserpendataan is null AND a.is_deluserskpdkb is NULL 
            AND YEAR(a.t_tglbayarskpdkb) = '$tahun')"),

            "skpdkbdenda" => new \Zend\Db\Sql\Expression("(SELECT COALESCE(SUM(a.t_bungaketerlambatanbayarskpdkb),0) FROM t_skpdkb a
            LEFT JOIN t_transaksi b on b.t_idtransaksi = a.t_idtransaksi
            WHERE b.t_jenispajak = x.s_idjenis AND b.is_deluserpendataan is null AND a.is_deluserskpdkb is NULL 
            AND YEAR(a.t_tglbayarskpdkb) = '$tahun')"),

            "skpdkbt" => new \Zend\Db\Sql\Expression("(SELECT COALESCE(SUM(a.t_jmlhbayarskpdkbt),0) FROM t_skpdkbt a
            LEFT JOIN t_transaksi b on b.t_idtransaksi = a.t_idtransaksi
            WHERE b.t_jenispajak = x.s_idjenis AND b.is_deluserpendataan is null AND a.is_deluserskpdkbt is NULL
            AND YEAR(a.t_tglbayarskpdkbt) = '$tahun')"),

            "skpdkbtdenda" => new \Zend\Db\Sql\Expression("(SELECT COALESCE(SUM(a.t_bungaketerlambatanbayarskpdkbt),0) FROM t_skpdkbt a
            LEFT JOIN t_transaksi b on b.t_idtransaksi = a.t_idtransaksi
            WHERE b.t_jenispajak = x.s_idjenis AND b.is_deluserpendataan is null AND a.is_deluserskpdkbt is NULL
            AND YEAR(a.t_tglbayarskpdkbt) = '$tahun')"),

            "skpdt" => new \Zend\Db\Sql\Expression("(SELECT COALESCE(SUM(a.t_jmlhbayarskpdt),0) FROM t_skpdt a
            LEFT JOIN t_transaksi b on b.t_idtransaksi = a.t_idtransaksi
            WHERE b.t_jenispajak = x.s_idjenis AND b.is_deluserpendataan is null AND a.is_deluserskpdt is NULL
            AND YEAR(a.t_tglbayarskpdt) = '$tahun')"),

            "skpdtdenda" => new \Zend\Db\Sql\Expression("(SELECT COALESCE(SUM(a.t_bungaketerlambatanbayarskpdt),0) FROM t_skpdt a
            LEFT JOIN t_transaksi b on b.t_idtransaksi = a.t_idtransaksi
            WHERE b.t_jenispajak = x.s_idjenis AND b.is_deluserpendataan is null AND a.is_deluserskpdt is NULL
            AND YEAR(a.t_tglbayarskpdt) = '$tahun')")
        ));
        $where = new Where();
        $select->where($where);
        $select->order("x.s_idjenis");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function menu_user_esptpd($t_idwp)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_wpobjek");
        $select->columns([]);
        $select->join("s_jenisobjek", "s_jenisobjek.s_idjenis = t_wpobjek.t_jenisobjek", ["s_idjenis", "s_namajenis"], "left");

        $where = new Where();
        $where->literal("t_wpobjek.t_jenisobjek in (1,2,3,5,6,7,9)");
        $where->literal("t_wpobjek.t_idwp = " . $t_idwp . " ");

        $select->where($where);
        $select->group(["s_idjenis", "s_namajenis", "t_jenisobjek"]);
        $select->order("t_wpobjek.t_jenisobjek ASC");

        //echo $select->getSqlString(); exit();

        $state = $sql->prepareStatementForSqlObject($select);
        //var_dump($state);exit();
        $res = $state->execute();
        return $res;
    }

    public function menu_user_esptpd_admin()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_wpobjek");
        $select->columns([]);
        $select->join("s_jenisobjek", "s_jenisobjek.s_idjenis = t_wpobjek.t_jenisobjek", ["s_idjenis", "s_namajenis"], "left");
        $where = new Where();
        $where->literal("t_wpobjek.t_jenisobjek in (1,2,3,5,7,9)");
        //        $where->literal("t_wpobjek.t_idwp = ".$t_idwp." ");
        $select->where($where);
        $select->group(["s_idjenis", "s_namajenis", "t_jenisobjek"]);
        $select->order("t_wpobjek.t_jenisobjek ASC");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function carijumlahpelaporan($t_idwp, $t_idobjek)
    {
        $sql = "select SUM(t_jmlhpajak) AS t_jmlhpajak, COUNT(t_jenisobjek) AS t_jmlhpelaporan from t_wpobjek a
        left join  t_transaksi b on b.t_idwpobjek=a.t_idobjek
        where a.t_idwp='" . $t_idwp . "' and a.t_jenisobjek in (1,2,3,5,7,9) and extract(year from b.t_masaawal)='" . date('Y') . "' and a.t_jenisobjek='" . $t_idobjek . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function carijumlahpelaporan_admin($t_idobjek)
    {
        $sql = "select SUM(t_jmlhpajak) AS t_jmlhpajak, COUNT(t_jenisobjek) AS t_jmlhpelaporan from t_wpobjek a
        left join  t_transaksi b on b.t_idwpobjek=a.t_idobjek
        where a.t_jenisobjek in (1,2,3,5,7,9) and extract(year from b.t_masaawal)='" . date('Y') . "' and a.t_jenisobjek='" . $t_idobjek . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getmenu_opwp($s_idjenis, $idwp)
    {
        $sql = "select * from t_wpobjek A
                left join s_kecamatan C on C.s_kodekec = A.t_kecamatanobjek
                left join s_kelurahan D on D.s_kodekel = A.t_kelurahanobjek AND A.t_kecamatanobjek = D.s_idkeckel
                left join s_rekening B on B.s_idkorek = A.t_korekobjek 
                where A.t_jenisobjek=" . $s_idjenis . " AND A.t_idwp = " . $idwp . " 
                AND B.s_jeniskorek != '4' ";
        //B.s_rinciankorek != '00' and 
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;

        //        $sql = new Sql($this->adapter);
        //        $select = $sql->select();
        //        $select->from("t_wpobjek");
        //        $select->join("s_kecamatan", "s_kecamatan.s_idkec = t_wpobjek.t_kecamatanobjek", ["s_kodekecobjek" => "s_kodekec","s_namakecobjek" => "s_namakec"], "left"); //
        //        $select->join("s_kelurahan", "s_kelurahan.s_idkel = t_wpobjek.t_kelurahanobjek",["s_kodekelobjek" => "s_kodekel", "s_namakelobjek" => "s_namakel"], "left"); //
        ////        $select->join("s_rekening", "s_rekening.s_jenisobjek = t_wpobjek.t_jenisobjek", ["*"], "left");
        //       $select->join("s_rekening", "s_rekening.s_idkorek = t_wpobjek.t_korekobjek", ["*"], "left");
        //        $where = new Where();
        //        $where->literal("t_wpobjek.t_jenisobjek = ".$s_idjenis." ");
        //        $where->literal("t_wpobjek.t_idwp = ".$idwp." ");
        //        $where->literal("s_rekening.s_rinciankorek != '00' and s_rekening.s_jeniskorek != '4' ");
        //        $select->where($where);
        //        $state = $sql->prepareStatementForSqlObject($select);
        //        $res = $state->execute();
        //        return $res;
    }

    public function cekmasapajakbyidobjek($idobjek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi"
        ));
        $select->columns(array(
            't_masaawal'
        ));
        $where = new Where();
        $where->equalTo('a.t_idwpobjek', $idobjek);
        $select->where($where);
        $select->order('a.t_masaawal desc');
        $select->limit(1);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function cekmasapajakbymasaawal($idobjek, $bulan, $tahun)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi"
        ));
        $select->columns(array(
            't_masaawal'
        ));
        $where = new Where();
        $where->literal("extract(month from a.t_masaawal) = '" . $bulan . "' and extract(year from a.t_masaawal) = '" . $tahun . "' and a.t_idwpobjek= " . $idobjek);
        // $where->equalTo('a.t_idwpobjek', $idobjek);
        $select->where($where);
        $select->order('a.t_masaawal desc');
        $select->limit(1);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getrekeningtransaksi($s_jenisobjek, $s_objekkorek, $s_rinciankorek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("view_rekening");
        $where = new Where();
        $where->equalTo("s_jenisobjek", $s_jenisobjek);
        $where->equalTo("s_objekkorek", $s_objekkorek);
        $where->equalTo("s_rinciankorek", $s_rinciankorek);
        $where->equalTo("s_sub1korek", '001');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getHargaDasarWalet($data)
    {
        //var_dump($data);die;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_walet");
        $where = new Where();
        $where->equalTo("s_idwalet", $data);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDetailWalet($id_transaksi)
    {
        //var_dump($dataparent);die;
        //$id_transaksi = $dataparent['t_idtransaksi'];
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_detailwalet");
        $where = new Where();
        $where->equalTo("t_idtransaksi", $id_transaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataJenis($id)
    {
        $sql = "SELECT * FROM s_jenisobjek where s_idjenis = " . $id;

        $st = $this->adapter->query($sql);
        $data = $st->execute()->current();

        return $data;
    }

    public function getSanksiAktif()
    {
        $sql = "SELECT * FROM s_sanksiadm where is_active = 't' order by s_idsanksi desc";

        $st = $this->adapter->query($sql);
        $data = $st->execute()->current();

        return $data;
    }
}
