<?php

namespace Pajak\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

/**
 * Tools that could be accessed by all of your controllers
 * Add your repetitive functions or methods here, so you can access it easily
 * @author farhan
 */
class Tools extends AbstractPlugin implements ServiceLocatorAwareInterface {

    protected $service;

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     *  Function to access your predefined service in Module
     */
    public function getService($serviceName) {
        $this->service = $this->getServiceLocator()->getServiceLocator()->get($serviceName);
        return $this->service;
    }

    /**
     * $date:date string to be formatted
     * $type:format output
     * $type: 0 (dd-mm-yyyy) ex: 20-04-2016
     *        1 (dd MM yyyy) ex: 20 April 2016
     *        2 (MM)         ex: April
     */
    public function dateFormat($date, $type) {

        $bulan = [null, "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        $tgl = explode('-', $date);
        switch ($type) {
            case 0:
                $hasil = date('d-m-Y', strtotime($date));
                break;
            case 1:
                $hasil = $tgl[2] . " " . $bulan[(int) $tgl[1]] . " " . $tgl[0];
                break;
            case 2:
                $hasil = $bulan[(int) $tgl[1]];
                break;
            default:
                $hasil = $date;
                break;
        }

        return $hasil;
    }

    public function getSession() {
        return $this->getService("Sessi_Manager")->getStorage();
    }

    /**
     * Fungsi ini digunakan untuk mendapatkan daftar semua aksi yg dimiliki controller
     * @param type $classInstance masukkan instance dari class 
     * @return type array[]
     */
    public function getPermissions($classInstance) {
        // daftar method dari class yg diinginkan
        $methods = get_class_methods($classInstance);
        // daftar method yg tidak termasuk action
        $excludeMethods = array(
            "getMethodFromAction",
            "notFoundAction",
            "onDispatch",
            "dispatch",
            "getRequest",
            "getResponse",
            "setEventManager",
            "getEventManager",
            "setEvent",
            "getEvent",
            "setServiceLocator",
            "getServiceLocator",
            "getPluginManager",
            "setPluginManager",
            "plugin",
            "__call"
        );
        //hapus method built-in dari controller
        foreach ($excludeMethods as $key => $value) {
            if (($key = array_search($value, $methods)) !== false) {
                unset($methods[$key]);
            }
        }
        //hapus kata 'Action' dari method
        foreach ($methods as $key => $value) {
            $methods[$key] = str_replace('Action', '', $value);
        }

        return $methods;
    }

    public function getResources() {
        $resources = $this->getService("Config")["controllers"]["invokables"];
        $searchword = 'Pajak';
        $exeption = 'AclManager';
        foreach ($resources as $k => $v) {
            if (!preg_match("/\b$searchword\b/i", $v) || preg_match("/\b$exeption\b/i", $v)) {
                unset($resources[$k]);
            }
        }
        return $resources;
    }

    public function getBasePath() {
        $server = $this->getService('ViewHelperManager')->get('serverUrl');
        $base = $this->getService('ViewHelperManager')->get('basePath');
        return $server('') . $base('/');
    }

    public function sendMail($get_user, $link) {
        $return = ["error_message" => null, "error_count" => 0];
        try {
            set_include_path('path/to/library/' . get_include_path());
            include('public/PHPMailer/PHPMailerAutoload.php');
    //        include('public/PHPMailer/class.phpmailer.php');

            $mail = new \PHPMailer;

            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'esptpd.kaimanakab';
            $mail->Password = 'eSPTPDkaimanak@b2018';
            // $mail->Username = 'developerw49@gmail.com';
            // $mail->Password = 'Webdeveloper2018';
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->SMTPDebug = 1;

            $mail->From = 'support@esptpd.kaimanakab.go.id';
            $mail->FromName = 'e-SPTPD KAIMANA';
    //        $mail->setFrom('esptpd.kaimanakab.go.id', 'Reset Password e-SPTPD');
    //        $mail->addReplyTo('esptpd.kaimanakab.go.id', 'Reset Password e-SPTPD');

            // Menambahkan penerima
            $mail->addAddress($get_user['s_email']);
            $mail->Subject = '[No-Reply] Konfirmasi Reset Password';
            $mailContent = "<h1 style='color:blue;'>Konfirmasi Reset Password Akun e-SPTPD</h1>
                <p>Kepada <b>" . $get_user['s_nama'] . "</b>, kami beritahukan bahwa Anda telah melakukan permintaan untuk mengatur ulang password anda.</p><p>Untuk mengatur ulang password anda klik tautan berikut:<br> "
                    . "<a href='" . $link . "' style='background-color: #f44336;border: none; color: white;padding: 15px 32px;text-align: center; text-decoration: none;display: inline-block;font-size: 16px;'>Reset Password</a></p>"
                    . "<p>Abaikan e-mail ini jika anda tidak merasa melakukan permintaan pengaturan ulang password.</p><p>Terima Kasih.</p>";
//            $mailContent = "<h1 style='color:green;'>Password e-SPTPD anda sudah direset</h1>
//                <p>Kepada <b>" . $get_user['s_nama'] . "</b>, kami beritahukan bahwa Anda telah melakukan permintaan untuk mengatur ulang password anda.</p><p>Untuk mengatur ulang password anda klik tautan berikut: <a href='" . $link . "'>" . $link . "</a></p><p>Jika anda tidak merasa melakukan pengaturan ulang password, abaikan pesan ini.</p><p>Terima Kasih.</p>";
            $mail->MsgHTML($mailContent);
//            $mail->Body = $mailContent;
//            $mail->isHTML(true);
            $mail->Send($mailContent);
        } catch (\Exception $e) {
            $return["error_message"] = "Anda tidak terhubung dengan internet. Mohon pastikan anda terhubung dengan internet.";
            $return["error_count"] += 1;
        }
        return $return;
    }

    public function addCookie($cname, $cvalue) {

        $cookie = new \Zend\Http\Header\SetCookie($cname, $cvalue, time() + 60 * 60 * 24 * 30 * 12);
        $response = $this->getController()->getResponse();
        $response->getHeaders()->addHeader($cookie);
    }

}
