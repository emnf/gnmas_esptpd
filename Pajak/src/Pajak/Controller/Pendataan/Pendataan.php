<?php

namespace Pajak\Controller\Pendataan;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Pajak\Model\Pendataan\PendataanBase;

class Pendataan extends AbstractActionController
{

    public function indexAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $s_idjenis = $this->getEvent()->getRouteMatch()->getParam('s_idjenis');
        $jenisobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjekId($s_idjenis);
        $view = new ViewModel(array(
            's_idjenis' => $s_idjenis,
            'jenisobjek' => $jenisobjek,
        ));
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'pendataanaktif' => 1,
            's_idjenis' => $allParams['s_idjenis'],
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new PendataanBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('PendataanTable')->getGridCount($base, $allParams['s_idjenis']);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('PendataanTable')->getGridData($base, $start, $allParams['s_idjenis']);
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        $sptpd = "";
        foreach ($data as $row) {
            $t_tglpembayaran = ($row['t_tglpembayaran'] != "" ? '<b style="color:green"><center>Lunas / ' . "(" . date('d-m-Y', strtotime($row['t_tglpembayaran'])) . ")" . '</center></b>' : ''); // returns true
            $t_jmlhpembayaran = ($row['t_jmlhpembayaran'] != 0 ? '<b style="color:green"><center>' . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . '</center></b>' : '<b style="color:red"><center>Belum Bayar</center></b>'); // returns true
            $is_esptpd = ($row['is_esptpd'] != 0 ? '<b style="color:#db403c">e-SPTPD</b>' : '<b style="color:#3850b8">SIMPATDA</b>');
            $s .= "<tr>";
            $s .= "<td data-title='No'><center>" . $counter . "</center></td>";
            $s .= "<td data-title='No. Urut'><center>" . $row['t_nourut'] . "</center></td>";
            $s .= "<td data-title='NPWPD'><center>" . $row['t_npwpd'] . "</center></td>";
            $s .= "<td data-title='Nama'>" . $row['t_nama'] . "</td>";
            $s .= "<td data-title='NOP'><center>" . $row['t_nop'] . "</center></td>";
            $s .= "<td data-title='Nama Objek'>" . $row['t_namaobjek'] . "</td>";
            $s .= "<td data-title='Tanggal Masa'><center>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</center></td>";
            $s .= "<td data-title='Tanggal Laporan'><center>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</center></td>";
            $s .= "<td data-title='Jumlah Pajak' style='color:black; text-align: right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>";
            if ($row['t_jmlhpajak'] == 0) {
                $s .= "<td data-title='Jumlah Pembayaran' style='color:blue; text-align: center'><b>NIHIL</b></td>";
            } else {
                $s .= "<td data-title='Jumlah Pembayaran' style='color:black; text-align: right'>" . $t_tglpembayaran . " " . $t_jmlhpembayaran . "</td>";
            }

            $s .= "<td data-title='Inputan Dari' style='text-align: center'>" . $is_esptpd . "</td>";
            if ($row['t_tglpembayaran'] == '') {
                if ($row['t_jenispajak'] == 1) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdhotel?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 2) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdrestoran?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 3) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdhiburan?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 6) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdminerba?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 5) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdppj?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 7) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdparkir?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 9) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdwalet?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                }
                $kodebayar = "<a href='" . $this->cekurl() . "/laporan/cetakkodebayar?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak Kode Bayar'><i class='glyph-icon icon-print'></i> Kode Bayar</a>";
                $sspd = "";
            } else {
                if ($row['t_jenispajak'] == 1) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdhotel?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 2) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdrestoran?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 3) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdhiburan?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 6) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdminerba?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 5) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdppj?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 7) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdparkir?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 9) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdwalet?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } else {
                    $sptpd = "";
                }
                $kodebayar = "<a href='" . $this->cekurl() . "/laporan/cetakkodebayar?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak Kode Bayar'><i class='glyph-icon icon-print'></i> Kode Bayar</a>";
                $sspd = "<a href='" . $this->cekurl() . "/laporan/cetaksspd?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SSPD'><i class='glyph-icon icon-print'></i> SSPD</a>";
            }

            if ($row['t_tglpembayaran'] != '' || $row['t_jmlhpajak'] == 0) {
                $hapus = "";
            } else {
                $hapus = "<a href='" . $this->cekurl() . "/laporan/form_pagedefaultedit?t_idtransaksi=$row[t_idtransaksi]' class='bg-red' title='Hapus'><i class='glyphicon glyphicon-minus-circle'></i> Hapus</a>";
            }
            $s .= "<td data-title='#' style='text-align:center;'>
                    <div class='btn-group'>
                      <button type='button' class='btn btn-xs btn-info dropdown-toggle' data-toggle='dropdown'>
                        <span class='caret'></span>
                        <span class='sr-only'>Toggle Dropdown</span>
                      </button>
                      <ul class='dropdown-menu' role='menu'>
                        <li>$sptpd</li>
                        <li>$kodebayar</li>
                        <li>$sspd</li>
                      </ul>
                    </div></td>";
            //            $s .= "<td data-title='#'><center>$edit $sptpd $kodebayar $sspd</center></td>";
            $s .= "</tr>";
            $counter++;
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages,
            "paginatore" => $datapaging['paginatore'],
            "akhirdata" => $datapaging['akhirdata'],
            "awaldata" => $datapaging['awaldata']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cetaksptpdhotelAction()
    {
        /** Cetak SPTPD Hotel
         * @param int $idtransaksi
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();

        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningByJenis(1);
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('LaporanTable')->getDataLaporanID($data_get->t_idtransaksi);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);

        $hislog_action = 'Mencetak SPTPD ' . $datasekarang['t_jenisobjek'] . '/' . $datasekarang['t_idwpobjek'] . '/' . $datasekarang['t_masaawal'];
        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdrestoranAction()
    {
        /** Cetak SPTPD Restoran
         * @param int $idtransaksi
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 22/09/2018
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningByJenis(2);
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('LaporanTable')->getDataLaporanID($data_get->t_idtransaksi);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdhiburanAction()
    {
        /** Cetak SPTPD Hiburan
         * @param int $idtransaksi
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 22/09/2018
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningSubByJenis(3);
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('LaporanTable')->getDataLaporanID($data_get->t_idtransaksi);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdppjAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningSubByJenis(5);
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('LaporanTable')->getDataLaporanID($data_get->t_idtransaksi);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        //        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'rekening' => $rekening,
            //            'ar_ttd' => $ar_ttd,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdparkirAction()
    {
        /** Cetak SPTPD Parkir
         * @param int $idtransaksi
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 22/09/2018
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // $rekening = $this->Tools()->getService('RekeningTable')->getRekeningSubByJenis(7);
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('LaporanTable')->getDataLaporanID($data_get->t_idtransaksi);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
        $datadetailparkir = $this->Tools()->getService('PendataanTable')->getDataDetailParkir($data_get->t_idtransaksi);
        $dataParkir = $this->Tools()->getService('PendataanTable')->getDataParkir($data_get->t_idtransaksi);
        //var_dump($dataParkir);die;
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'dataparkir' => $dataParkir,
            'datadetailparkir' => $datadetailparkir,
            'ar_pemda' => $ar_pemda,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function hapusAction()
    {
        /** Hapus Laporan
         * @param int $s_idkorek
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 22/09/2018
         */
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $this->Tools()->getService('LaporanTable')->hapusLaporan($this->params('s_idkorek'), $session);
        return $this->getResponse();
    }

    public function cetakkodebayarAction()
    {
        /** Cetak Kode Bayar
         * @param int $t_idwp
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP
        $data = $this->Tools()->getService('LaporanTable')->getDataPembayaranID($data_get['t_idtransaksi']);

        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'data' => $data,
            'ar_pemda' => $ar_pemda,
        ));
        $pdf->setOption("paperSize", "potrait");
        return $pdf;
    }


    public function cetaklaporanpajakAction()
    {
        /** Cetak Laporan pajak
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 22/09/2018
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $jenisobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjekId($data_get['t_idjenispajak']);
        $data = $this->Tools()->getService('PendataanTable')->getDataTransaksi($data_get['t_idjenispajak'], $data_get['bulan_pajak'], $data_get['periode_pajak'], $data_get['pelaporan_dari'], $data_get['status_bayar']);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'ar_pemda' => $ar_pemda,
            'periode_pajak' => $data_get['periode_pajak'],
            'bulan_pajak' => $data_get['bulan_pajak'],
            'nama_jenispajak' => $jenisobjek['s_namajenis']
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }


    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        if ($uri->getHost() == 'localhost' || $uri->getHost() == '127.0.0.1') {
            return $uri->getScheme() . '://' . $uri->getHost() . ':' . $_SERVER['SERVER_PORT'] . '' . $uri->getPath();
        }
        return $uri->getScheme() . '://' . $uri->getHost() . '' . $uri->getPath();
    }

    public function cetaksptpdminerbaAction()
    {
        /** Cetak SPTPD MINERBA
         * @param int $idtransaksi
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 18/02/2019
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('LaporanTable')->getDataLaporanID($data_get->t_idtransaksi);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);

        // Mengambil data Detail Minerba
        $datadetailminerba = $this->Tools()->getService('PendataanTable')->getDataDetailMinerba($data_get->t_idtransaksi);

        $hislog_action = 'Mencetak SPTPD ' . $datasekarang['t_jenisobjek'] . '/' . $datasekarang['t_idwpobjek'] . '/' . $datasekarang['t_masaawal'];
        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'dataminerba' => $datadetailminerba,
            'ar_pemda' => $ar_pemda,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }
}
