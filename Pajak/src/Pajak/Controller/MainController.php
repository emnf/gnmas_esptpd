<?php

namespace Pajak\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class MainController extends AbstractActionController {

    public function indexAction() {
        ini_set('memory_limit', '1048M');
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_slidebgAktif = $this->Tools()->getService('CustomSlideTable')->get_bg();
//        var_dump($ar_slidebgAktif['id_bg_slide']);exit();
        $ar_slidebg = $this->Tools()->getService('CustomSlideTable')->getData();
        if ($session['s_akses'] == 1) {
            $recordspajak = $this->Tools()->getService('ObjekTable')->getDataObjekLeftMenu($session['s_wp']);
            $dataWP = $this->Tools()->getService('ObjekTable')->getDataWP($session['s_wp']);
//            $datanominalpelaporan = $this->Tools()->getService('ObjekTable')->datanominalpelaporan($session['s_wp']);
        } else if ($session['s_akses'] == 3) {
            $recordspajak = array();
            $dataWP = array();
//            $datanominalpelaporan = array();
        } else {
            $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
            $dataWP = array();
//            $datanominalpelaporan = $this->Tools()->getService('ObjekTable')->datanominalpelaporanadmin();
            $recordspajak = array();
            foreach ($dataobjek as $dataobjek) {
                $recordspajak[] = $dataobjek;
            }
        }
        $view = new ViewModel(array(
            'datauser' => $session,
            'data_pemda' => $ar_pemda,
            'data_slidebgaktif' => $ar_slidebgAktif['file_bg_slide'],
            'data_slidebg' => $ar_slidebg,
            'data_wp' => $dataWP
//            'datanominalpelaporan' => $datanominalpelaporan
        ));

        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'homeaktif' => 1
        );
        $this->layout()->setVariables($data);
        return $view;
    }

}
