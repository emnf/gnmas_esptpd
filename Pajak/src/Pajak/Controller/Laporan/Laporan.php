<?php

namespace Pajak\Controller\Laporan;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Pajak\Form\Laporan\LaporanFrm;
use Pajak\Helper\DendaHelper;
use Pajak\Model\Laporan\LaporanBase;
use Zend\Debug\Debug;

class Laporan extends AbstractActionController
{

    public function indexAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();

        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $s_idjenis = $this->getEvent()->getRouteMatch()->getParam('s_idjenis');
        $jenisobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjekId($s_idjenis);
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();

        $recordspajak = $this->Tools()->getService('ObjekTable')->getDataObjekLeftMenu($session['s_wp']);
        $hislog_action = 'Membuka Menu / Tabel Data Pelaporan Objek Pajak ' . $s_idjenis . '/' . $allParams['t_idobjek'] . '/' . $jenisobjek['s_namajenis'];
        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);

        $dataidtransaksi = null;
        $helper = new DendaHelper();
        if ($allParams['modaltransaksi'] != 'null') {
            $dataidtransaksi = $this->Tools()->getService('LaporanTable')->getDataLaporanID($allParams['modaltransaksi']);
            if (!empty($dataidtransaksi['t_tglpembayaran'])) {
                $dataidtransaksi = null;
            } else {
                $dataidtransaksi['url'] = $this->cekurl();
                $jatuhtempo = $dataidtransaksi['t_tgljatuhtempo'];
                $tglsekarang = date('Y-m-d');

                $ts1 = strtotime($jatuhtempo); //ok
                $ts2 = strtotime($tglsekarang);
                $datadenda = $helper->hitungDenda($ts1, $ts2, $dataidtransaksi['t_jmlhpajak']);
                $dataidtransaksi['denda'] = $datadenda['jmldenda'];


                $selfassesment = [1, 2, 3, 5, 6, 7, 8];
                $dataidtransaksi['sanksiadm'] = 0;
                if (in_array($dataidtransaksi['t_jenisobjek'], $selfassesment)) {
                    $datajenis = $this->Tools()->getService('LaporanTable')->getDataJenis($dataidtransaksi['t_jenisobjek']);
                    $datelapor = $datelapor = $dataidtransaksi['t_masaakhir'];
                    if ($dataidtransaksi['korek'] == '4.1.01.07.07.001') {
                        $datelapor = $dataidtransaksi['t_tgljatuhtempo'];
                    }
                    $bataslapor = date('Y-m-d', strtotime('+' . $datajenis['t_akhirlapor'] . ' days', strtotime($datelapor)));
                    if (date('Y-m-d') > $bataslapor) {
                        $datasanksi = $this->Tools()->getService('LaporanTable')->getSanksiAktif();
                        $dataidtransaksi['sanksiadm'] = $datasanksi['s_sanksi'] != null ? $datasanksi['s_sanksi'] : 0;
                    }
                }
            }
        }

        // Debug::dump($dataidtransaksi);
        // exit;

        $view = new ViewModel(array(
            's_idjenis' => $s_idjenis,
            'jenisobjek' => $jenisobjek,
            'objekwp' => $allParams['t_idobjek'],
            'dataidtransaksi' => $dataidtransaksi
        ));
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'laporanaktif' => 1,
            'sub_menu_objekwp' => $allParams['t_idobjek'],
            'sub_menu_jenispajak' => $allParams['s_idjenis'],
            //            'sub_menu_koreksub' => $allParams['s_idkorek']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new LaporanBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('LaporanTable')->getGridCount($base, $allParams['s_idjenis'], $allParams['t_idobjek']);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('LaporanTable')->getGridData($base, $start, $allParams['s_idjenis'], $allParams['t_idobjek']);
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        $sptpd = "";
        foreach ($data as $row) {
            $t_tglpembayaran = ($row['t_tglpembayaran'] != "" ? '<b style="color:green"><center>Lunas / ' . "(" . date('d-m-Y', strtotime($row['t_tglpembayaran'])) . ")" . '</center></b>' : ''); // returns true
            $t_jmlhpembayaran = ($row['t_jmlhpembayaran'] != 0 ? '<b style="color:green"><center>Rp. ' . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . '</center></b>' : '<center><b style="color:blue;">' . $row['t_kodebayar'] . '</b><br><b style="color:red"><i class="glyph-icon icon-money"></i> BELUM BAYAR</b></center>'); // returns true
            $is_esptpd = ($row['is_esptpd'] != 0 ? '<b style="color:#db403c">e-SPTPD</b>' : '<b style="color:#3850b8">SIMPATDA</b>');
            $s .= "<tr>";
            $s .= "<td data-title='No'><center>" . $counter . "</center></td>";
            $s .= "<td data-title='No. Urut'><center>" . $row['t_nourut'] . "</center></td>";
            $s .= "<td data-title='NPWPD'><center>" . $row['t_npwpd'] . "</center></td>";
            $s .= "<td data-title='Nama'>" . $row['t_nama'] . "</td>";
            $s .= "<td data-title='NOP'><center>" . $row['t_nop'] . "</center></td>";
            $s .= "<td data-title='Nama Objek'>" . $row['t_namaobjek'] . "</td>";
            $s .= "<td data-title='Tanggal Masa'><center>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</center></td>";
            $s .= "<td data-title='Tanggal Laporan'><center>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</center></td>";
            $s .= "<td data-title='Jumlah Pajak' style='color:black; text-align: right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>";
            if ($row['t_jmlhpajak'] == 0) {
                $s .= "<td data-title='Jumlah Pembayaran' style='color:blue; text-align: center'><b>NIHIL</b></td>";
            } else {
                $s .= "<td data-title='Jumlah Pembayaran' style='color:black; text-align: right'>" . $t_tglpembayaran . " " . $t_jmlhpembayaran . "</td>";
            }

            $s .= "<td data-title='Inputan Dari' style='text-align: center'>" . $is_esptpd . "</td>";
            if ($row['t_tglpembayaran'] == '') {
                if ($row['t_jenispajak'] == 1) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdhotel?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 2) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdrestoran?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 3) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdhiburan?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 6) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdminerba?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 5) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdppj?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 7) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdparkir?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 9) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdwalet?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                }
                $kodebayar = "<a href='" . $this->cekurl() . "/laporan/cetakkodebayar?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak Kode Bayar'><i class='glyph-icon icon-print'></i> Kode Bayar</a>";
                $sspd = "";
            } else {
                if ($row['t_jenispajak'] == 1) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdhotel?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 2) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdrestoran?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 3) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdhiburan?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 6) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdminerba?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 5) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdppj?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 7) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdparkir?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } elseif ($row['t_jenispajak'] == 9) {
                    $sptpd = "<a href='" . $this->cekurl() . "/laporan/cetaksptpdwalet?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i> SPTPD</a>";
                } else {
                    $sptpd = "";
                }
                $kodebayar = "<a href='" . $this->cekurl() . "/laporan/cetakkodebayar?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak Kode Bayar'><i class='glyph-icon icon-print'></i> Kode Bayar</a>";
                $sspd = "<a href='" . $this->cekurl() . "/laporan/cetaksspd?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' title='Cetak SSPD'><i class='glyph-icon icon-print'></i> SSPD</a>";
            }
            $sptpd = !empty($row['t_tglpembayaran']) ? $sptpd : "";
            if ($row['t_tglpembayaran'] != '' || $row['t_jmlhpajak'] == 0) {
                $edit = "";
            } else {
                if ($row['t_jenispajak'] == 5) {
                    $edit = "<a href='" . $this->cekurl() . "/laporan/form_pageppjedit?t_idtransaksi=$row[t_idtransaksi]' class='bg-blue' title='Edit'><i class='glyphicon glyphicon-pencil'></i> Edit</a>";
                } else
				if ($row['t_jenispajak'] == 6) {
                    $edit = "<a href='" . $this->cekurl() . "/laporan/form_pageminerbaedit?t_idtransaksi=$row[t_idtransaksi]' class='bg-blue' title='Edit'><i class='glyphicon glyphicon-pencil'></i> Edit</a>";
                    // }elseif ($row['t_jenispajak'] == 7) {
                    // $edit = "<a href='" . $this->cekurl() . "/laporan/form_pageparkiredit?t_idtransaksi=$row[t_idtransaksi]' class='bg-blue' title='Edit'><i class='glyphicon glyphicon-pencil'></i> Edit</a>";	
                } else {
                    $edit = "<a href='" . $this->cekurl() . "/laporan/form_pagedefaultedit?t_idtransaksi=$row[t_idtransaksi]' class='bg-blue' title='Edit'><i class='glyphicon glyphicon-pencil'></i> Edit</a>";
                }
            }
            $s .= "<td data-title='#' style='text-align:center;'>
                    <div class='btn-group'>
                      <button type='button' class='btn btn-xs btn-info dropdown-toggle' data-toggle='dropdown'>
                        <span class='caret'></span>
                        <span class='sr-only'>Toggle Dropdown</span>
                      </button>
                      <ul class='dropdown-menu' role='menu'>
                        <li>$edit</li>
                        <li>$sptpd</li>
                        <li>$kodebayar</li>
                        <li>$sspd</li>
                      </ul>
                    </div></td>";
            //            $s .= "<td data-title='#'><center>$edit $sptpd $kodebayar $sspd</center></td>";
            $s .= "</tr>";
            $counter++;
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages,
            "paginatore" => $datapaging['paginatore'],
            "akhirdata" => $datapaging['akhirdata'],
            "awaldata" => $datapaging['awaldata']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function FormPagedefaultAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $data_get = $req->getPost();
        //var_dump($data_get);die;
        if ($req->isGet()) {
            $id = $allParams['s_idjenis']; // ini sebenarnya id objek wp
            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
            $detailrekening = $this->Tools()->getService('LaporanTable')->getrekeningtransaksi($datapendaftaran['t_jenisobjek'], $datapendaftaran['s_objekkorek'], $datapendaftaran['s_rinciankorek']);
            //var_dump($detailrekening);exit();
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datakorekwp = $this->Tools()->getService('LaporanTable')->getKorekWp($id);
            $data->t_idkorek = $detailrekening['s_idkorek'];
            $data->t_tarifpajak = $datakorekwp['s_persentarifkorek'];
            $tahun = date('Y');
            $datapendataan = $this->Tools()->getService('LaporanTable')->getLaporanMaxByPeriode($tahun);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');

            $form = new LaporanFrm($abulan);
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            $form = new LaporanFrm($abulan);
            $base = new LaporanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());

                $sudahditetapkan = $this->Tools()->getService('LaporanTable')->getLaporanSeMasaWp($base, $post["t_idtransaksi"]);
                //apabila katering
                $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($base->t_idobjek);
                //var_dump($datapendaftaran);exit();
                if ((int)$datapendaftaran['t_korekobjek'] == 33 || (int)$datapendaftaran['t_korekobjek'] == 34) {

                    $dataparent = $this->Tools()->getService('LaporanTable')->simpanpendataanself($base, $session, $post);
                    $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/check-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
                                    Pelaporan SPT Berhasil disimpan, silahkan Cetak Kode Bayar atau SPTPD!');
                    // Debug::dump($dataparent);
                    // Debug::dump(1);
                    // exit;
                    return $this->redirect()->toRoute("laporan", array(
                        "controllers" => "Laporan",
                        "action" => "index",
                        "s_idjenis" => $req->getPost()['t_jenisobjekpajak'],
                        "t_idobjek" => $post['t_idobjek'],
                        "modaltransaksi" => $dataparent['t_idtransaksi']
                    ));
                }
                //============ end katering

                if (empty($sudahditetapkan)) {

                    #cekpealporansebelumnya
                    $belumpernahlapor = $this->Tools()->getService('LaporanTable')->cekBelumLapor($post['t_idobjek']);
                    if (empty($belumpernahlapor)) {
                        $dataparent = $this->Tools()->getService('LaporanTable')->simpanpendataanself($base, $session, $post);
                        if (!empty($post['t_iddetailomzet'])) {
                            for ($i = 0; $i < count($post['t_iddetailomzet']); $i++) {
                                $this->Tools()->getService('LaporanTable')->simpandetail($post, $dataparent, $i);
                            }
                        }
                        $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/check-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
                                    Pelaporan SPT Berhasil disimpan, silahkan Cetak Kode Bayar atau SPTPD!');
                        // Debug::dump($dataparent);
                        // Debug::dump(2);
                        // exit;
                        return $this->redirect()->toRoute("laporan", array(
                            "controllers" => "Laporan",
                            "action" => "index",
                            "s_idjenis" => $req->getPost()['t_jenisobjekpajak'],
                            "t_idobjek" => $post['t_idobjek'],
                            "modaltransaksi" => $dataparent['t_idtransaksi']
                        ));
                    } else {
                        $t_masapendataan = $base->t_periodepajak . '-' . $base->t_masapajak . '-01';
                        $sebelumnya = $this->Tools()->getService('LaporanTable')->cekPelaporanSebelumnya($base->t_jenispajak, $base->t_idobjek, $t_masapendataan);
                        if (empty($sebelumnya)) { #belum melaporkan bulan sebelumnya
                            $id = $base->t_idobjek;
                            //var_dump($id);die;
                            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
                            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                            $data->t_tglpendataan = date('d-m-Y');
                            $data->t_idwp = $datapendaftaran['t_idwp'];
                            $bulanseblum = date("m", strtotime($t_masapendataan . "-1 month"));
                            $tahunsebelum = date("Y", strtotime($t_masapendataan . "-1 month"));
                            $message = 'Belum Melaporkan Pajak Bulan ' . $abulan[$bulanseblum] . ' Tahun ' . $tahunsebelum . '';
                            $form->bind($data);
                            $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                    Belum Melaporkan Pajak Bulan ' . $abulan[$bulanseblum] . ' Tahun ' . $tahunsebelum . '!');
                            return $this->redirect()->toRoute("laporan", array(
                                "controllers" => "Laporan",
                                "action" => "form_pagedefault",
                                "t_idobjek" => $post['t_idobjek']
                            ));
                        } else { #sudahmelaporakan bulan sebelumnya
                            //=== cek lagi apakah lapor lebih dari bulan sekarang
                            if ($base->t_masapajak >= date('m') && $base->t_periodepajak >= date('Y')) {
                                $id = $base->t_idobjek;
                                $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
                                $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                                $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                                $data->t_tglpendataan = date('d-m-Y');
                                $data->t_idwp = $datapendaftaran['t_idwp'];
                                $message = 'Belum Waktunya Laporan Bulan ' . $abulan[$base->t_masapajak];
                                $form->bind($data);
                                $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                Belum Waktunya Laporan Bulan ' . $abulan[$base->t_masapajak] . ' Tahun ' . $base->t_periodepajak . '!');
                                return $this->redirect()->toRoute("laporan", array(
                                    "controllers" => "Laporan",
                                    "action" => "form_pagedefault",
                                    "t_idobjek" => $post['t_idobjek']
                                ));
                            }
                            //==================================================
                            else {
                                if ($post['t_jenisobjekpajak'] == 9) { // WALET
                                    //var_dump($post);die;
                                    $hargawalet = $this->Tools()->getService('LaporanTable')->getHargaDasarWalet(1);
                                    $harga_dasar = str_ireplace(".", "", $post['t_hargadasar']);
                                    $dataparent = $this->Tools()->getService('LaporanTable')->simpandatawalet($base, $session, $post);
                                    $impandetailwalet = $this->Tools()->getService('LaporanTable')->simpandetailwalet($dataparent, $post, $harga_dasar);
                                } else {
                                    $dataparent = $this->Tools()->getService('LaporanTable')->simpanpendataanself($base, $session, $post);
                                }
                                if (!empty($post['t_iddetailomzet'])) {
                                    for ($i = 0; $i < count($post['t_iddetailomzet']); $i++) {
                                        $this->Tools()->getService('LaporanTable')->simpandetail($post, $dataparent, $i);
                                    }
                                }
                                $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/check-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
                                    Pelaporan SPT Berhasil disimpan, silahkan Cetak <b>Kode Bayar</b> dan <b>SPTPD</b>!');
                                // Debug::dump($dataparent);
                                // Debug::dump(3);
                                // exit;
                                return $this->redirect()->toRoute("laporan", array(
                                    "controllers" => "Laporan",
                                    "action" => "index",
                                    "s_idjenis" => $req->getPost()['t_jenisobjekpajak'],
                                    "t_idobjek" => $post['t_idobjek'],
                                    "modaltransaksi" => $dataparent['t_idtransaksi']
                                ));
                            }
                        }
                    }
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
                    $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                    $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                    $data->t_tglpendataan = date('d-m-Y');
                    $data->t_idwp = $datapendaftaran['t_idwp'];
                    $txt_bulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $message = 'Data WP untuk Bulan ' . $txt_bulan[$base->t_masapajak] . ' Tahun ' . $base->t_periodepajak . ' Sudah Pernah Dilaporkan!';
                    $form->bind($data);
                    $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                Data WP untuk Bulan ' . $txt_bulan[$base->t_masapajak] . ' Tahun ' . $base->t_periodepajak . ' Sudah Pernah Dilaporkan!');
                    return $this->redirect()->toRoute("laporan", array(
                        "controllers" => "Laporan",
                        "action" => "form_pagedefault",
                        "t_idobjek" => $post['t_idobjek']
                    ));
                }
            } else {
                $message = $form->getMessages();
                var_dump($message);
                die;
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            //            'objekwp' => $allParams['t_idobjek'],
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $recordspajak = $this->Tools()->getService('ObjekTable')->getDataObjekLeftMenu($session['s_wp']);
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'laporanaktif' => 1,
            'sub_menu_objekwp' => $datapendaftaran['t_idobjek'], //3804,
            'sub_menu_jenispajak' => $datapendaftaran['t_jenisobjek'], //$allParams['s_idjenis'],
            'datapendaftaran' => $datapendaftaran,
        );
        //        $data = array(
        //            'data_pemda' => $ar_pemda,
        //            'datauser' => $session,
        //            'dataobjek' => $recordspajak,
        //            'laporanaktif' => 1,
        ////            'sub_menu_objekwp' => $data_get['t_idobjek'],//3804,
        ////            'sub_menu_jenispajak' => $data_get['t_jenisobjek'],//$allParams['s_idjenis'],
        ////            'sub_menu_koreksub' => $allParams['s_idkorek']
        //        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagedefaulteditAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new LaporanFrm();
        if ($req->isGet()) {
            $message = '';
            $id = $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdTransaksi($id);

            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_tglpendataan = date('d-m-Y', strtotime($data->t_tglpendataan));
            $data->t_masaawal = date('d-m-Y', strtotime($data->t_masaawal));
            $data->t_masaakhir = date('d-m-Y', strtotime($data->t_masaakhir));
            $data->t_jmlhpajak = number_format($data->t_jmlhpajak, 0, ',', '.');
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');
            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            // $detailrekening = $this->Tools()->getService('LaporanTable')->getrekeningtransaksi($datapendaftaran['t_jenisobjek'],$datapendaftaran['s_objekkorek'],$datapendaftaran['s_rinciankorek']);

            if ($datapendaftaran['t_jenisobjek'] == 2) {
                if ((int)$datatransaksi['s_idkorek'] == 34) {
                    $t_berdasarmasa = 'Tidak Berdasar Masa';
                } else {
                    $t_berdasarmasa = 'Berdasar Masa';
                }
            } else {
                $t_berdasarmasa = null;
            }

            //walet ambil omsetnya dari volume yang di dapat
            if ($datatransaksi['t_jenisobjek'] == 9) {
                $detailwalet = $this->Tools()->getService('PendataanTable')->getDataDetailWalet($id);
                $data->t_nilaiperolehan1  = $detailwalet['t_nilaiperolehan1'];
                $data->t_hargadasar  = $detailwalet['t_hargadasar1'];
                $data->t_jenissarang = $detailwalet['t_jenissarang1'];
                $data->t_tarifpajak = $detailwalet['t_tarifpajak'];
            }

            //var_dump($detailwalet);exit();

            //katering
            $data->t_keterangankatering = $datapendaftaran['t_keterangankatering'];
            $data->t_opdkatering = $datapendaftaran['t_opdkatering'];


            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_masapajak = date('m', strtotime($datatransaksi['t_masaawal']));
            $form = new LaporanFrm($this->Tools()->getService('LaporanTable')->getBulanBelumLaporEdit($datapendaftaran['t_idobjek']));
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            //apabila katering
            //if ($datapendaftaran['s_objekkorek'] == 2 && $datatransaksi['s_idkorek'] == 34)
            $form = new LaporanFrm($abulan);
            $form->bind($data);
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            't_berdasarmasa' => $t_berdasarmasa,
            //            't_rincianomzet' => $datatransaksi['t_rincianomzet']
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $recordspajak = $this->Tools()->getService('ObjekTable')->getDataObjekLeftMenu($session['s_wp']);
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'laporanaktif' => 1,
            'sub_menu_objekwp' => $datapendaftaran['t_idobjek'], //3804,
            'sub_menu_jenispajak' => $datapendaftaran['t_jenisobjek'], //$allParams['s_idjenis'],
        );
        //var_dump($data);die;
        $this->layout()->setVariables($data);
        return $view;
    }

    //PPJ
    public function FormPageppjAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $data_get = $req->getPost();
        if ($req->isGet()) {
            $id = $allParams['s_idjenis']; // ini sebenarnya id objek wp
            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);

            $rekening = $this->Tools()->getService('LaporanTable')->getrekeningtransaksi($datapendaftaran['t_jenisobjek'], $datapendaftaran['s_objekkorek'], $datapendaftaran['s_rinciankorek']);
            //$this->Tools()->getService('RekeningTable')->getdataRekeningPPJ($datapendaftaran['s_rinciankorek']);
            $tarif = $rekening['s_persentarifkorek'];
            $recordsrekening = array();
            foreach ($rekening as $rekening) {
                $recordsrekening[] = $rekening;
            }
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_tarifpajak = $tarif;
            $message = '';
            $datakorekwp = $this->Tools()->getService('LaporanTable')->getKorekWp($id);
            $detailrekening = $this->Tools()->getService('LaporanTable')->getrekeningtransaksi($datapendaftaran['t_jenisobjek'], $datapendaftaran['s_objekkorek'], $datapendaftaran['s_rinciankorek']);
            $data->t_idkorek = $detailrekening['s_idkorek'];
            $tahun = date('Y');
            $datapendataan = $this->Tools()->getService('LaporanTable')->getLaporanMaxByPeriode($tahun);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            $form = new LaporanFrm($abulan);
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            $form = new LaporanFrm($abulan);
            $base = new LaporanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);

            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $sudahditetapkan = $this->Tools()->getService('LaporanTable')->getLaporanSeMasaWp($base, $post["t_idtransaksi"]);
                if (empty($sudahditetapkan)) {
                    #cekpealporansebelumnya
                    $belumpernahlapor = $this->Tools()->getService('LaporanTable')->cekBelumLapor($post['t_idobjek']);
                    if (empty($belumpernahlapor)) {
                        $dataparent = $this->Tools()->getService('LaporanTable')->simpanpendataanself($base, $session, $post);
                        if ($post['t_asallistrik'] == '2') {
                            $this->Tools()->getService('DetailPpjTable')->simpanpendataanppj($data, $post);
                        } else {
                        }
                        $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/check-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
                                    Pelaporan SPT Berhasil disimpan, silahkan Cetak Kode Bayar atau SPTPD!');
                        return $this->redirect()->toRoute("laporan", array(
                            "controllers" => "Laporan",
                            "action" => "index",
                            "s_idjenis" => $req->getPost()['t_jenisobjekpajak'],
                            "t_idobjek" => $post['t_idobjek'],
                            "modaltransaksi" => $dataparent['t_idtransaksi']
                        ));
                    } else {
                        $t_masapendataan = $base->t_periodepajak . '-' . $base->t_masapajak . '-01';
                        $sebelumnya = $this->Tools()->getService('LaporanTable')->cekPelaporanSebelumnya($base->t_jenispajak, $base->t_idobjek, $t_masapendataan);
                        if (empty($sebelumnya)) {
                            #belum melaporkan bulan sebelumnya
                            $id = $base->t_idobjek;
                            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
                            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                            $data->t_tglpendataan = date('d-m-Y');
                            $data->t_idwp = $datapendaftaran['t_idwp'];
                            $bulanseblum = date("m", strtotime($t_masapendataan . "-1 month"));
                            $tahunsebelum = date("Y", strtotime($t_masapendataan . "-1 month"));
                            $message = 'Belum Melaporkan Pajak Bulan ' . $abulan[$bulanseblum] . ' Tahun ' . $tahunsebelum . '';
                            $form->bind($data);
                            $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                    Belum Melaporkan Pajak Bulan ' . $abulan[$bulanseblum] . ' Tahun ' . $tahunsebelum . '!');
                            return $this->redirect()->toRoute("laporan", array(
                                "controllers" => "Laporan",
                                "action" => "form_pageppj",
                                "t_idobjek" => $post['t_idobjek']
                            ));
                        } else {
                            #sudahmelaporakan bulan sebelumnya
                            //=== cek lagi apakah lapor lebih dari bulan sekarang
                            $cek_pelaporan = $base->t_masapajak >= date('m') && $base->t_periodepajak >= date('Y');
                            if ($cek_pelaporan) { //$base->t_masapajak > date('m') && $base->t_periodepajak >= date('Y')
                                $id = $base->t_idobjek;
                                $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
                                $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                                $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                                $data->t_tglpendataan = date('d-m-Y');
                                $data->t_idwp = $datapendaftaran['t_idwp'];
                                $message = 'Belum Waktunya Laporan Bulan ' . $abulan[$base->t_masapajak];
                                $form->bind($data);
                                $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                Belum Waktunya Laporan Bulan ' . $abulan[$base->t_masapajak] . ' Tahun ' . $base->t_periodepajak . '!');
                                return $this->redirect()->toRoute("laporan", array(
                                    "controllers" => "Laporan",
                                    "action" => "form_pageppj",
                                    "t_idobjek" => $post['t_idobjek']
                                ));
                            } else {
                                $dataparent = $this->Tools()->getService('LaporanTable')->simpanpendataanself($base, $session, $post);
                                // var_dump($post);die();
                                if ($post['t_asallistrik'] == '2') {

                                    $this->Tools()->getService('DetailPpjTable')->simpanpendataanppj($dataparent, $post);
                                } else {
                                }

                                $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/check-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
                                    Pelaporan SPT Berhasil disimpan, silahkan Cetak <b>Kode Bayar</b> dan <b>SPTPD</b>!');
                                return $this->redirect()->toRoute("laporan", array(
                                    "controllers" => "Laporan",
                                    "action" => "index",
                                    "s_idjenis" => $req->getPost()['t_jenisobjekpajak'],
                                    "t_idobjek" => $post['t_idobjek'],
                                    "modaltransaksi" => $dataparent['t_idtransaksi']
                                ));
                            }
                        }
                    }
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
                    $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                    $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                    $data->t_tglpendataan = date('d-m-Y');
                    $data->t_idwp = $datapendaftaran['t_idwp'];
                    $txt_bulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $message = 'Data WP untuk Bulan ' . $txt_bulan[$base->t_masapajak] . ' Tahun ' . $base->t_periodepajak . ' Sudah Pernah Dilaporkan!';
                    $form->bind($data);
                    $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                Data WP untuk Bulan ' . $txt_bulan[$base->t_masapajak] . ' Tahun ' . $base->t_periodepajak . ' Sudah Pernah Dilaporkan!');
                    return $this->redirect()->toRoute("laporan", array(
                        "controllers" => "Laporan",
                        "action" => "form_pageppj",
                        "t_idobjek" => $post['t_idobjek']
                    ));
                }
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'rekeningppj' => $recordsrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $recordspajak = $this->Tools()->getService('ObjekTable')->getDataObjekLeftMenu($session['s_wp']);
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'laporanaktif' => 1,
            'sub_menu_objekwp' => $datapendaftaran['t_idobjek'], //3804,
            'sub_menu_jenispajak' => $datapendaftaran['t_jenisobjek'], //$allParams['s_idjenis'],
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageppjeditAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new LaporanFrm();
        if ($req->isGet()) {
            $message = '';
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningPPJ($datapendaftaran['s_rinciankorek']);
            $recordsrekening = array();
            foreach ($rekening as $rekening) {
                $recordsrekening[] = $rekening;
            }
            $data->t_tglpendataan = date('d-m-Y', strtotime($data->t_tglpendataan));
            $data->t_masaawal = date('d-m-Y', strtotime($data->t_masaawal));
            $data->t_masaakhir = date('d-m-Y', strtotime($data->t_masaakhir));
            $data->t_jmlhpajak = number_format($data->t_jmlhpajak, 0, ',', '.');
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');

            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $detailppj = $this->Tools()->getService('DetailPpjTable')->getDetailByIdTransaksi($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['t_tarifpajak'];
            $data->t_asallistrik = $datatransaksi['t_asallistrik'];
            $t_asallistrik = $datatransaksi['t_asallistrik'];
            $data->t_masapajak = date('m', strtotime($datatransaksi['t_masaawal']));
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            $form = new LaporanFrm($abulan);
            $form->bind($data);
        }
        $datappj = $this->Tools()->getService('DetailPpjTable')->getDetailByIdTransaksi($req->getQuery()->get('t_idtransaksi'));
        $detailppj = array();
        if ($t_asallistrik == '2') {
            $detail = $detailppj;
            foreach ($datappj as $datappj) {
                $detailppj[] = $datappj;
            }
        }
        // var_dump($detailppj);exit();

        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'detailppj' => $detailppj,
            'rekeningppj' => $recordsrekening,
            'detailppj' => $detailppj,
            't_asallistrik' => $t_asallistrik
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $recordspajak = $this->Tools()->getService('ObjekTable')->getDataObjekLeftMenu($session['s_wp']);
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'laporanaktif' => 1,
            'sub_menu_objekwp' => $datapendaftaran['t_idobjek'], //3804,
            'sub_menu_jenispajak' => $datapendaftaran['t_jenisobjek'], //$allParams['s_idjenis'],
        );
        //        $data = array(
        //            'data_pemda' => $ar_pemda,
        //            'datauser' => $session,
        //            'dataobjek' => $recordspajak
        //        );
        $this->layout()->setVariables($data);
        return $view;
    }

    //MINERBA
    public function FormPageminerbaAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $data_get = $req->getPost();
        if ($req->isGet()) {
            $id = $allParams['s_idjenis']; // ini sebenarnya id objek wp
            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
            $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningMineral();
            $recordsrekening = array();
            foreach ($rekening as $rekening) {
                $recordsrekening[] = $rekening;
            }

            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datakorekwp = $this->Tools()->getService('LaporanTable')->getKorekWp($id);
            $data->t_idkorek = $datakorekwp['s_idkorek'];
            $data->t_tarifpajak = $datakorekwp['s_persentarifkorek'];
            $tahun = date('Y');
            $datapendataan = $this->Tools()->getService('LaporanTable')->getLaporanMaxByPeriode($tahun);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            $form = new LaporanFrm($abulan);
            $form->bind($data);
            //var_dump($data);die;
        }
        if ($this->getRequest()->isPost()) {
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            $form = new LaporanFrm($abulan);
            $base = new LaporanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            //var_dump($post);die;
            if ($form->isValid()) {
                //var_dump($base->exchangeArray($form->getData()));die;
                $base->exchangeArray($form->getData());
                $sudahditetapkan = $this->Tools()->getService('LaporanTable')->getLaporanSeMasaWp($base, $post["t_idtransaksi"]);
                //var_dump($sudahditetapkan);die;
                if (empty($sudahditetapkan)) {

                    #cekpealporansebelumnya
                    $belumpernahlapor = $this->Tools()->getService('LaporanTable')->cekBelumLapor($post['t_idobjek']);

                    if (empty($belumpernahlapor)) {

                        $dataparent = $this->Tools()->getService('LaporanTable')->simpanpendataanself($base, $session, $post);
                        $this->Tools()->getService('DetailminerbaTable')->simpanpendataanminerba($post, $dataparent);
                        $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/check-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
                                    Pelaporan SPT Berhasil disimpan, silahkan Cetak Kode Bayar atau SPTPD!');
                        return $this->redirect()->toRoute("laporan", array(
                            "controllers" => "Laporan",
                            "action" => "index",
                            "s_idjenis" => $req->getPost()['t_jenisobjekpajak'],
                            "t_idobjek" => $post['t_idobjek'],
                            "modaltransaksi" => $dataparent['t_idtransaksi']
                        ));
                    } else {
                        $t_masapendataan = $base->t_periodepajak . '-' . $base->t_masapajak . '-01';
                        $sebelumnya = $this->Tools()->getService('LaporanTable')->cekPelaporanSebelumnya($base->t_jenispajak, $base->t_idobjek, $t_masapendataan);
                        if (empty($sebelumnya)) { #belum melaporkan bulan sebelumnya
                            $id = $base->t_idobjek;
                            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
                            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                            $data->t_tglpendataan = date('d-m-Y');
                            $data->t_idwp = $datapendaftaran['t_idwp'];
                            $bulanseblum = date("m", strtotime($t_masapendataan . "-1 month"));
                            $tahunsebelum = date("Y", strtotime($t_masapendataan . "-1 month"));
                            $message = 'Belum Melaporkan Pajak Bulan ' . $abulan[$bulanseblum] . ' Tahun ' . $tahunsebelum . '';
                            $form->bind($data);
                            $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                    Belum Melaporkan Pajak Bulan ' . $abulan[$bulanseblum] . ' Tahun ' . $tahunsebelum . '!');
                            return $this->redirect()->toRoute("laporan", array(
                                "controllers" => "Laporan",
                                "action" => "form_pageminerba",
                                "t_idobjek" => $post['t_idobjek']
                            ));
                        } else { #sudahmelaporakan bulan sebelumnya
                            //=== cek lagi apakah lapor lebih dari bulan sekarang
                            $cek_pelaporan = $base->t_masapajak >= date('m') && $base->t_periodepajak >= date('Y');
                            if ($cek_pelaporan) { //$base->t_masapajak > date('m') && $base->t_periodepajak >= date('Y')
                                $id = $base->t_idobjek;
                                $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
                                $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                                $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                                $data->t_tglpendataan = date('d-m-Y');
                                $data->t_idwp = $datapendaftaran['t_idwp'];
                                $message = 'Belum Waktunya Laporan Bulan ' . $abulan[$base->t_masapajak];
                                $form->bind($data);
                                $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                Belum Waktunya Laporan Bulan ' . $abulan[$base->t_masapajak] . ' Tahun ' . $base->t_periodepajak . '!');
                                return $this->redirect()->toRoute("laporan", array(
                                    "controllers" => "Laporan",
                                    "action" => "form_pageminerba",
                                    "t_idobjek" => $post['t_idobjek']
                                ));
                            }
                            //==================================================
                            else {
                                $dataparent = $this->Tools()->getService('LaporanTable')->simpanpendataanself($base, $session, $post);
                                // var_dump($dataparent);exit();
                                $this->Tools()->getService('DetailminerbaTable')->simpanpendataanminerba($post, $dataparent);
                                $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/check-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
                                    Pelaporan SPT Berhasil disimpan, silahkan Cetak <b>Kode Bayar</b> dan <b>SPTPD</b>!');
                                return $this->redirect()->toRoute("laporan", array(
                                    "controllers" => "Laporan",
                                    "action" => "index",
                                    "s_idjenis" => $req->getPost()['t_jenisobjekpajak'],
                                    "t_idobjek" => $post['t_idobjek'],
                                    "modaltransaksi" => $dataparent['t_idtransaksi']
                                ));
                            }
                        }
                    }
                } else {
                    /**
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
                    $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                    $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                    $data->t_tglpendataan = date('d-m-Y');
                    $data->t_idwp = $datapendaftaran['t_idwp'];
                    $txt_bulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $message = 'Data WP untuk Bulan ' . $txt_bulan[$base->t_masapajak] . ' Tahun ' . $base->t_periodepajak . ' Sudah Pernah Dilaporkan!';
                    $form->bind($data);
					var_dump($post);die;
                    $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                Data WP untuk Bulan ' . $txt_bulan[$base->t_masapajak] . ' Tahun ' . $base->t_periodepajak . ' Sudah Pernah Dilaporkan!');
                    return $this->redirect()->toRoute("laporan", array(
                                "controllers" => "Laporan",
                                "action" => "form_pageminerba",
                                "t_idobjek" => $post['t_idobjek']
                    )); **/
                    //var_dump($post);die;
                    $dataparent = $this->Tools()->getService('LaporanTable')->simpanpendataanself($base, $session, $post);
                    $this->Tools()->getService('DetailminerbaTable')->simpanpendataanminerba($post, $dataparent);
                    //var_dump($detailminerba);exit();

                    $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/check-green.jpg" width="100" style="margin-bottom: 5px;"><br>
						<span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
						Pelaporan SPT Berhasil disimpan, silahkan Cetak <b>Kode Bayar</b> dan <b>SPTPD</b>!');
                    return $this->redirect()->toRoute("laporan", array(
                        "controllers" => "Laporan",
                        "action" => "index",
                        "s_idjenis" => $req->getPost()['t_jenisobjekpajak'],
                        "t_idobjek" => $post['t_idobjek'],
                        "modaltransaksi" => $dataparent['t_idtransaksi']
                    ));
                }
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'rekeningmineral' => $recordsrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $recordspajak = $this->Tools()->getService('ObjekTable')->getDataObjekLeftMenu($session['s_wp']);
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'laporanaktif' => 1,
            'sub_menu_objekwp' => $datapendaftaran['t_idobjek'], //3804,
            'sub_menu_jenispajak' => $datapendaftaran['t_jenisobjek'], //$allParams['s_idjenis'],
        );
        //        $data = array(
        //            'data_pemda' => $ar_pemda,
        //            'datauser' => $session,
        //            'dataobjek' => $recordspajak,
        //            'laporanaktif' => 1,
        //        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageminerbaeditAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new LaporanFrm();
        if ($req->isGet()) {
            $message = '';
            $id = $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_tglpendataan = date('d-m-Y', strtotime($data->t_tglpendataan));
            $data->t_masaawal = date('d-m-Y', strtotime($data->t_masaawal));
            $data->t_masaakhir = date('d-m-Y', strtotime($data->t_masaakhir));
            $data->t_jmlhpajak = number_format($data->t_jmlhpajak, 0, ',', '.');
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');

            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = 25; //$datatransaksi['s_persentarifkorek'];
            $data->t_masapajak = date('m', strtotime($datatransaksi['t_masaawal']));
            $form = new LaporanFrm($this->Tools()->getService('LaporanTable')->getBulanBelumLaporEdit($datapendaftaran['t_idobjek']));
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            $form = new LaporanFrm($abulan);
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailminerbaTable')->getDetailMinerbaByIdTransaksi($id);
        }

        $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningMineral();
        $recordsrekening = array();
        foreach ($rekening as $rekening) {
            $recordsrekening[] = $rekening;
        }

        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'datadetail' => $datadetail,
            'rekeningmineral' => $recordsrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $recordspajak = $this->Tools()->getService('ObjekTable')->getDataObjekLeftMenu($session['s_wp']);
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'laporanaktif' => 1,
            'sub_menu_objekwp' => $datapendaftaran['t_idobjek'], //3804,
            'sub_menu_jenispajak' => $datapendaftaran['t_jenisobjek'], //$allParams['s_idjenis'],
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    //PARKIR
    public function FormPageparkirAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $data_get = $req->getPost();
        if ($req->isGet()) {
            $id = $allParams['s_idjenis']; // ini sebenarnya id objek wp
            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
            $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningParkir();
            $recordsrekening = array();
            foreach ($rekening as $rekening) {
                $recordsrekening[] = $rekening;
            }
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datakorekwp = $this->Tools()->getService('LaporanTable')->getKorekWp($id);
            $data->t_idkorek = $datakorekwp['s_idkorek'];
            $data->t_tarifpajak = $datakorekwp['s_persentarifkorek'];
            $tahun = date('Y');
            $datapendataan = $this->Tools()->getService('LaporanTable')->getLaporanMaxByPeriode($tahun);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            $form = new LaporanFrm($abulan);
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            $form = new LaporanFrm($abulan);
            $base = new LaporanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $sudahditetapkan = $this->Tools()->getService('LaporanTable')->getLaporanSeMasaWp($base, $post["t_idtransaksi"]);
                //apabila katering
                $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($base->t_idobjek);
                if ($datapendaftaran['s_objekkorek'] == 02 && $datapendaftaran['s_rinciankorek'] == 05) {
                    $dataparent = $this->Tools()->getService('LaporanTable')->simpanpendataanself($base, $session, $post);
                    $hislog_action = 'Menyimpan Data Pelaporan ' . $base->t_idobjek . '/' . $base->t_idkorek . '/' . $base->t_jenispajak;
                    $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
                    $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/check-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
                                    Pelaporan SPT Berhasil disimpan, silahkan Cetak Kode Bayar atau SPTPD!');
                    return $this->redirect()->toRoute("laporan", array(
                        "controllers" => "Laporan",
                        "action" => "index",
                        "s_idjenis" => $req->getPost()['t_jenisobjekpajak'],
                        "t_idobjek" => $post['t_idobjek']
                    ));
                }
                //============ end katering

                if (empty($sudahditetapkan)) {

                    #cekpealporansebelumnya
                    $belumpernahlapor = $this->Tools()->getService('LaporanTable')->cekBelumLapor($post['t_idobjek']);
                    if (empty($belumpernahlapor)) {

                        $dataparent = $this->Tools()->getService('LaporanTable')->simpanpendataanself($base, $session, $post);
                        $this->Tools()->getService('DetailparkirTable')->simpanpendataanparkir($post, $dataparent);
                        $hislog_action = 'Menyimpan Data Pelaporan ' . $base->t_idobjek . '/' . $base->t_idkorek . '/' . $base->t_jenispajak;
                        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
                        $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/check-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
                                    Pelaporan SPT Berhasil disimpan, silahkan Cetak Kode Bayar atau SPTPD!');
                        return $this->redirect()->toRoute("laporan", array(
                            "controllers" => "Laporan",
                            "action" => "index",
                            "s_idjenis" => $req->getPost()['t_jenisobjekpajak'],
                            "t_idobjek" => $post['t_idobjek']
                        ));
                    } else {
                        $t_masapendataan = $base->t_periodepajak . '-' . $base->t_masapajak . '-01';
                        $sebelumnya = $this->Tools()->getService('LaporanTable')->cekPelaporanSebelumnya($base->t_jenispajak, $base->t_idobjek, $t_masapendataan);
                        if (empty($sebelumnya)) { #belum melaporkan bulan sebelumnya
                            $id = $base->t_idobjek;
                            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
                            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                            $data->t_tglpendataan = date('d-m-Y');
                            $data->t_idwp = $datapendaftaran['t_idwp'];
                            $bulanseblum = date("m", strtotime($t_masapendataan . "-1 month"));
                            $tahunsebelum = date("Y", strtotime($t_masapendataan . "-1 month"));
                            $message = 'Belum Melaporkan Pajak Bulan ' . $abulan[$bulanseblum] . ' Tahun ' . $tahunsebelum . '';
                            $form->bind($data);
                            $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                    Belum Melaporkan Pajak Bulan ' . $abulan[$bulanseblum] . ' Tahun ' . $tahunsebelum . '!');
                            return $this->redirect()->toRoute("laporan", array(
                                "controllers" => "Laporan",
                                "action" => "form_pageminerba",
                                "t_idobjek" => $post['t_idobjek']
                            ));
                        } else { #sudahmelaporakan bulan sebelumnya
                            //=== cek lagi apakah lapor lebih dari bulan sekarang
                            if ($base->t_masapajak == 1 && $base->t_periodepajak == date('Y')) {
                                $cek_pelaporan = $base->t_masapajak > date('m') || $base->t_periodepajak > date('Y');
                            } else {
                                $cek_pelaporan = ($base->t_masapajak > date('m') || $base->t_periodepajak >= date('Y')) && $base->t_periodepajak >= date('Y');
                            }

                            if ($cek_pelaporan) { //$base->t_masapajak > date('m') && $base->t_periodepajak >= date('Y')
                                $id = $base->t_idobjek;
                                $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
                                $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                                $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                                $data->t_tglpendataan = date('d-m-Y');
                                $data->t_idwp = $datapendaftaran['t_idwp'];
                                $message = 'Belum Waktunya Laporan Bulan ' . $abulan[$base->t_masapajak];
                                $form->bind($data);
                                $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                Belum Waktunya Laporan Bulan ' . $abulan[$base->t_masapajak] . ' Tahun ' . $base->t_periodepajak . '!');
                                return $this->redirect()->toRoute("laporan", array(
                                    "controllers" => "Laporan",
                                    "action" => "form_pageminerba",
                                    "t_idobjek" => $post['t_idobjek']
                                ));
                            }
                            //==================================================
                            else {
                                $dataparent = $this->Tools()->getService('LaporanTable')->simpanpendataanself($base, $session, $post);
                                $this->Tools()->getService('DetailparkirTable')->simpanpendataanparkir($post, $dataparent);
                                $hislog_action = 'Menyimpan Data Pelaporan ' . $base->t_idobjek . '/' . $base->t_idkorek . '/' . $base->t_jenispajak;
                                $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
                                $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/check-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
                                    Pelaporan SPT Berhasil disimpan, silahkan Cetak <b>Kode Bayar</b> dan <b>SPTPD</b>!');
                                return $this->redirect()->toRoute("laporan", array(
                                    "controllers" => "Laporan",
                                    "action" => "index",
                                    "s_idjenis" => $req->getPost()['t_jenisobjekpajak'],
                                    "t_idobjek" => $post['t_idobjek']
                                ));
                            }
                        }
                    }
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($id);
                    $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
                    $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                    $data->t_tglpendataan = date('d-m-Y');
                    $data->t_idwp = $datapendaftaran['t_idwp'];
                    $txt_bulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $message = 'Data WP untuk Bulan ' . $txt_bulan[$base->t_masapajak] . ' Tahun ' . $base->t_periodepajak . ' Sudah Pernah Dilaporkan!';
                    $form->bind($data);
                    $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                Data WP untuk Bulan ' . $txt_bulan[$base->t_masapajak] . ' Tahun ' . $base->t_periodepajak . ' Sudah Pernah Dilaporkan!');
                    return $this->redirect()->toRoute("laporan", array(
                        "controllers" => "Laporan",
                        "action" => "form_pageminerba",
                        "t_idobjek" => $post['t_idobjek']
                    ));
                }
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'rekeningparkir' => $recordsrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $recordspajak = $this->Tools()->getService('ObjekTable')->getDataObjekLeftMenu($session['s_wp']);
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'laporanaktif' => 1,
            'sub_menu_objekwp' => $datapendaftaran['t_idobjek'], //3804,
            'sub_menu_jenispajak' => $datapendaftaran['t_jenisobjek'], //$allParams['s_idjenis'],
        );
        //        $data = array(
        //            'data_pemda' => $ar_pemda,
        //            'datauser' => $session,
        //            'dataobjek' => $recordspajak,
        //            'laporanaktif' => 1,
        //        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageparkireditAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new LaporanFrm();
        if ($req->isGet()) {
            $message = '';
            $id = $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_tglpendataan = date('d-m-Y', strtotime($data->t_tglpendataan));
            $data->t_masaawal = date('d-m-Y', strtotime($data->t_masaawal));
            $data->t_masaakhir = date('d-m-Y', strtotime($data->t_masaakhir));
            $data->t_jmlhpajak = number_format($data->t_jmlhpajak, 0, ',', '.');
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');
            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            if ($datapendaftaran['t_jenisobjek'] == 2) {
                if ($datatransaksi['s_idkorek'] == 21) {
                    $t_berdasarmasa = 'Tidak Berdasar Masa';
                } else {
                    $t_berdasarmasa = 'Berdasar Masa';
                }
            } else {
                $t_berdasarmasa = null;
            }
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_masapajak = date('m', strtotime($datatransaksi['t_masaawal']));
            $hislog_action = 'Edit Data Pelaporan ' . $datapendaftaran['t_idobjek'] . '/' . $datatransaksi['s_idkorek'] . '/' . $datapendaftaran['t_jenisobjek'];
            $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
            $form = new LaporanFrm($this->Tools()->getService('LaporanTable')->getBulanBelumLaporEdit($datapendaftaran['t_idobjek']));
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            //apabila katering
            if ($datapendaftaran['s_objekkorek'] == 02 && $datapendaftaran['s_rinciankorek'] == 05)
                $form = new LaporanFrm($abulan);
            $form->bind($data);
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            't_berdasarmasa' => $t_berdasarmasa,
            //            't_rincianomzet' => $datatransaksi['t_rincianomzet']
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $recordspajak = $this->Tools()->getService('ObjekTable')->getDataObjekLeftMenu($session['s_wp']);
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'laporanaktif' => 1,
            'sub_menu_objekwp' => $datapendaftaran['t_idobjek'], //3804,
            'sub_menu_jenispajak' => $datapendaftaran['t_jenisobjek'], //$allParams['s_idjenis'],
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function hapusAction()
    {
        /** Hapus Laporan
         * @param int $s_idkorek
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 04/11/2016
         */
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $this->Tools()->getService('LaporanTable')->hapusLaporan($this->params('s_idkorek'), $session);
        return $this->getResponse();
    }

    public function cetakkodebayarAction()
    {
        /** Cetak Kode Bayar
         * @param int $t_idwp
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 22/09/2018
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();

        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        // Mengambil Data WP
        $data = $this->Tools()->getService('LaporanTable')->getDataPembayaranID($data_get['t_idtransaksi']);

        $selfassesment = [1, 2, 3, 5, 6, 7, 8];
        $data['sanksiadm'] = 0;
        if (in_array($data['t_jenisobjek'], $selfassesment)) {
            $datajenis = $this->Tools()->getService('LaporanTable')->getDataJenis($data['t_jenisobjek']);
            $datelapor = $datelapor = $data['t_masaakhir'];
            if ($data['korek'] == '4.1.01.07.07.001') {
                $datelapor = $data['t_tgljatuhtempo'];
            }
            $bataslapor = date('Y-m-d', strtotime('+' . $datajenis['t_akhirlapor'] . ' days', strtotime($datelapor)));
            if (date('Y-m-d') > $bataslapor) {
                $datasanksi = $this->Tools()->getService('LaporanTable')->getSanksiAktif();
                $data['sanksiadm'] = $datasanksi['s_sanksi'] != null ? $datasanksi['s_sanksi'] : 0;
            }
        }

        $hislog_action = 'Mencetak Kode Bayar ' . $data['t_kodebayar'];
        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'data' => $data,
            'ar_pemda' => $ar_pemda,
        ));
        $pdf->setOption("paperSize", "potrait");
        return $pdf;
    }

    public function cetaksptpdhotelAction()
    {
        /** Cetak SPTPD Hotel
         * @param int $idtransaksi
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();

        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningByJenis(1);
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('LaporanTable')->getDataLaporanID($data_get->t_idtransaksi);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);

        $hislog_action = 'Mencetak SPTPD ' . $datasekarang['t_jenisobjek'] . '/' . $datasekarang['t_idwpobjek'] . '/' . $datasekarang['t_masaawal'];
        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdrestoranAction()
    {
        /** Cetak SPTPD Restoran
         * @param int $idtransaksi
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningByJenis(2);
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('LaporanTable')->getDataLaporanID($data_get->t_idtransaksi);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);

        $hislog_action = 'Mencetak SPTPD ' . $datasekarang['t_jenisobjek'] . '/' . $datasekarang['t_idwpobjek'] . '/' . $datasekarang['t_masaawal'];
        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdhiburanAction()
    {
        /** Cetak SPTPD Hiburan
         * @param int $idtransaksi
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningSubByJenis(3);
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('LaporanTable')->getDataLaporanID($data_get->t_idtransaksi);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);

        $hislog_action = 'Mencetak SPTPD ' . $datasekarang['t_jenisobjek'] . '/' . $datasekarang['t_idwpobjek'] . '/' . $datasekarang['t_masaawal'];
        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);

        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdppjAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['t_idtransaksi']);
        $detailppjsekarang = $this->Tools()->getService('DetailPpjTable')->getPendataanByIdTransaksi($data_get['t_idtransaksi']);
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
        // $datadetailppj = $this->Tools()->getService('PendataanTable')->getDataDetailPPJ($data_get->t_idtransaksi);
        // $tarifdasarkorek = $this->Tools()->getService('PendataanTable')->getDataDetailPPJ($data_get->t_idtransaksi);

        $hislog_action = 'Mencetak SPTPD ' . $datasekarang['t_jenisobjek'] . '/' . $datasekarang['t_idwpobjek'] . '/' . $datasekarang['t_masaawal'];
        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
        // var_dump($data_get['t_idtransaksi']);die();
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        //        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'detailppjsekarang' => $detailppjsekarang,
            // 'data_detailppj' => $datadetailppj,
            'ar_pemda' => $ar_pemda,
            // 'rekening' => $rekening,
            // 't_tarifdasarkorek' => $t_tarifdasarkorek['s_persentarifkorek'],
            // 'petugas_penerima' => $petugas_penerima,
            // 'operator' => $ar_operator
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdminerbaAction()
    {
        /** Cetak SPTPD MINERBA
         * @param int $idtransaksi
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 18/02/2019
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('LaporanTable')->getDataLaporanID($data_get->t_idtransaksi);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);

        // Mengambil data Detail Minerba
        $datadetailminerba = $this->Tools()->getService('PendataanTable')->getDataDetailMinerba($data_get->t_idtransaksi);
        //var_dump($datadetailminerba);die;

        $hislog_action = 'Mencetak SPTPD ' . $datasekarang['t_jenisobjek'] . '/' . $datasekarang['t_idwpobjek'] . '/' . $datasekarang['t_masaawal'];
        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();

        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'dataminerba' => $datadetailminerba,
            'ar_pemda' => $ar_pemda,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdwaletAction()
    {
        /** Cetak SPTPD WALET
         * @param int $idtransaksi
         * @author BaCode <>
         * @date 12/03/2022
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil rekening
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningByJenis(9);
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('LaporanTable')->getDataLaporanID($data_get->t_idtransaksi);
        //var_dump($rekening);die;
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
        // Mengambil data Detail Minerba
        $datadetailwalet = $this->Tools()->getService('PendataanTable')->getDataDetailWalet($data_get->t_idtransaksi);
        //var_dump($datadetailwalet);die;

        $hislog_action = 'Mencetak SPTPD ' . $datasekarang['t_jenisobjek'] . '/' . $datasekarang['t_idwpobjek'] . '/' . $datasekarang['t_masaawal'];
        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();

        //$ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        //$ar_operator = $this->Tools()->getService('UserTable')->getUserId($datasekarang['t_operatorpendataan']);

        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'rekening' => $rekening,
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'datawalet' => $datadetailwalet,
            'ar_pemda' => $ar_pemda,
            //'ar_ttd' => $ar_ttd,
            //'operator' => $ar_operator
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdparkirAction()
    {
        /** Cetak SPTPD Parkir
         * @param int $idtransaksi
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        // $rekening = $this->Tools()->getService('RekeningTable')->getRekeningSubByJenis(7);
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('LaporanTable')->getDataLaporanID($data_get->t_idtransaksi);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('LaporanTable')->getDataLaporanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
        $datadetailparkir = $this->Tools()->getService('PendataanTable')->getDataDetailParkir($data_get->t_idtransaksi);

        $hislog_action = 'Mencetak SPTPD ' . $datasekarang['t_jenisobjek'] . '/' . $datasekarang['t_idwpobjek'] . '/' . $datasekarang['t_masaawal'];
        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'dataparkir' => $datadetailparkir,
            'ar_pemda' => $ar_pemda,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function hitungpajakAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_dasarpengenaan = str_ireplace(".", "", $data_get['t_dasarpengenaan']);
        $t_tarifpajak = $data_get['t_tarifpajak'];
        $t_jmlhpajak = ($t_dasarpengenaan * $t_tarifpajak / 100);
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_tarifpajak" => $t_tarifpajak
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }
    //WALET
    public function hitungpajakwaletAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        //var_dump($data_get);die;
        $t_umurbangunan = '1';
        $t_jenissarang = '1';
        if ($t_umurbangunan == '1' || $t_umurbangunan == 1) {
            $t_tarifpajak = 2.5;
        } elseif ($t_umurbangunan == '2' || $t_umurbangunan == 2) {
            $t_tarifpajak = 2.5;
        } else {
            $t_tarifpajak = 2.5;
        }
        //$hargawalet = $this->Tools()->getService('LaporanTable')->getHargaDasarWalet(1);
        //$harga_dasar = $hargawalet['s_harga_dasar'];
        //$t_dasarpengenaan = $data_get['t_nilaiperolehan1'] * $harga_dasar;
        //$t_jmlhpajak = ($data_get['t_nilaiperolehan1'] * $harga_dasar) * $t_tarifpajak / 100;

        //$hargawalet = $this->Tools()->getService('LaporanTable')->getHargaDasarWalet(1);
        $harga_dasar = str_replace(".", "", $data_get['t_hargadasar']);
        $nilaiperolehan = $data_get['t_nilaiperolehan1'];
        //var_dump(str_replace(".", "", $harga_dasar));die;
        $t_dasarpengenaan = $data_get['t_nilaiperolehan1'] * $harga_dasar;
        //$t_jmlhpajak = ($data_get['t_nilaiperolehan1'] * $harga_dasar) * $t_tarifpajak / 100;
        $t_jmlhpajak = $t_dasarpengenaan * $t_tarifpajak / 100;

        //var_dump($t_jmlhpajak);die;
        //var_dump($data_get['t_nilaiperolehan1'] * $harga_dasar);die;
        //var_dump($t_dasarpengenaan,($harga_dasar ),"*",( $data_get['t_nilaiperolehan1']),$t_tarifpajak, $t_jmlhpajak);die;
        //var_dump($data_get['t_nilaiperolehan1'] ,"*", $harga_dasar, $data_get['t_nilaiperolehan1'] * $harga_dasar);die;
        //var_dump($harga_dasar, $t_jmlhpajak, $t_dasarpengenaan);die;
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_tarifpajak" => $t_tarifpajak,
            "t_umurbangunan" => $t_umurbangunan,
            "t_jenissarang" => $t_jenissarang,
            "t_dasarpengenaan" => number_format($t_dasarpengenaan, 0, ",", "."),
            "t_hargadasar" => $harga_dasar
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function ambilhargadasarAction()
    {

        $req = $this->getRequest();
        $data_get = $req->getPost();

        //var_dump($data_get);die;

        $hargawalet = $this->Tools()->getService('LaporanTable')->getHargaDasarWalet(1);
        $harga_dasar = $hargawalet['s_harga_dasar'];

        $data_render = array(
            // "t_hargadasar" => $harga_dasar
            "t_hargadasar" => number_format($harga_dasar, 0, ",", ".")
        );
        // //var_dump($data_render);die;
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    //MINERBA
    public function caritarifminerbaAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();

        $dataRekening = $this->Tools()->getService('RekeningTable')->getDataRekeningMinerba($data_get['t_idkorek']);
        foreach ($dataRekening as $v) {
            $t_tarifpersen = $v['s_persentarifkorek'];
            $s_tarifdasarkorek = $v['s_tarifdasarkorek'];
        }

        $data = array(
            't_tarifpersen' => $t_tarifpersen,
            's_tarifdasarkorek' => number_format($s_tarifdasarkorek, 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungpajakminerbaAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(",", ".", $data_get['t_volume']) * str_ireplace(".", "", $data_get['t_hargapasaran']);
        //$t_pajak = $t_jumlah * $data_get['t_tarifpersen'] / 100;
        $data = array(
            't_jumlah' => number_format($t_jumlah, 0, ",", "."),
            //'t_pajak' => number_format($t_pajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }


    public function hitungtotalpajakminerbaAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jumlah1']) + str_ireplace(".", "", $data_get['t_jumlah2']) + str_ireplace(".", "", $data_get['t_jumlah3']) + str_ireplace(".", "", $data_get['t_jumlah4']) + str_ireplace(".", "", $data_get['t_jumlah5']) + str_ireplace(".", "", $data_get['t_jumlah6']) + str_ireplace(".", "", $data_get['t_jumlah7'])  + str_ireplace(".", "", $data_get['t_jumlah8']) + str_ireplace(".", "", $data_get['t_jumlah9']) + str_ireplace(".", "", $data_get['t_jumlah10']);
        //$t_jmlhpajak = str_ireplace(".", "", $data_get['t_pajak1']) + str_ireplace(".", "", $data_get['t_pajak2']) + str_ireplace(".", "", $data_get['t_pajak3']) + str_ireplace(".", "", $data_get['t_pajak4']) + str_ireplace(".", "", $data_get['t_pajak5']) + str_ireplace(".", "", $data_get['t_pajak6']) + str_ireplace(".", "", $data_get['t_pajak7'])  + str_ireplace(".", "", $data_get['t_pajak8']) + str_ireplace(".", "", $data_get['t_pajak9']) + str_ireplace(".", "", $data_get['t_pajak10']);
        //$t_pajak = $t_jmlhpajak;
        $t_pajak = $t_jumlah;
        $data = array(
            't_dasarpengenaan' => number_format($t_jumlah, 0, ",", "."),
            't_jmlhpajak' => number_format($t_pajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    //PARKIR
    public function caritarifparkirAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataRekening = $this->Tools()->getService('RekeningTable')->getDataRekeningId($data_get['t_idkorek']);
        $data = array(
            //            't_hargapasaran' => number_format($dataRekening['s_tarifdasarkorek'], 0, ",", "."),
            't_tarifpersen' => $dataRekening['s_persentarifkorek']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungpajakparkirAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jmlh_kendaraan']) * str_ireplace(".", "", $data_get['t_hargadasar']);
        $t_pajak = $t_jumlah * $data_get['t_tarifpersen'] / 100;
        $data = array(
            't_jumlah' => number_format($t_jumlah, 0, ",", "."),
            't_pajak' => number_format($t_pajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalpajakparkirAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jumlah1']) + str_ireplace(".", "", $data_get['t_jumlah2']) + str_ireplace(".", "", $data_get['t_jumlah3']) + str_ireplace(".", "", $data_get['t_jumlah4']) + str_ireplace(".", "", $data_get['t_jumlah5']) + str_ireplace(".", "", $data_get['t_jumlah6']) + str_ireplace(".", "", $data_get['t_jumlah7']);
        $t_jmlhpajak = str_ireplace(".", "", $data_get['t_pajak1']) + str_ireplace(".", "", $data_get['t_pajak2']) + str_ireplace(".", "", $data_get['t_pajak3']) + str_ireplace(".", "", $data_get['t_pajak4']) + str_ireplace(".", "", $data_get['t_pajak5']) + str_ireplace(".", "", $data_get['t_pajak6']) + str_ireplace(".", "", $data_get['t_pajak7']);
        $t_pajak = $t_jmlhpajak;
        $data = array(
            't_dasarpengenaan' => number_format($t_jumlah, 0, ",", "."),
            't_jmlhpajak' => number_format($t_pajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function CariLaporanByObjekAction()
    {
        $req = $this->getRequest();
        $abulan = array('1' => 'Januari', '2' => 'Februari', '3' => 'Maret', '4' => 'April', '5' => 'Mei', '6' => 'Juni', '7' => 'Juli', '8' => 'Agustus', '9' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
        $data_get = $req->getPost();
        $datatransaksi = "  <div style='overflow: auto'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 9pt; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th colspan='8' style='background-color: #3c8dbc; color: white; text-align:center'><b>Data Transaksi Tahun : " . $data_get['periodepajak'] . "</b></th>
                                            </tr
                                            <tr>
                                                <th rowspan='2' style='background-color: #3c8dbc; color: white; text-align:center;vertical-align:middle'>No.</th>
                                                <th rowspan='2' style='background-color: #3c8dbc; color: white; text-align:center;vertical-align:middle'>Masa Pajak</th>
                                                <th colspan='4' style='background-color: #3c8dbc; color: white; text-align:center'>Laporan</th>
                                                <th colspan='2' style='background-color: #3c8dbc; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #3c8dbc; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #3c8dbc; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #3c8dbc; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #3c8dbc; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #3c8dbc; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #3c8dbc; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataawal = $this->Tools()->getService('ObjekTable')->getPendataanAwalbyIdObjek($data_get['idobjek']);
        $dataRek = $this->Tools()->getService('ObjekTable')->getRekeningById($dataawal['t_idkorek']);
        if ($dataRek['s_objekkorek'] == 2 && $dataRek['s_rinciankorek'] == 5) {
            $rowKatering = $this->Tools()->getService('ObjekTable')->getPendataanKatering($data_get['periodepajak'], $data_get['idobjek']);
            //$i = 1;
            $n = 1;
            foreach ($rowKatering as $row) {
                $i = date('m', strtotime($row['t_masaawal'])) * 1;
                $t_tglpembayaran = ($row['t_tglpembayaran'] != '' ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
                $t_jmlhpembayaran = ($row['t_tglpembayaran'] != '' ? number_format($row['t_jmlhpembayaran'], 0, ',', '.') : '-'); // returns true
                $datatransaksi .= "         <tr>
                                                <td data-title='No.'><center>" . $n . "</center></td>
                                                <td data-title='Masa Pajak'><center>" . $abulan[$i] . "</center></td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>" . $t_jmlhpembayaran . "</td>
                                            </tr>";
                $n = $n + 1;
            }
        } else {
            for ($i = 1; $i <= 12; $i++) {
                $row = $this->Tools()->getService('ObjekTable')->getPendataanbyMasa($i, $data_get['periodepajak'], $data_get['idobjek']);
                $abulan = array('1' => 'Januari', '2' => 'Februari', '3' => 'Maret', '4' => 'April', '5' => 'Mei', '6' => 'Juni', '7' => 'Juli', '8' => 'Agustus', '9' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                if ($row == false) {
                    $tglpembanding = $data_get['periodepajak'] . "-" . str_pad($i, 2, '0', STR_PAD_LEFT) . "-01";
                    if ($dataawal['t_masaawal'] <= date('Y-m-t', strtotime($tglpembanding)) && date('Y-m-d') >= date('Y-m-t', strtotime($tglpembanding))) { //jika belum melaporkan pajak dan pernah melaporkan sebelumnya
                        $dataparameter = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01";
                        $dataparameterTeguran = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01";
                        $datatransaksi .= "     <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Masa Pajak'>" . $abulan[$i] . "</td>
                                                <td data-title='Keterangan' colspan='6' style='text-align:center'><b style='color:red; font-size:12px'>BELUM LAPOR</b></td>
                                            </tr>";
                    } else { // data sebelum wp daftar dan dilakukan pendataan sama sekali
                        $datatransaksi .= "     <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Masa Pajak'>" . $abulan[$i] . "</td>
                                                <td data-title='Kode Rekening'>-</td>
                                                <td data-title='Tgl' style='text-align:center'>-</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>-</td>
                                                <td data-title='Pajak' style='text-align:right'>-</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>-</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>-</td>
                                            </tr>";
                    }
                } else {
                    $t_tglpembayaran = ($row['t_tglpembayaran'] != '' ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
                    $t_jmlhpembayaran = ($row['t_tglpembayaran'] != '' ? number_format($row['t_jmlhpembayaran'], 0, ',', '.') : '-'); // returns true
                    if ($row['t_jmlhpajak'] == 0) {
                        $pembayaran_status = "<td colspan='2' data-title='Tgl. Bayar' style='color:blue;text-align:center'><b>NIHIL</b></td>";
                    } else {
                        $pembayaran_status = "<td data-title='Tgl. Bayar' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>" . $t_jmlhpembayaran . "</td>";
                    }
                    $datatransaksi .= "         <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Masa Pajak'>" . $abulan[$i] . "</td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                " . $pembayaran_status . "
                                            </tr>";
                }
            }
        }
        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cetaksspdAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP
        $data = $this->Tools()->getService('LaporanTable')->getDataPembayaranID($data_get->t_idtransaksi);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'ar_pemda' => $ar_pemda
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function ubahpassbackAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $message = '';
        $data = $this->Tools()->getService('UserTable')->getUserId($session['s_iduser']);
        $frm = new \Pajak\Form\Setting\PenggunaFrm();
        $frm->bind($data);
        if ($this->getRequest()->isPost()) {
            $bs = new \Pajak\Model\Setting\SettingUserBase();
            $frm2 = new \Pajak\Form\Setting\PenggunaFrm();
            $frm2->setInputFilter($bs->getInputFilter());
            $frm2->setData($this->getRequest()->getPost());
            if (!$frm2->isValid()) {
                $bs->exchangeArray($frm2->getData());
                $this->Tools()->getService('UserTable')->savepassword($bs, $session);
                $hislog_action = 'Merubah Password userID' . $session['s_iduser'] . '/' . $session['s_username'];
                $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
            }
        }
        $view = new ViewModel(array(
            'frm' => $frm,
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $recordspajak = $this->Tools()->getService('ObjekTable')->getDataObjekLeftMenu($session['s_wp']);
        $datane = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'ubahpassaktif' => 1
        );
        $this->layout()->setVariables($datane);
        return $view;
    }

    public function CariRincianTanggalAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $tgl = date('t-m-Y', strtotime('01-' . $data_get->t_masapajak . '-' . $data_get->t_periodepajak));
        $ambiltanggal = date('d', strtotime($tgl));
        $rinciantanggal = "  <div style='overflow: auto'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th colspan='3' style='background-color: #00BCA4; color: red; text-align:center'><b>Rincian Omzet Per Hari</b></th>
                                            </tr
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>No. Bill</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pandapatan/Omzet (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $i = 1;
        for ($i; $ambiltanggal >= $i; $i++) {
            $rinciantanggal .= "     <tr>
                                        <td>
                                            <input type='hidden' class='form-control' name='t_iddetailomzet[]' id='t_iddetailomzet$i'>
                                            <input type='text' class='form-control' name='t_tglomzet[]' id='t_tglomzet$i' value='" . str_pad($i, 2, '0', STR_PAD_LEFT) . "-" . date('m', strtotime($tgl)) . "-" . date('Y', strtotime($tgl)) . "' style='text-align:center' readonly='true'>
                                        </td>
                                        <td><input type='text' class='form-control' name='t_nobill[]' id='t_nobill$i'></td>
                                        <td><input type='text' class='form-control' name='t_omzet[]' id='t_omzet$i' style='text-align:right' onchange='this.value = formatCurrency(this.value);HitungRincianOmzet(this.value)' onblur='this.value = formatCurrency(this.value);HitungRincianOmzet(this.value)' onKeyPress='return numbersonly(this, event);' onkeyup='this.value = formatCurrency(this.value);' value='0'></td>
                                    </tr>";
        }
        $rinciantanggal .= "<script type='text/javascript'>
                                function HitungRincianOmzet(a) {
                                    $.post('../HitungRincianOmzet', {t_tarifpajak: $('#t_tarifpajak').val()
                                    , t_masapajak: $('#t_masapajak').val(), t_periodepajak: $('#t_periodepajak').val()
                                    , t_dasarpengenaan: $('#t_dasarpengenaan').val()";
        $j = 1;
        for ($j; $ambiltanggal >= $j; $j++) {
            $rinciantanggal .= ", t_omzet$j: $('#t_omzet$j').val()";
        }
        $rinciantanggal .= "        }, function (data) {
                                        var aa = JSON.parse(data);
                                        $('#t_dasarpengenaan').val(aa.t_dasarpengenaan);
                                        $('#t_jmlhpajak').val(aa.t_jmlhpajak);
                                    });
                                }
                            </script>";

        $rinciantanggal .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "t_dasarpengenaan" => 0,
            "t_jmlhpajak" => 0,
            "rinciantanggal" => $rinciantanggal
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function HitungRincianOmzetAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $tgl = date('t-m-Y', strtotime('01-' . $data_get->t_masapajak . '-' . $data_get->t_periodepajak));
        $ambiltanggal = date('d', strtotime($tgl));
        $t_dasarpengenaan = 0;
        $j = 1;
        for ($j; $ambiltanggal >= $j; $j++) {
            $omzet = "t_omzet" . $j;
            $t_dasarpengenaan += str_ireplace(".", "", $data_get->$omzet);
        }

        $t_jmlhpajak = $t_dasarpengenaan * $data_get->t_tarifpajak / 100;
        $data_render = array(
            "t_dasarpengenaan" => number_format($t_dasarpengenaan, 0, ",", "."),
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function indexskpdAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        //        var_dump($session);exit();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $view = new ViewModel(array());
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'laporanskpdaktif' => 1,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function datagridlaporanskpdAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $input = $this->getRequest();
        $aColumns = array('t_idtransaksi', 't_nourut', 't_tglpendataan', 't_nama', 't_namaobjek', 't_masaawal', 't_jmlhpajak', 't_tglpembayaran', 't_idtransaksi');
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $rResult = $this->Tools()->getService('LaporanTable')->griddatalaporanskpd($input, $aColumns, $session, $this->cekurl(), $allParams);
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());

        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        $scheme = $uri->getScheme();
        $host = $uri->getHost();
        $port = $uri->getPort();

        // Check if port exists and non-standard, then append it
        $portSuffix = ($port && !in_array($port, [80, 443])) ? ":" . $port : "";

        return $scheme . '://' . $host . $portSuffix . $uri->getPath();
    }


    public function formpageskpdAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $dataskpd = $this->Tools()->getService('LaporanTable')->getdataskpd($session);
        if ($req->isGet()) {
            $message = '';
            $tahun = date('Y');
            $datapendataan = $this->Tools()->getService('LaporanTable')->getLaporanMaxByPeriode($tahun);
            $data = $this->Tools()->getService('PemdaTable')->getdata();
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            $form = new LaporanFrm($abulan);
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            $form = new LaporanFrm($abulan);
            $base = new LaporanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $sudahditetapkan = $this->Tools()->getService('LaporanTable')->getLaporanSeMasaWp($base, $post["t_idtransaksi"]);
                //apabila katering
                $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($base->t_idobjek);
                if (!empty($datapendaftaran['s_objekkorek'])) {
                    $katering = $datapendaftaran['s_objekkorek'] == 02 && $datapendaftaran['s_rinciankorek'] == 05;
                } else {
                    $katering = $base->t_jenispajak == 2 && $base->t_idkorek == 27;
                }
                $post['t_jenisobjekpajak'] = $base->t_jenispajak;
                if ($katering) {
                    $dataparent = $this->Tools()->getService('LaporanTable')->simpanpendataanself($base, $session, $post);
                    $hislog_action = 'Menyimpan Data Pelaporan ' . $base->t_idobjek . '/' . $base->t_idkorek . '/' . $base->t_jenispajak;
                    $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
                    $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/check-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                <span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
                                Pelaporan SPT Berhasil disimpan, silahkan Cetak <b>Kode Bayar</b> dan <b>SPTPD</b>!');
                    return $this->redirect()->toRoute("laporan", array(
                        "controllers" => "Laporan",
                        "action" => "indexskpd",
                    ));
                } else {
                    $this->flashMessenger()->addMessage('<img src="' . $this->cekurl() . '/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                Objek Pajak Belum Terdaftar di Aplikasi e-SPTPD, silahkan lapor ke Kantor BAPENDA!');
                    return $this->redirect()->toRoute("laporan", array(
                        "controllers" => "Laporan",
                        "action" => "formpageskpd",
                    ));
                }
                //============ end katering
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
            'message' => $message,
            'dataskpd' => $dataskpd,
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function datagridobjekpajakAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $input = $this->getRequest();
        $aColumns = array('t_idobjek', 't_nama', 't_namaobjek', 's_namajenis', 't_idobjek');
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $rResult = $this->Tools()->getService('LaporanTable')->datagridobjekpajak($input, $aColumns, $session, $this->cekurl(), $allParams);
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function pilihopAction()
    {
        $req = $this->getRequest();
        $res = $this->getResponse();
        $data = array();
        if ($req->isPost()) {
            $post = $req->getPost();
            $data = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdObjek($post['idobjek']);
            $datakorekwp = $this->Tools()->getService('LaporanTable')->getKorekWpObjek($post['idobjek']);
            //            if(!empty($datakorekwp['s_persentarifkorek'])){
            $data['t_tarifpajak'] = $datakorekwp['s_persentarifkorek'];
            //            }else{
            //                $data['t_tarifpajak'] = 10;
            //            }
            //            $data['t_tarifpajak'] = $datakorekwp['s_persentarifkorek'];
            //            var_dump($data);exit();
        }
        return $res->setContent(json_encode($data));
    }

    public function formpageskpdeditAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $dataskpd = $this->Tools()->getService('LaporanTable')->getdataskpd($session);
        if ($req->isGet()) {
            $message = '';
            $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
            $form = new LaporanFrm($abulan);
            $id = $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('LaporanTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_namaobjek = $datapendaftaran['t_namaobjek'];
            $data->t_masapajak = date("m", strtotime($data->t_masaawal));
            //            var_dump($datapendaftaran);
            //                        exit();
            $form->bind($data);
        }
        $view = new ViewModel(array(
            'form' => $form,
            'message' => $message,
            'dataskpd' => $dataskpd
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function hitungpajakppjAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah0 = str_ireplace(".", "", $data_get['nilailistrik0']);
        $t_jumlah1 = str_ireplace(".", "", $data_get['nilailistrik1']);
        $t_jumlah2 = str_ireplace(".", "", $data_get['nilailistrik2']);
        $t_subpajak0 = round($t_jumlah0 * $data_get['persentarifkorek0'] / 100);
        $t_subpajak1 = round($t_jumlah1 * $data_get['persentarifkorek1'] / 100);
        $t_subpajak2 = round($t_jumlah2 * $data_get['persentarifkorek2'] / 100);
        $data = array(
            // 't_jumlah' => number_format($t_jumlah, 0, ",", "."),
            'subtotalpajak0' => number_format($t_subpajak0, 0, ",", "."),
            'subtotalpajak1' => number_format($t_subpajak1, 0, ",", "."),
            'subtotalpajak2' => number_format($t_subpajak2, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalpajakppjAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();

        $t_subtotalpajak0 = str_ireplace(".", "", $data_get['subtotalpajak0']);
        $t_subtotalpajak1 = str_ireplace(".", "", $data_get['subtotalpajak1']);
        $t_subtotalpajak2 = str_ireplace(".", "", $data_get['subtotalpajak2']);
        $t_jmlhpajak = ($t_subtotalpajak0 + $t_subtotalpajak1 + $t_subtotalpajak2);

        $data = array(
            // 't_tarifdasar' => $tarif_pajak,
            't_jmlhpajak' => number_format($t_jmlhpajak, 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function sesuaikanmasapajakAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
        $data_res = $this->Tools()->getService('LaporanTable')->cekmasapajakbyidobjek($data_get->t_idobjek);
        $datamasa_res = $this->Tools()->getService('LaporanTable')->cekmasapajakbymasaawal($data_get->t_idobjek, $data_get->t_masapajak, $data_get->t_periodepajak);
        //var_dump($data_res['t_masaawal']);
        $masa_sebelumnya = date('m-Y', strtotime($data_res['t_masaawal']  . "+1 month"));
        $masa_sekarang = $data_get->t_masapajak . '-' . $data_get->t_periodepajak;
        //var_dump($data_get->t_periodepajak);exit();
        // Masa Pajak terahir yang diinput dan ditambah 1 bulan
        if (!empty($data_res['t_masaawal'])) {

            if ($masa_sebelumnya == $masa_sekarang) {
                if ($data_get->t_masapajak >= date('m') && $data_get->t_periodepajak >= date('Y')) {
                    $bulanterahir = '';
                    $text_content = 'Belum Waktunya Melaporkan Pajak Bulan <b>' . $abulan[$data_get->t_masapajak] . '</b> Tahun <b>' . $data_get->t_periodepajak . '</b>!';
                    $status = false;
                } else {
                    $bulanterahir = $data_get->t_masapajak;
                    $text_content = '';
                    $status = true;
                }
            }
            //////tambahan ben gak error
            else {
                $bulanterahir = $data_get->t_masapajak;
                $text_content = '';
                $status = true;
            }
            //////endTambahan

            //elseif(!empty($datamasa_res)){
            //	$bulanterahir = '';
            //	$text_content = 'Sudah Melaporkan Pajak Bulan <b>'.$abulan[date('m', strtotime($datamasa_res['t_masaawal']))].'</b> Tahun <b>'.date('Y', strtotime($datamasa_res['t_masaawal'])).'</b>!';
            //	$status = false;
            //}
            //else{
            //	$bulanterahir = '';
            //	$text_content = 'Belum Melaporkan Pajak Bulan <b>' . $abulan[date('m', strtotime($data_res['t_masaawal']. "+1 month"))] . '</b> Tahun <b>' . date('Y', strtotime($data_res['t_masaawal'])) . '</b>!';
            //	$status = false;
            //}
        } else {
            $bulanterahir = '';
            $text_content = '<b>Lapor pertama kali silahkan dikantor PENDAPATAN DAERAH. Terimakasih!</b>';
            $status = false;
        }
        // Masa Pajak yang akan diinput
        $data_render = array(
            "t_masapajak" => $bulanterahir,
            "text_content" => $text_content,
            "status" => $status
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function validasipassAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $pass_lama = $this->Tools()->getService('UserTable')->cekPassOld($data_get['t_password_old'], $session);
        if (empty($pass_lama)) {
            $pesan_passlama = 'Password Lama Salah!';
            $field_passlama = '';
        } else {
            $pesan_passlama = '';
            $field_passlama = $data_get['t_password_old'];
        }

        if ($data_get['s_password'] == $data_get['t_pass2']) {
            $pesan_passbaru = '';
            $field_passbaru = $data_get['s_password'];
            $field_passbaru2 = $data_get['t_pass2'];
        } else {
            $pesan_passbaru = 'Password Baru & Ulangi Password Baru tidak sama!';
            $field_passbaru = '';
            $field_passbaru2 = '';
        }
        $data_render = array(
            "pesan_passlama" => $pesan_passlama,
            "field_passlama" => $field_passlama,
            "pesan_passbaru" => $pesan_passbaru,
            "field_passbaru" => $field_passbaru,
            "field_passbaru2" => $field_passbaru2
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }
    public function carijenisminerbaAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        if ($data_get['s_idjenisminerba'] == '1' || $data_get['s_idjenisminerba'] == 1) {
            $tarif = 6;
        } else {
            $tarif = 4;
        }


        $dataJenisminerba = $this->Tools()->getService('RekeningTable')->getDataJenisMinerba($tarif);
        $opsi = "";
        $opsi .= "
            <option value=''>Silahkan Pilih</option>";
        foreach ($dataJenisminerba as $r) {
            $opsi .= "<option value='" . $r['s_idkorek'] . "'>" . $r['korek'] . ' || ' . $r['s_namakorek'] . "</option>";
        }
        // var_dump($opsi);exit();
        $data = array(
            'opsi' => $opsi,
            // 't_tarifpersen' => $dataRekening['s_persentarifkorek']
        );
        // var_dump($data); die;
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }



    public function hitungkwhppjAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_kwh = '';
        if ($data_get['t_kategori'] == '1') {
            $t_kwh = $data_get['t_kapasitas'] * $data_get['t_jam'] * $data_get['t_hari'];
        } else {
            $t_kwh = '';
        }
        $hargasatuan = $this->Tools()->getService('PendataanTable')->gethargadasarppj($data_get['t_kapasitas']);
        $data = array(
            't_kwh' => $t_kwh,
            't_hargasatuan' => $hargasatuan['s_hargasatuan'],
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungnjtlppjAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $faktordaya = $this->Tools()->getService('PendataanTable')->getfaktordaya($data_get['t_umur']);
        $njtl = '';
        $t_detailhitung = '';
        $t_biayabeban = '';

        if ($data_get['t_kategori'] == '1') {
            $njtl = $data_get['t_kwh'] * $faktordaya['s_faktordaya'] * $data_get['t_hargasatuan'] * $data_get['t_jumlah'];
            $t_detailhitung =  $data_get['t_kwh'] . ' x ' . $faktordaya['s_faktordaya'] . ' x ' . $data_get['t_hargasatuan'] . ' x ' . $data_get['t_jumlah'] . ' = Rp.' . number_format($njtl, 0, ',', '.');
        }

        if ($data_get['t_kategori'] == '2') {
            $jamkerja = $data_get['t_jam'] * $data_get['t_hari'];
            $njtl = $data_get['t_kapasitas'] * $faktordaya['s_faktordaya'] * $jamkerja * $data_get['t_hargasatuan'] * $data_get['t_jumlah'];
            $t_detailhitung = $data_get['t_kapasitas'] . ' x ' . $faktordaya['s_faktordaya'] . ' x ' . $jamkerja . ' x ' . $data_get['t_hargasatuan'] . ' x ' . $data_get['t_jumlah'] . ' = Rp.' . number_format($njtl, 0, ',', '.');
        }

        if ($data_get['t_kategori'] == '3') {
            $t_biayabeban = $data_get['t_hargasatuan'] * $data_get['t_jam'];
            $njtl = $data_get['t_kapasitas'] * $faktordaya['s_faktordaya'] * $data_get['t_jam'] * $data_get['t_hargasatuan'] * $data_get['t_jumlah'];
            $t_detailhitung = $data_get['t_kapasitas'] . ' x ' . $faktordaya['s_faktordaya'] . ' x ' . $data_get['t_jam'] . ' x ' . $data_get['t_hargasatuan'] . ' x ' . $data_get['t_jumlah'] . ' = Rp.' . number_format($njtl, 0, ',', '.');
        }
        $t_pajak = $njtl * $data_get['t_tarifpajak'] / 100;

        $data = array(
            't_njtl' => $njtl,
            't_detailhitung' => $t_detailhitung,
            't_biayabeban' => $t_biayabeban,
            't_pajak' => number_format($t_pajak, 0, ',', '.'),


        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }
    public function hitungtotalpajakppjnewAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        foreach ($data_get['t_pajaks'] as $v) {
            $pajak[] = str_ireplace(".", "", $v);
        }
        $t_jmlhpajak = array_sum($pajak);
        $data = array(
            't_jmlhpajak' => number_format($t_jmlhpajak, 0, ',', '.'),
            't_dasarpengenaan' => ($t_jmlhpajak * 100) / $data_get['t_tarifpajak'],
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungpajakppjplnAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jmlhpajak = (str_ireplace(".", "", $data_get['t_dasarpengenaan'])) * $data_get['t_tarifpajak'] / 100;
        $data = array(
            't_jmlhpajak' => number_format($t_jmlhpajak, 0, ',', '.'),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }
}
