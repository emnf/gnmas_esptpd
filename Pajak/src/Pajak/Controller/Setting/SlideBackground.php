<?php

namespace Pajak\Controller\Setting;

use Zend\View\Model\ViewModel;
use Pajak\Form\Setting\BackgroundslideFrm;
use Pajak\Model\CustomSlide\CustomSlideBase;


class SlideBackground extends \Zend\Mvc\Controller\AbstractActionController {

    protected $table_pemda;
    
    public function cekurl()
     {
        $basePath = $this->getRequest()->getBasePath();
            $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
            $uri->setPath($basePath);
            $uri->setQuery(array());
            $uri->setFragment('');
            
        return $uri->getScheme() . '://' . $uri->getHost() . '' . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'
    
    }
     
    public function indexAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        
        $view = new ViewModel(array(null));
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $datane = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'settingslidebackround' => 1
        );
        $this->layout()->setVariables($datane);
        return $view;
    }
     
     public function dataGridAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $s_iduser = $session['s_iduser']; 
        
        
        $sTable = 'fr_background_slide';
        $count = 'id_bg_slide';
        
        $input = $this->getRequest();
        $order_default = " id_bg_slide DESC";
        $aColumns = array('id_bg_slide','id_bg_slide', 'file_bg_slide', 's_iduser','status_bg_slide');
        
        $panggildata = $this->getServiceLocator()->get("CustomSlideTable");
        $rResult = $panggildata->semuadataackground($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());    
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }
    
    public function tambahAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $frm = new \Pajak\Form\Setting\BackgroundslideFrm();
        $req = $this->getRequest();
        $datapost = $req->getPost();
        $newFile = "";
        if ($req->isPost()) {
            $base = new CustomSlideBase();
            $post = array_merge_recursive($req->getPost()->toArray(), $req->getFiles()->toArray());
            $frm->setData($post);
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $httpadapter = new \Zend\File\Transfer\Adapter\Http();
                $httpadapter->setDestination('public/upload/file_bg_slide/');
                if ($httpadapter->receive($post["t_file_bg_slide"]["name"])) {
                    $newFile = $httpadapter->getFileName();
                }
                
                $this->Tools()->getService('CustomSlideTable')->savedata($newFile, $session, $datapost);
                return $this->redirect()->toRoute('slidebackground');
            }
        }
        $view = new ViewModel(array("frm" => $frm));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'settingslidebackround' => 1
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    public function editAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $frm = new \Pajak\Form\Setting\BackgroundslideFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('id_bg_slide');
            $data = $this->Tools()->getService('CustomSlideTable')->getDataId($id);
            
            $frm->bind($data);
        }
        $view = new ViewModel(array(
            'frm' => $frm,
            'data' => $data
        ));
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'settingslidebackround' => 1
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    public function hapusAction() {
        $this->Tools()->getService('CustomSlideTable')->hapusData($this->params('page'));
        return $this->getResponse();
    }
    
    

    

}
