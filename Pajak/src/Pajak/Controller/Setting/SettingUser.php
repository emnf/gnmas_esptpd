<?php

namespace Pajak\Controller\Setting;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Pajak\Form\Setting\PenggunaFrm;
use Pajak\Model\Setting\SettingUserBase;

class SettingUser extends AbstractActionController {

    public function cekurl() {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        return $uri->getScheme() . '://' . $uri->getHost() . '' . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'
    }

    public function indexAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $form = new PenggunaFrm($this->Tools()->getService('UserTable')->getRole(), array());
        $view = new ViewModel(array(
            'form' => $form
        ));
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'settinguseraktif' => 1
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();

        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $input = $this->getRequest();

        $aColumns = array('s_iduser', 's_username', 's_nama', 's_noidentitas', 's_akses');

        $panggildata = $this->Tools()->getService('UserTable');

        $rResult = $panggildata->semuadatapengguna($input, $aColumns, $session, $this->cekurl(), $allParams);
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    
    public function dataGrid1Action() {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new SettingUserBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('UserTable')->getGridCount($base);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('UserTable')->getGridData($base, $start);
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td data-title='No' style='color:black'><center>" . $counter . "</center></td>";
            $s .= "<td data-title='Username' style='color:black'>" . $row['s_username'] . "</td>";
            $s .= "<td data-title='Nama' style='color:black'>" . $row['s_nama'] . "</td>";
            $s .= "<td data-title='NIK' style='color:black'>" . $row['s_noidentitas'] . "</td>";
            $s .= "<td data-title='NIK' style='color:black'>" . $row['role_name'] . "</td>";
            $s .= "<td data-title='#'><center><a href='setting_user/edit?s_iduser=$row[s_iduser]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Edit</a> <a href='#' onclick='hapus(" . $row['s_iduser'] . ");return false;' class='btn btn-danger btn-xs btn-flat'><i class='glyph-icon icon-trash'></i> Hapus</a></center></td>";
            $s .= "</tr>";
            $counter++;
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages,
            "paginatore" => $datapaging['paginatore'],
            "akhirdata" => $datapaging['akhirdata'],
            "awaldata" => $datapaging['awaldata']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function tambahAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
//        $arskpd = $this->Tools()->getService('ObjekTable')->arskpd($this->Tools()->getService('UserTable')->getSkpdSudah());
//        $selectskpd = array();
//        foreach ($arskpd as $d) {
//            $selectskpd[$d['t_idskpd']] = $d['t_namaskpd'];
//        }
        $frm = new PenggunaFrm($this->Tools()->getService('UserTable')->getRole(), $this->Tools()->getService('ObjekTable')->getAllWpSudah($this->Tools()->getService('UserTable')->getWpSudah()), null, null, null, null, $this->Tools()->getService('ObjekTable')->getAllSkpdSudah($this->Tools()->getService('UserTable')->getSkpdSudah()));
        $req = $this->getRequest();
        if ($req->isPost()) {
            $bs = new SettingUserBase();
            $frm->setInputFilter($bs->getInputFilter());
            $frm->setData($req->getPost());
            if ($frm->isValid()) {
                $bs->exchangeArray($frm->getData());
                $post = $req->getPost();
                if (empty($post->s_iduser)) {
                    $cariEmail = $this->Tools()->getService('UserTable')->cekEmail($post->s_email);
                    if (empty($cariEmail->s_email)) {
                        if ($post->s_akses == 1) {
                            $this->Tools()->getService('ObjekTable')->saveDataKorek($post);
                        }
                        $data = $this->Tools()->getService('UserTable')->saveData($bs);
                        $this->Tools()->getService('UserTable')->deleteResourcePermision($data->s_iduser);
                        $akses = $this->Tools()->getService('UserTable')->CariAkses($post->s_akses);
                        foreach ($akses as $row => $rw) {
                            $this->Tools()->getService('UserTable')->saveresourcepermission($data->s_iduser, $rw['id']);
                        }
                        return $this->redirect()->toRoute('setting_user');
                    } else {
                        $frm->get("s_email")->setMessages(array(
                            "Email Sudah ada yang punya!."
                        ));
                    }
                } else {
                    if ($post->s_akses == 1) {
                        $this->Tools()->getService('ObjekTable')->saveDataKorek($post);
                    }
                    $data = $this->Tools()->getService('UserTable')->saveData($bs);
                    $this->Tools()->getService('UserTable')->deleteResourcePermision($data->s_iduser);
                    $akses = $this->Tools()->getService('UserTable')->CariAkses($post->s_akses);
                    foreach ($akses as $row => $rw) {
                        $this->Tools()->getService('UserTable')->saveresourcepermission($data->s_iduser, $rw['id']);
                    }
                    return $this->redirect()->toRoute('setting_user');
                }
            } else {
//                $msg = $frm->getMessages();
//                var_dump($msg);
//                exit();
            }
        }
        $view = new ViewModel(array(
            'frm' => $frm,
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'settinguseraktif' => 1
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function editAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('s_iduser');
            $data = $this->Tools()->getService('UserTable')->getUserId($id);
            $data_role = $this->Tools()->getService('UserTable')->getResourcePermision($id);
            $data->s_password = '';
           // $arskpd = $this->Tools()->getService('ObjekTable')->arskpd();
           // $selectskpd = array();
           // foreach ($arskpd as $d) {
               // $selectskpd[$d['t_idskpd']] = $d['t_namaskpd'];
           // }
            $frm = new PenggunaFrm($this->Tools()->getService('UserTable')->getRole(), $this->Tools()->getService('ObjekTable')->getAllWpEdit(), null, null, null, null, null); //$this->Tools()->getService('ObjekTable')->getAllSkpdEdit()
            $frm->bind($data);
            // $jsonrole = \Zend\Json\Json::encode($data_role);
            // $frm->get("s_main")->setValue(\Zend\Json\Json::decode($jsonrole));
            // $frm->get("s_laporan")->setValue(\Zend\Json\Json::decode($jsonrole));
            // $frm->get("s_user")->setValue(\Zend\Json\Json::decode($jsonrole));
            // $frm->get("s_laporanbendahara")->setValue(\Zend\Json\Json::decode($jsonrole));
        }
        if ($data->s_akses == 1) {
            $idwp = $data->s_wp;
        } else {
            $idwp = null;
        }
        $displaywp = 'none';
        $displayadmin = 'none';
        if ($data->s_akses == 1) {
            $displaywp = 'block';
            $displayadmin = 'none';
        } elseif ($data->s_akses == 2) {
            $displaywp = 'none';
            $displayadmin = 'block';
        }
        $view = new ViewModel(array(
            'frm' => $frm,
            'idwp' => $idwp,
            'displaywp' => $displaywp,
            'displayadmin' => $displayadmin
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'settinguseraktif' => 1
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function hapusAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $this->Tools()->getService('UserTable')->hapusData($this->params('page'), $session);
        return $this->getResponse();
    }

    public function ubahpassbackAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $data = $this->Tools()->getService('UserTable')->getUserId($session['s_iduser']);
        $data_get = $this->getRequest()->isPost();
        $frm = new PenggunaFrm();
        $frm->bind($data);
        if ($this->getRequest()->isPost()) {
            $bs = new SettingUserBase();
            $frm2 = new PenggunaFrm();
            $frm2->setInputFilter($bs->getInputFilter());
            $frm2->setData($this->getRequest()->getPost());
            if (!$frm2->isValid()) {
                $bs->exchangeArray($frm2->getData());
                $this->Tools()->getService('UserTable')->savepassword($bs, $session);
            }
        }
        $view = new ViewModel(array(
            'frm' => $frm
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $datane = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'ubahpassaktif' => 1
        );
        $this->layout()->setVariables($datane);
        return $view;
    }

    private function getPejabat() {
        $data = $this->Tools()->getService('UserTable')->getPejabat();
        $html = "<option value=''>Silahkan Pilih</option>";
        foreach ($data as $row) {
            $html .= "<option value='" . $row['s_idpejabat'] . "'>" . $row['s_namapejabat'] . " - " . $row['s_jabatanpejabat'] . "</option>";
        }
        return $html;
    }

    private function getResource($resourceid) {
        $data = $this->Tools()->getService('UserTable')->getPermission($resourceid);
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row['id']] = ' ' . ucwords($row['alias']);
        }
        return $selectData;
    }

    public function cariobjekwpAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataobjek = $this->Tools()->getService('ObjekTable')->getDataObjekKorek($data_get['idwp']);
        $html = "";
        $html .= "<div class='col-md-12'>
                    <div class='scroll-columns'>
                        <table class='table table-bordered table-striped table-condensed cf' style='font-size:11px; color: black'>
                            <thead class='cf'>
                                <tr>
                                    <th style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                    <th style='background-color: #00BCA4; color: white; text-align:center'>NOP</th>
                                    <th style='background-color: #00BCA4; color: white; text-align:center'>Nama</th>
                                    <th style='background-color: #00BCA4; color: white; text-align:center'>Jenis</th>
                                    <th style='background-color: #00BCA4; color: white; text-align:center'>Alamat</th>
                                    <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                    <th style='background-color: #00BCA4; color: white; text-align:center'>#</th>
                                </tr>
                            </thead>
                            <tbody>";
        $counter = 1;
        foreach ($dataobjek as $row) {
            if ($row['t_jenisobjek'] == 1 || $row['t_jenisobjek'] == 2 || $row['t_jenisobjek'] == 3 || $row['t_jenisobjek'] == 5 || $row['t_jenisobjek'] == 6 || $row['t_jenisobjek'] == 7 || $row['t_jenisobjek'] == 9) { //|| $row['t_jenisobjek'] == 6  minerba tidak //walet || $row['t_jenisobjek'] == 9
                $html .= "<tr>
                                        <td><center>" . $counter . "<input type='hidden' name='t_idobjek[]' id='t_idobjek' value='" . $row['t_idobjek'] . "'></center></td>
                                        <td style='color:red; font-size:12px; font-weight:bold'><center>" . $row['t_nop'] . "</center></td>
                                        <td>" . $row['t_namaobjek'] . "</td>
                                        <td>" . $row['s_namajenis'] . "</td>
                                        <td>" . $row['t_alamatobjek'] . "</td>
                                        <td><input type='hidden' name='t_idkorek[]' id='t_idkorek" . $row['t_idobjek'] . "' value='" . $row['t_korekobjek'] . "'><input type='text' name='t_korek[]' id='t_korek" . $row['t_idobjek'] . "' class='form-control' readonly='true' value='" . $row['korek'] . " || " . $row['s_namakorek'] . "'></td>
                                        <td style='text-align: center'><button class='btn btn-warning btn-xs' type='button' onclick='bukamodalRekening(" . $row['t_jenisobjek'] . "," . $row['t_idobjek'] . ")'><span class='glyph-icon icon-search'></span></button></td>
                                    </tr>";
                $counter++;
            }
        }
        $html .= "</tbody>
                        </table>
                    </div>
                </div>";
        $data = array(
            'dataobjek' => $html
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function dataGridRekeningAction() {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $req->isGet();
        $parametercari = $req->getQuery();
        $base = new \Pajak\Model\Setting\RekeningBase();
        $base->exchangeArray($allParams);
        $count = $this->Tools()->getService('RekeningTable')->getGridCountRekeningUser($base, $parametercari);
        $s = "";
        $data = $this->Tools()->getService('RekeningTable')->getGridDataRekeningUser($base, $parametercari);
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td>" . $row['korek'] . "</td>";
            $s .= "<td>" . $row['s_namakorek'] . "</td>";
            $s .= "<td>" . $row['s_persentarifkorek'] . "</td>";
            $s .= "<td><center><a href='#' onclick='pilihRekening(" . $row['s_idkorek'] . "," . $parametercari->t_idobjek . ");return false;' class='btn btn-xs btn-primary'><span class='glyph-icon icon-hand-o-up'> Pilih </a></center></td>";
            $s .= "</tr>";
        }
        $data_render = array(
            "grid" => $s,
            "count" => $count,
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function pilihRekeningAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataRekening = $this->Tools()->getService('RekeningTable')->getDataRekeningId($data_get['s_idkorek']);
        $data = array(
            't_idkorek' => $dataRekening['s_idkorek'],
            't_korek' => $dataRekening['korek'] . " || " . $dataRekening['s_namakorek']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function resetpassword($id) {
        $newpassword = Rand::getString(64, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', true);
        $this->update(array("s_passwordreset" => $newpassword, "s_passwordresetvalid" => date('Y-m-d H:i:s', strtotime("+15 minutes"))), array("s_id" => $id));
        return $newpassword;
    }

    //Histori Login
    public function sessionhislogAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $view = new ViewModel(array());
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'historylogsession' => 1
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridsessionhislogAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();

        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $input = $this->getRequest();

        $aColumns = array('s_iduser', 's_username', 's_nama', 'ip', 'login_time', 'last_active');

        $panggildata = $this->Tools()->getService('UserTable');

        $rResult = $panggildata->semuadataSessionhislog($input, $aColumns, $session, $this->cekurl(), $allParams);
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }
    
    //Histori Aktivitas user
    public function hisaktifitasAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $view = new ViewModel(array());
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'hisaktifitas' => 1
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridhisaktifitasAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();

        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $input = $this->getRequest();

        $aColumns = array('s_iduser', 's_username', 's_nama', 'ip', 'hislog_time', 'hislog_action');

        $panggildata = $this->Tools()->getService('UserTable');

        $rResult = $panggildata->semuadataHisaktifitas($input, $aColumns, $session, $this->cekurl(), $allParams);
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }
    
	public function validasipassAction() {
		$session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $pass_lama = $this->Tools()->getService('UserTable')->cekPassOld($data_get['t_password_old'], $session);
		if(empty($pass_lama)){
			$pesan_passlama = 'Password Lama Salah!';
			$field_passlama = '';
		}else{
			$pesan_passlama = '';
			$field_passlama = $data_get['t_password_old'];
		}
		
		if($data_get['s_password'] == $data_get['t_pass2']){
			$pesan_passbaru = '';
			$field_passbaru = $data_get['s_password'];
			$field_passbaru2 = $data_get['t_pass2'];
		}else{
			$pesan_passbaru = 'Password Baru & Ulangi Password Baru tidak sama!';
			$field_passbaru = '';
			$field_passbaru2 = '';
		}
		$data_render = array(
			"pesan_passlama" => $pesan_passlama,
			"field_passlama" => $field_passlama,
			"pesan_passbaru" => $pesan_passbaru,
			"field_passbaru" => $field_passbaru,
			"field_passbaru2" => $field_passbaru2
		);
		return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
	}
}
