<?php

namespace Pajak\Controller;

use Zend\View\Model\ViewModel;
use Pajak\Form\LoginAccessFrm;

class LoginAccess extends \Zend\Mvc\Controller\AbstractActionController {
    
    public function indexAction() {
        /** index login 
         * sebagai halaman awal login
         * @param string $s_username
         * @param string $s_password
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 03/11/2016
         */
        $form = new LoginAccessFrm();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pemda = $ar_pemda;
        $view = new ViewModel(array(
            "form" => $form,
            'data_pemda' => $pemda
        ));
        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());
            if ($form->isValid()) {
                $this->getServiceLocator()->get('PajakService')
                        ->getAdapter()
                        ->setIdentity($this->getRequest()->getPost('s_username'))
                        ->setCredential($this->getRequest()->getPost('s_password'));
                if (empty($this->getRequest()->getPost('s_username'))) {
                    return $this->redirect()->toRoute('sign_in');
                }
                $hasil = $this->getServiceLocator()->get('PajakService')->authenticate();
                if ($hasil->isValid()) {
                    $data_user = $this->Tools()->getService('UserTable')->getuserdata($this->getRequest()->getPost('s_username'));
                    $resultRowObject = $this->getServiceLocator()->get('PajakService')->getAdapter()->getResultRowObject();
                    $last_login = $this->Tools()->getService('UserTable')->getSessionlastLoginId($resultRowObject->s_iduser);
                    $status_login = (date('d-m-Y', strtotime($last_login['last_active'])) == '01-01-1970' ? ' - ' : date("d-m-Y H:i:s", strtotime($last_login['last_active'])));
                    $this->Tools()->getService('UserTable')->saveSessionLogin($resultRowObject->s_iduser,$resultRowObject->s_username,$data_user['s_nama']);
                    $this->getServiceLocator()->get('PajakService')->getStorage()->write(
                            array(
                                's_iduser' => $resultRowObject->s_iduser,
                                's_username' => $resultRowObject->s_username,
                                's_akses' => $resultRowObject->s_akses,
                                's_namauserrole' => $data_user['role_name'],
                                's_nama' => $data_user['s_nama'],
                                's_menu' => $data_user['s_menu'],
                                's_wp' => $data_user['s_wp'],
                                's_skpd' => $data_user['s_skpd'],
                                's_tipeoperator' => $data_user['s_tipeoperator'],
                            )
                    );
                    $this->flashMessenger()->addMessage('<h4><i class="glyph-icon icon-check"></i> Login Sukses!</h4>Selamat Datang '.strtoupper($data_user['s_nama']).', Terima kasih telah menggunakan aplikasi ini dengan baik!<br>'
                        . 'Terakhir Login : '.$status_login.'');
                    return $this->redirect()->toRoute('main');
                }
            }
        }
        $data = array('nilai' => '2','data_pemda' => $pemda);
        $this->layout()->setVariables($data);
        return $view;
    }

    public function logoutAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $this->getServiceLocator()->get('PajakService')->clearIdentity();
        $hislog_action = 'Keluar Dari Aplikasi '.$session['s_iduser'].'/'.$session['s_username'];
        $this->Tools()->getService('UserTable')->saveHislogActivity($session, $hislog_action);
        $this->flashMessenger()->addMessage('<img src="'. $this->cekurl().'/public/img/lock-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;"><b>Sesi Berakhir!</b></span><br>
                                    Untuk keamanan, tutup browser Anda dan Login Kembali');
//        $this->flashMessenger()->addMessage('<h4><i class="glyph-icon icon-key"></i> Sesi Berakhir!</h4>Untuk keamanan, tutup browser Anda dan Login Kembali');
        return $this->redirect()->toRoute('sign_in');
    }
    
    public function realisasiAction(){
        /*
        $datatargetpbb = $this->Tools()->getService('PbbTable')->cekgridpbb();
     * 
     */
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataget = array("tahun" => date("Y"));
        //bphtb ==========================
        $datatargetbphtb = $this->Tools()->getService('BphtbTable')->caritargetbphtb();
        $targetbphtb = str_ireplace(".", "", $datatargetbphtb['s_targetrealisasi']);
        $datarealisasibphtb = $this->Tools()->getService('BphtbTable')->getrealisasibphtb($dataget);
        //================================
        //====PBB
        $datatargetpbb = $this->Tools()->getService('PbbTable')->targetpbb();
        $realisasipbb = $this->Tools()->getService('PbbTable')->realisasipbb();
        //====PBB
        //=========pajak
        $arrealisasi = array();
        $datarealisasipajak = $this->Tools()->getService('LaporanTable')->getdatarealisasi($dataget);
        foreach ($datarealisasipajak as $d){
            $arrealisasi['realisasi'][] = $d['transaksi'] + $d['transaksidenda'] + $d['skpdkb'] + $d['skpdkbdenda'] + $d['skpdkbt'] + $d['skpdkbtdenda'] + $d['skpdt'] + $d['skpdtdenda'];
            $arrealisasi['target'][] = $d['target'];
            $arrealisasi['namapajak'][] = $d['s_namajenis'];
            $arrealisasi['namapajaksingkat'][] = $d['s_namajenissingkat'];
            $arrealisasi['idjenis'][] = $d['s_idjenis'];
        }
        //==================================
         //====== PBB
        $arrealisasi['realisasi'][] = $realisasipbb['realisasi'];
        $arrealisasi['target'][] = $datatargetpbb['target'];
        $arrealisasi['namapajak'][] = "Pajak Bumi Dan Bangunan";
        $arrealisasi['namapajaksingkat'][] = "PBB";
        $arrealisasi['idjenis'][] = 10;
        //==================================
        //====== bphtb
        $arrealisasi['realisasi'][] = $datarealisasibphtb['realisasi'];
        $arrealisasi['target'][] = $targetbphtb;
        $arrealisasi['namapajak'][] = "Bea Perolehan Hak Atas Tanah dan Bangungn";
        $arrealisasi['namapajaksingkat'][] = "BPHTB";
        $arrealisasi['idjenis'][] = 11;
        //==================================
        
//        var_dump($arrealisasi);exit();
        $view = new ViewModel(array(
            'data_pemda' => $ar_pemda,
            'datarealisasi' => $arrealisasi,
        ));

        $data = array(
            'data_pemda' => $ar_pemda,
            'nilai' => 3
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    public function datagridrealisasiAction(){
        $input = $this->getRequest();
        $dataget = array("tahun" => date("Y"));
        $aColumns = array();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        
        //bphtb ==========================
        $datatargetbphtb = $this->Tools()->getService('BphtbTable')->caritargetbphtb();
        $targetbphtb = str_ireplace(".", "", $datatargetbphtb['s_targetrealisasi']);
        $datarealisasibphtb = $this->Tools()->getService('BphtbTable')->getrealisasibphtb($dataget);
//        var_dump($datarealisasibphtb);exit();
        //================================
        
        $iTotal = 0;
        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal, 
            "aaData" => array(),
        );
        $no = 1;
        $rResult = array();
        foreach ($rResult as $aRow) {
            $row = array();            
            $row = array("<center>".$no."</center>", 
                                "<center>".$aRow['t_nama_npwpd']."</center>",
                                "<center>".$aRow['t_namaobjek']."</center>", 
                                "<center>".$aRow['s_namajenis']."</center>", 
                                "<button type='button' class='btn btn-success' onclick='pilihop(".$aRow['t_idobjek'].")'>Pilih</button>", 
                        );
            $output['aaData'][] = $row;
            $no++;
        }
        
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($output));
        
    }
    
    public function cekurl()
     {
        $basePath = $this->getRequest()->getBasePath();
            $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
            $uri->setPath($basePath);
            $uri->setQuery(array());
            $uri->setFragment('');
            
        //return $uri->getScheme() . '://' . $uri->getHost() . ':'.$_SERVER['SERVER_PORT'].'' . $uri->getPath();
		return $uri->getScheme() . '://' . $uri->getHost() . '' . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'
     }
     
     public function resetpasswordAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $form = new \Pajak\Form\ResetPasswordFrm();
        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());
            if ($form->isValid()) {
                $getEmail = $this->Tools()->getService('UserTable')->getUserByEmail($data_get['s_email']);
//                var_dump($getEmail['s_email']);exit();
                if(!empty($getEmail['s_iduser'])){
                    $tokenresetpasswd = $this->Tools()->getService("UserTable")->resetpassword($getEmail['s_iduser']);
//                    $link = "http://esptpd.kaimanakab.go.id/confirmresetpassword?part1=".$tokenresetpasswd;
                    $link = "http://192.168.8.101/esptpd/esptpd_kaimana/confirmresetpassword?part1=".$tokenresetpasswd;
                    $this->Tools()->sendMail($getEmail, $link);
//                    $this->flashMessenger()->addMessage('Kami sudah mengirimkan tautan ke alamat e-mail, silahkan cek e-mail anda!');
                    $this->flashMessenger()->addMessage('<img src="'. $this->cekurl().'/public/img/send-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:#37d63a;"><b>Terkirim!</b></span><br>
                                    Kami sudah mengirimkan tautan ke alamat e-mail, silahkan cek e-mail anda!');
                    return $this->redirect()->toRoute('sign_in');
                }else{
//                    $this->flashMessenger()->addMessage('Alamat e-mail yang anda masukkan tidak terdaftar!');
                    $this->flashMessenger()->addMessage('<img src="'. $this->cekurl().'/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                    Alamat e-mail yang anda masukkan tidak terdaftar!');
                    return $this->redirect()->toRoute('resetpassword');
//                    $form->get("message_success")->setMessages(array(
//                        "Alamat e-mail yang anda masukkan tidak terdaftar."
//                    ));
                }
            }
        }
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pemda = $ar_pemda;
        $view = new ViewModel(array(
            "form" => $form,
            'data_pemda' => $pemda,
        ));
        $data = array(
            'nilai' => '2',
            'data_pemda' => $pemda
            );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function confirmresetpasswordAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $form = new \Pajak\Form\ConfirmresetPasswordFrm();
        $ar_token = $this->Tools()->getService('UserTable')->validasiToken($req->getQuery()->get('part1'));//$this->params("par1")
//        var_dump($ar_token['s_passwordreset']);exit();
        if(empty($ar_token['s_passwordreset'])){
//            $this->flashMessenger()->addMessage('Tautan yang anda buka sudah tidak valid!');
            $this->flashMessenger()->addMessage('<img src="'. $this->cekurl().'/public/img/icon-warning.jpg" width="100" style="margin-bottom: 5px;"><br>
                                    <span style="font-size:16pt;color:red;"><b>Gagal!</b></span><br>
                                    Tautan yang anda buka sudah tidak valid!');
            return $this->redirect()->toRoute('resetpassword');
        }
        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());
            if ($form->isValid()) {
                if($data_get['s_passwordbaru'] === $data_get['s_passwordbarulagi']){
                    $this->Tools()->getService('UserTable')->changepassword($data_get['s_passwordbarulagi'],$ar_token['s_iduser']);
                    $this->Tools()->getService("UserTable")->resetpassword($ar_token['s_iduser']);
    //                $this->flashMessenger()->addMessage('Password Berhasil diganti!');
                    $this->flashMessenger()->addMessage('<img src="'. $this->cekurl().'/public/img/key-green.jpg" width="100" style="margin-bottom: 5px;"><br>
                                        <span style="font-size:16pt;color:#37d63a;"><b>Sukses!</b></span><br>
                                        Password berhasil diganti!');
                    return $this->redirect()->toRoute('sign_in');
                }else{
                    $form->get("s_passwordbarulagi")->setMessages(array(
                        "Password Tidak Sama!."
                    ));
                }
            }
        }
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pemda = $ar_pemda;
        $view = new ViewModel(array(
            "form" => $form,
            'data_pemda' => $pemda,
        ));
        $data = array(
            'nilai' => '2',
            'data_pemda' => $pemda
            );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    public function npwpdrdAction() {
        $req = $this->getRequest();
        $npwpd_rd = $req->getQuery()->get('npwpd_rd');
        $ar_wp = $this->Tools()->getService('PendataanTable')->getDataNPWPDRD($npwpd_rd);
        $ar_wpobjek = $this->Tools()->getService('PendataanTable')->getDataWPId($ar_wp['t_idwp']);
        if(!empty($ar_wp['t_npwpd'])){
            $ar_npwpd = $ar_wp['t_npwpd'];
        }else{
            $ar_npwpd = '';
        }
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $view = new ViewModel(array(
            'data_wp' => $ar_wp,
            'data_wpobjek' => $ar_wpobjek,
            'data_pemda' => $ar_pemda,
            'data_npwpdrd' => $ar_npwpd
        ));
        $data = array(
            'nilai' => '2',
            );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    public function npwpdrdobjekAction() {
        $req = $this->getRequest();
        $npwpd_rd = $req->getQuery()->get('npwpd_rd');
        $t_idobjek = $req->getQuery()->get('t_idobjek');
        $ar_wp = $this->Tools()->getService('PendataanTable')->getDataNPWPDRD($npwpd_rd);
        $ar_wpobjek = $this->Tools()->getService('PendataanTable')->getDataWPId($ar_wp['t_idwp']);
//        var_dump($ar_wpobjek['t_idobjek']); exit();
        $ar_transaksi = $this->Tools()->getService('PendataanTable')->getDataTransaksiWP($t_idobjek);
        if(!empty($ar_wp['t_npwpd'])){
            $ar_npwpd = $ar_wp['t_npwpd'];
        }else{
            $ar_npwpd = '';
        }
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $view = new ViewModel(array(
            'data_wp' => $ar_wp,
            'data_wpobjek' => $ar_wpobjek->current(),
            'data_transaksi' => $ar_transaksi,
            'data_pemda' => $ar_pemda,
            'data_npwpdrd' => $ar_npwpd
        ));
        $data = array(
            'nilai' => '2',
            );
        $this->layout()->setVariables($data);
        return $view;
    }
    

    //generate captcha
    public function generateAction()
    {
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', "image/png");
 
        $id = $this->params('id', false);
 
        if ($id) {
 
            $image = './data/captcha/' . $id;
 
            if (file_exists($image) !== false) {
                $imagegetcontent = @file_get_contents($image);
 
                $response->setStatusCode(300);
                $response->setContent($imagegetcontent);
 
                if (file_exists($image) == true) {
                    unlink($image);
                }
            }
 
        }
 
        return $response;
    }

}
