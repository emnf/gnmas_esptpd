<?php


namespace Pajak;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;

use Pajak\Helper\MenuHelper;

class Module implements AutoloaderProviderInterface {

    // Common Code
    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    // Custom Code
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'CustomLayoutTable' => function ($sm) {
                    $dbAdapter = $sm->get('esptpddb');
                    $table = new Model\CustomLayout\CustomLayoutTable($dbAdapter);
                    return $table;
                },
                'CustomSlideTable' => function ($sm) {
                    $dbAdapter = $sm->get('esptpddb');
                    $table = new Model\CustomSlide\CustomSlideTable($dbAdapter);
                    return $table;
                },
                'PemdaTable' => function ($sm) {
                    $dbAdapter = $sm->get('simpatdadb');
                    $table = new Model\Setting\PemdaTable($dbAdapter);
                    return $table;
                },
                'RekeningTable' => function ($sm) {
                    $dbAdapter = $sm->get('simpatdadb');
                    $table = new Model\Setting\RekeningTable($dbAdapter);
                    return $table;
                },
                'ObjekTable' => function ($sm) {
                    $dbAdapter = $sm->get('simpatdadb');
                    $table = new Model\Pendaftaran\ObjekTable($dbAdapter);
                    return $table;
                },
                'PendataanTable' => function ($sm) {
                    $dbAdapter = $sm->get('simpatdadb');
                    $table = new Model\Pendataan\PendataanTable($dbAdapter);
                    return $table;
                },
                'DetailPpjTable' => function ($sm) {
                    $dbAdapter = $sm->get('simpatdadb');
                    $table = new Model\Pendataan\DetailPpjTable($dbAdapter);
                    return $table;
                },
                'DetailminerbaTable' => function ($sm) {
                    $dbAdapter = $sm->get('simpatdadb');
                    $table = new Model\Pendataan\DetailminerbaTable($dbAdapter);
                    return $table;
                },
                'DetailparkirTable' => function ($sm) {
                    $dbAdapter = $sm->get('simpatdadb');
                    $table = new Model\Pendataan\DetailparkirTable($dbAdapter);
                    return $table;
                },
                'LaporanTable' => function ($sm) {
                    $dbAdapter = $sm->get('simpatdadb');
                    $table = new Model\Laporan\LaporanTable($dbAdapter);
                    return $table;
                },
                'LaporanbendaharaTable' => function ($sm) {
                    $dbAdapter = $sm->get('simpatdadb');
                    $table = new Model\Laporanbendahara\LaporanbendaharaTable($dbAdapter);
                    return $table;
                },
                'UserTable' => function ($sm) {
                    $dbAdapter = $sm->get('esptpddb');
                    $table = new Model\Setting\SettingUserTable($dbAdapter);
                    return $table;
                },
                'BphtbTable' => function ($sm) {
                    $adapter = $sm->get('bphtbdb');
                    $table = new Model\Bphtb\BphtbTable($adapter);
                    return $table;
                },
                'AclPermissionTable' => function ($sm) {
                    $dbAdapter = $sm->get('esptpddb');
                    $table = new Model\Setting\AclPermissionTable($dbAdapter);
                    return $table;
                },
                'AclResourceTable' => function ($sm) {
                    $dbAdapter = $sm->get('esptpddb');
                    $table = new Model\Setting\AclResourceTable($dbAdapter);
                    return $table;
                },
                'PajakService' => function ($sm) {
                    $dbAdapter = $sm->get('esptpddb');
                    $dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, "s_users", "s_username", "s_password", "MD5(?)");
                    $eTaxService = new AuthenticationService ();
                    $eTaxService->setAdapter($dbTableAuthAdapter);
                    return $eTaxService;
                },
//                'PbbTable' => function ($sm) {
//                    $adapter = $sm->get('pbbdb');
//                    $table = new Model\Bphtb\PbbTable($adapter);
//                    return $table;
//                },
                'RoleTable' => function ($serviceManager) {
                    return new Model\Secure\Role($serviceManager->get('esptpddb'));
                },
                'UserRoleTable' => function ($serviceManager) {
                    return new Model\Secure\UserRole($serviceManager->get('esptpddb'));
                },
                'PermissionTable' => function ($serviceManager) {
                    return new Model\Secure\PermissionTable($serviceManager->get('esptpddb'));
                },
                'ResourceTable' => function ($serviceManager) {
                    return new Model\Secure\ResourceTable($serviceManager->get('esptpddb'));
                },
                'RolePermissionTable' => function ($serviceManager) {
                    return new Model\Secure\RolePermissionTable($serviceManager->get('esptpddb'));
                },
                'PajakAcl' => function ($serviceManager) {
                    return new Utility\Acl ();
                },
                'app_path' => function ($sm) {
                    $path_aplikasi = "/var/www/html/pajak";
                    return $path_aplikasi;
                },
            )
        );
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
                'MenuHelper' => function ($serviceManager) {
                    $helper = new MenuHelper();
                    return $helper;
                },
                'CustomHelper' => function ($serviceManager) {
                    $helper = new CustomHelper();
                    return $helper;
                }
            )
        );
    }

    public function init(\Zend\ModuleManager\ModuleManager $mm) {
        $mm->getEventManager()->getSharedManager()->attach(__NAMESPACE__, 'dispatch', function ($e) {
            $e->getTarget()->layout('pajak/layout');
        });
    }

    public function bootstrapSession(MvcEvent $e) {
        $session = $e->getApplication()->getServiceManager()->get('Sessi_Manager');
        $session->start();
        $container = new Container('PajakService');
        if (isset($container->init)) {
            $session->regenaretId(true);
            $container->init = 1;
        }
    }

    public function onBootstrap($e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new \Zend\Mvc\ModuleRouteListener ();
        $moduleRouteListener->attach($eventManager);
        $sessionManager = new SessionManager();
        $sessionManager->setName('eSPTPDROHIL');
        $sessionManager->rememberMe('36000');
        // $sessionManager->rememberMe('31536000');
        $sessionManager->start();
        $eventManager->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH, array(
            $this,
            'boforeDispatch'
                ), 100);
    }

    function boforeDispatch(\Zend\Mvc\MvcEvent $event) {
        $authenticationService = $event->getApplication()->getServiceManager()->get('PajakService');
        $request = $event->getRequest();
        $response = $event->getResponse();
        $target = $event->getTarget();
        $whiteList = array(
            'LoginAccess-logout',
            'LoginAccess-index',
            'LoginAccess-resetpassword',
            'LoginAccess-confirmresetpassword',
//            'LoginAccess-realisasi',
//            'LoginAccess-datagridrealisasi'
        );
        $requestUri = $request->getRequestUri();
        $controller = $event->getRouteMatch()->getParam('controller');
        $action = $event->getRouteMatch()->getParam('action');
        if (strpos($action, '_') !== false) {
            $arr_action = explode("_", $action);
            $action = ucwords($arr_action[0]) . "" . ucwords($arr_action[1]);
        }
        $requestedResourse = $controller . "-" . $action;
		# protect route using acl
        /*
        if ($authenticationService->hasIdentity()) {
            if ($requestedResourse == 'LoginAccess-logout' || in_array($requestedResourse, $whiteList)) {
                $url = 'sign_in';
                $response->setHeaders($response->getHeaders()->addHeaderLine('Location', $url));
                $response->setStatusCode(302);
            } else {
                $serviceManager = $event->getApplication()->getServiceManager();
                $storage = $serviceManager->get('PajakService')->getStorage()->read();
                // $userRole = $storage['s_namauserrole'];
                $userRole = $storage['s_username'];
                $acl = $serviceManager->get('PajakAcl');
                $acl->initAcl();

                $status = $acl->isAccessAllowed($userRole, $controller, $action);
                if (!$status) {
                    die('Anda tidak di ijinkan untuk membuka halaman ini');
//                $url = '-';
                    $response->setHeaders($response->getHeaders()->addHeaderLine('Location', $url));
                    $response->setStatusCode(302);
                }
            }
        } else {
            if ($requestedResourse != 'LoginAccess-index' && !in_array($requestedResourse, $whiteList)) {
                $url = 'sign_in';
                $response->setHeaders($response->getHeaders()->addHeaderLine('Location', $url));
                $response->setStatusCode(302);
            }
            $response->sendHeaders();
        }
		
		*/
		# protect route with session
        
        if (!$authenticationService->hasIdentity()) {
            if ($requestedResourse != 'LoginAccess-index' && !in_array($requestedResourse, $whiteList)) {
                $event->getRouteMatch()->setParam('controller', 'LoginAccess')->setParam('action', 'index');
            }
        } else {
            if ($requestedResourse == 'LoginAccess-index' || in_array($requestedResourse, $whiteList)) {
                $url = 'main';
                $response->setHeaders($response->getHeaders()->addHeaderLine('Location', $url));
                $response->setStatusCode(302);
            }
            
            $response->sendHeaders();
        }
    }

}
